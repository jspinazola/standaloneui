package xos;

import FPS.FPSData;
import FPS.SQLManager;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import xos.DataInterface;
import xos.ExportToCloud;
import xos.xom.comm.Payload;
import xos.xom.comm.XOSCommunicator;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Control {

    private static Thread dataThread;

    private static ZMQ.Context ctx = ZMQ.context(1);
    private static ZMQ.Socket pub = ctx.socket(SocketType.PUB);
    private static String addressHeader = "tcp://*:";
    private static int portPub = 5556;
    private static boolean connected = false;
    private static boolean running = true;
    private static XOSCommunicator comm = null;
    private static ExportToCloud ex = new ExportToCloud();
    private static SQLManager sql = new SQLManager("FPSDatabase");
    private static String heartbeatUrl = "https://datapit.duckdns.org/api/heartbeat";
    private static String submitUrl = "https://datapit.duckdns.org/api/submitXOS";

    public static void main(String [] args){
        DataInterface dataMonitor = new DataInterface();
        dataThread = new Thread(dataMonitor,"dataThread");
        dataMonitor.setEndpointUrl(submitUrl);
        dataThread.start();

        if(args.length > 0 && args[0].equals("test")) {
            try (FileReader reader = new FileReader("Sample.json")) {
                //Read JSON file
                JSONParser jsonParser = new JSONParser();
                Object obj = jsonParser.parse(reader);
                JSONObject jObj = (JSONObject) obj;
                Payload out = null;
                out = new Payload(jObj, null);
                comm = XOSCommunicator.getInstance();
                JSONObject resp = new JSONObject();
                resp = comm.publishToEndpoint(out, submitUrl);
                if (resp != null && resp.get("xos_token").toString() == "published") {
                    System.out.println(resp.toString());
                    //sql.setUploaded(in.getSqlID());
                } else {
                    System.out.println(resp.toString());
                }
                try (FileWriter file = new FileWriter("test.json")) {

                    file.write(out.getPayload().toString());
                    file.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            FPSData data = new FPSData();
            System.out.println(data.getkV1());
            System.exit(0);
        }

        ex.loadUnitConfig();
        comm = XOSCommunicator.getInstance();
        while(running){
            heartbeat();
            if(connected){
                if(sql.getNumberOfPendingUploads()>0){
                    uploadSavedData();
                }
            }
            try {
                Thread.sleep(60000);
            }catch (InterruptedException e){

            }
        }

    }

    public static void heartbeat(){
        JSONObject beat = new JSONObject();
        JSONObject resp = new JSONObject();
        beat.put("serialNumber", ex.getSerialNumber());
        beat.put("deviceID", ex.getDeviceID());
        beat.put("timeStamp",ex.getTimeStampZulu());
        Payload load = new Payload(beat,null);
        resp = comm.publishToEndpoint(load, heartbeatUrl);
        if (resp != null && resp.get("xos_token").toString() == "published") {
            //System.out.println(resp.toString());
            connected = true;
        }
    }

    public static void uploadSavedData(){
        for (FPSData data : sql.getPendingDataEntries()){
            ex.setEndpointUrl(submitUrl);
            ex.setFPSData(data);
            ex.exportData();
        }
    }
}