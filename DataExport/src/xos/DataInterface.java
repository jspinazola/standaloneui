package xos;

import javafx.application.Platform;
import org.json.simple.JSONObject;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import FPS.FPSData;
import xos.xom.comm.EndpointCommunicator;
import xos.xom.comm.XOSCommunicator;
import xos.ExportToCloud;

public class DataInterface implements Runnable {

    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket sub = ctx.socket(SocketType.SUB);
    private String addressHeader = "tcp://*:";
    private int portReq = 5557;
    private int portSub = 5556;
    private boolean running = true;
    private String endpointUrl = "";

    public void setEndpointUrl(String url){
        this.endpointUrl = url;
    }

    public void run(){
        sub.connect(addressHeader+Integer.toString(portSub));
        sub.subscribe("Data");
        while(running){
            String topic = sub.recvStr();
            switch (topic){
                case "Data":{
                        String data = sub.recvStr();
                        String FTIR = "";
                        if(sub.hasReceiveMore()){
                            FTIR = sub.recvStr();
                        }
                        ExportToCloud ex = new ExportToCloud();
                        Thread t = new Thread(ex);
                        ex.loadUnitConfig();
                        ex.setEndpointUrl(endpointUrl);
                        ex.setFtirData(FTIR);
                        ex.export(data);
                        t.start();
                    }
                    break;
                default: {
                    String data = sub.recvStr();
                    //System.out.println("Unwanted Topic - " + topic + " : " + data);
                }
            }
        }
    }

}