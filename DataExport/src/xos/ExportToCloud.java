package xos;

import FPS.FPSData;
import FPS.FTIR.FTIRData;
import FPS.SQLManager;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xos.xom.comm.EndpointCommunicator;
import xos.xom.comm.XOSCommunicator;
import xos.xom.comm.Payload;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.time.Instant;
import java.util.Date;

import java.io.FileWriter;
import java.io.IOException;


public class ExportToCloud implements Runnable{

    EndpointCommunicator comm = null;
    SQLManager sql = new SQLManager("FPSDatabase");
    private FPSData in = new FPSData();
    private FTIRData ftirData = new FTIRData();
    private String endpointUrl;
    private String serialNumber = "";
    private String deviceID = "";
    //"4cc7ea22-8eca-4c5e-8f81-467a41bd4a28"

    public void ExportToCloud(){
    }

    public void setEndpointUrl(String endpointUrl){
        this.endpointUrl = endpointUrl;
    }

    public void run(){
        exportData();
    }

    public void export(String data){
        in.parseJSONData(data);
    }

    public void setFPSData(FPSData data){
        in = data;
    }

    public void setFtirData(String data) {ftirData.parseJSON(data);}

    public String getTimeStampZulu() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Z"));
        return dateFormat.format(Date.from(Instant.now())) + "Z";
    }

    public void exportData(){
        JSONObject resp = new JSONObject();
        JSONObject json = new JSONObject();
        JSONObject message = new JSONObject();
        JSONObject xrf = new JSONObject();
        Payload out;
        comm = XOSCommunicator.getInstance();
        json.put("scanID",in.getSqlID());
        json.put("ftir",ftirData.toJSONObject());
        /*
        try (FileReader reader = new FileReader("FTIRData.json")) {
            //Read JSON file
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(reader);
            JSONObject jObj = (JSONObject) obj;
            json.put("ftir", jObj);
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        json.put("viscosityMeasurement",in.getDataExportJson());
        message.put("fpsFirmwareVersion",in.getScanInfo().getFWVersion());
        message.put("serialNumber",serialNumber);
        message.put("sampleID",in.getScanInfo().getName());
        message.put("scanID",in.getSqlID());
        message.put("timeStamp",getTimeStampZulu());
        message.put("deviceID",deviceID);
        message.put("appVersion","0.0.1");
        message.put("hwServerVersion","0.0.1");
        message.put("dataExporterVersion","0.0.1");
        message.put("solverVersion","NA");
        json.put("message",message);
        xrf.put("timeStamp",getTimeStampZulu());
        xrf.put("serialNumber",serialNumber);
        xrf.put("sampleID",in.getScanInfo().getName());
        try (FileReader reader = new FileReader("ElementData.json")) {
            //Read JSON file
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(reader);
            JSONArray jarr = (JSONArray) obj;
            xrf.put("elementData", jarr);
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        xrf.put("scanID",in.getSqlID());
        xrf.put("calibration","");
        json.put("xrf",xrf);
        out = new Payload(json,null);
        resp = comm.publishToEndpoint(out, endpointUrl);
        if (resp != null && resp.get("xos_token").toString() == "published") {
            //System.out.println(resp.toString());
            sql.setUploaded(in.getSqlID());
        }else{
            System.out.println(resp.toString());
        }
        try (FileWriter file = new FileWriter("test.json")) {

            file.write(json.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadUnitConfig(){
        try (FileReader reader = new FileReader("Config.json")) {
            //Read JSON file
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(reader);
            JSONObject jObj = (JSONObject) obj;
            for(Object key : jObj.keySet()){
                try{
                    if(key.toString().equals("serialNumber")){
                        serialNumber = jObj.get(key).toString();
                    }else if(key.toString().equals("deviceID")){
                        deviceID = jObj.get(key).toString();
                    }
                }catch (Exception e){

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getSerialNumber(){
        return serialNumber;
    }
    public String getDeviceID(){
        return deviceID;
    }
}