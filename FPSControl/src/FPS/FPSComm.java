package FPS;

import Util.SerialPortHelper;

public class FPSComm extends SerialPortHelper{
    private FPSSystemState fpsStatus;
    private int commandAttempts = 0;
    private final int maxAttempts = 5;

    public FPSComm(){
        super("USB-RS485 Cable",19200,500);
        fpsStatus = new FPSSystemState();
        fpsStatus.setState(FPSStates.Initialize);
        System.out.println(fpsStatus.getState());
        System.out.println(PORT_STATUS);
        fpsStatus.setFWVersion(getFWVersion());
        System.out.println(fpsStatus.getFWVersion());
        setMaxAttempts(3);
    }

    public void updateStatus(){
        fpsStatus.setSystemState(updateStateString());
    }

    public FPSSystemState getFpsStatus(){
        return fpsStatus;
    }

    public MeasurementData getDefaultMeasurementData(){
        MeasurementData boardData = new MeasurementData();
        String[] params = {"bSP-0","bSP-1","bSP-2","bSP-3","bSP-4","bSP-5","bSP-6","bSP-7","bSP-8"};
        String[] paramsResp = new String[params.length];
        for(int i=0;i<params.length;i++){
            writeString(params[i]);
            String response = readString();
            if(response.split(",")[0].equals("bOK") && response.contains(":")){
                response = response.split(":")[1];
                System.out.println(response);
            }else {
                response = "0";
                System.out.println(response);
            }
            paramsResp[i] = response;
        }
        boardData.setSetpoint0(Integer.parseInt(paramsResp[0]));
        boardData.setSetpoint1(Integer.parseInt(paramsResp[1]));
        boardData.setMeasurementTime(Integer.parseInt(paramsResp[2]));
        boardData.setFlushTime(Integer.parseInt(paramsResp[3]));
        boardData.setPumpSpeed(Integer.parseInt(paramsResp[4]));
        boardData.setFlushSpeed(Integer.parseInt(paramsResp[5]));
        boardData.setPurgeSpeed(Integer.parseInt(paramsResp[6]));
        boardData.setPurgeTime(Integer.parseInt(paramsResp[7]));
        boardData.setSETPOINTS(Integer.parseInt(paramsResp[8]));
        return boardData;
    }

    public void setMeasurementData(MeasurementData data){
        writeString("bSP-0="+data.getSetpoint0());
        String response = readString();
        System.out.println(data.getSetpoint0() + " - "+ response);
        writeString("bSP-1="+data.getSetpoint1());
        response = readString();
        System.out.println(data.getSetpoint1() + " - "+ response);
        writeString("bSP-2="+data.getMeasurementTime());
        response = readString();
        System.out.println(data.getMeasurementTime() + " - "+ response);
        writeString("bSP-3="+data.getFlushTime());
        response = readString();
        System.out.println(data.getFlushTime() + " - "+ response);
        writeString("bSP-4="+data.getPumpSpeed());
        response = readString();
        System.out.println(data.getPumpSpeed() + " - "+ response);
        writeString("bSP-5="+data.getFlushSpeed());
        response = readString();
        System.out.println(data.getFlushSpeed() + " - "+ response);
        writeString("bSP-6="+data.getPurgeSpeed());
        response = readString();
        System.out.println(data.getPurgeSpeed() + " - "+ response);
        writeString("bSP-7="+data.getPurgeTime());
        response = readString();
        System.out.println(data.getPurgeTime() + " - "+ response);
        writeString("bSP-8="+data.getSETPOINTS());
        response = readString();
        System.out.println(data.getSETPOINTS() + " - "+ response);
    }

    public FPSSystemState updateState(){
        FPSSystemState update = new FPSSystemState();
        writeString("bUS");
        String newState = readString().replace(" ","");
        String[] updateItems = newState.split(",");
        update.setSystemState(updateItems);
        return update;
    }

    public String[] updateStateString(){
        String response = writeRead("bUS");
        String[] updateItems;
        if(response.contains("bUS")){
            updateItems = response.replace(" ","").split(",");
        }else {
            updateItems = new String[0];
        }
        return updateItems;
    }

    public String getFWVersion(){
        return writeRead("bV");
    }

    //If system in unable to provide response will resend command
    private boolean validateResponse(String command, String inResponse){
        String response = writeRead(command);
        if(!response.contains(inResponse)){
            return false;
        }else {
            return true;
        }
    }

    private boolean validateCommandState(String command, String correctResponse, FPSStates desiredState){
        String response = writeRead(command);
        updateStatus();
        System.out.println("Validating Command: " + command + " - " + response + " = " + fpsStatus.getState().toString());
        if(response.contains(correctResponse)){
            if(fpsStatus.getState() == desiredState){
                return true;
            }else {
                return false;
            }
        }else if(response == ""){
            return validateCommandState(command,correctResponse,desiredState);
        }
        else {
            return false;
        }
    }

    public boolean sendMeasureCommand(){
       return validateResponse("bM","bOK");
    }

    public boolean sendFTIRDrain(){
        return validateCommandState("bM-e","bOK",FPSStates.FTIR_Drain);
    }

    public boolean sendFTIRMeasure(){
        return validateCommandState("bM-f","bOK",FPSStates.FTIR_Purge);
    }

    public boolean sendCancelCommand(){
        return validateResponse("bC","bOK");
    }

    public boolean sendTestCommand(){
        String response = writeRead("bFM");
        System.out.println(response);
        if(response.equals("Test,1")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendIceCal(){
        String response = writeRead("bIC");
        System.out.println(response);
        if(response.equals("IceCal,1")){
            return true;
        }else {
            return false;
        }
    }

    public boolean isDataPoint(){
        String response = writeRead("bDA");
        System.out.println(response);
        if(response.equals("Data Ready: 1")){
            return true;
        }else {
            return false;
        }
    }

    public FPSData getMeasurementData(){
        String response = writeRead("bD");
        FPSData dataIn = new FPSData();
        if(response.startsWith("KV")){
            dataIn.parseDataResponse(response);
        }
        return dataIn;
    }

    public SensorData[] getSensorData(int sqlID){
        String response = writeRead("bP").replace(










































































                "bOK,","");
        String[] points = response.split(",");
        SensorData[] data = new SensorData[points.length];
        if (data.length > 1) {
            int i = 0;
            for (String point : points) {
                data[i] = new SensorData(sqlID, point);
                i++;
            }
        }
        return data;
    }

    public boolean measure(){
        return  true;
    }
}
