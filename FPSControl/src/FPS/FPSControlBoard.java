package FPS;

import FPS.FTIR.*;
import Util.SerialPortHelper;
import org.json.simple.JSONObject;

import java.util.*;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

public class FPSControlBoard implements Runnable {

    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket pub = ctx.socket(SocketType.PUB);
    private String addressHeader = "tcp://*:";
    private int portRep = 5557;
    private int portPub = 5556;

    private SQLManager sqlManager = new SQLManager("FPSDatabase");
    private int sqlID;

    private CONTROLSTATE controlState = CONTROLSTATE.INIT;

    private FPSComm fpsComm;
    private FPSData measurementData = new FPSData();
    private ScanInfo scanInfo = new ScanInfo();
    private MeasurementData defaultData;

    private boolean running = true;
    private boolean sleeping = false;
    private int refreshTime = 1000;
    private Queue<ZMQCommand> commands= new LinkedList<>();

    private FTIRComm FTComm;
    private int FTIRMeasurementTime = 15;
    private FTIRData ftirData = new FTIRData();

    private int updateInterval = 5;
    private int updateCounter = 0;

    private TimerTask FTIRInitCheck;
    private Timer timer = new Timer();

    public enum CONTROLSTATE {
        INIT,
        WAITING,
        WAITINGTEST,
        MEASURING,
        FINISHED,
        CUSTOM_MEASUREMENT,
        ERROR,
        FTIRBG,
        FTIRPURGE,
        FTIR,
        FTIRDRAIN,
        ICECAL,
    }


    public void addCommand(ZMQCommand command){
        commands.add(command);
    }

    public boolean isSleeping(){
        return sleeping;
    }

    public CONTROLSTATE getControlState(){
        return controlState;
    }

    public void run(){
        boolean passedSetup = false;
        while (running){
            if(commands.size() > 0){
                executeCommand();
            }
            switch (controlState) {
                case WAITING:
                    doWaiting();
                    break;
                case WAITINGTEST:
                    doWaiting();
                    break;
                case INIT:
                    passedSetup = setUp();
                    break;
                case MEASURING:
                    doMeasuring();
                    break;
                case FINISHED:
                    doFinished();
                    break;
                case CUSTOM_MEASUREMENT:
                    doCustomMeasurement();
                    break;
                case ERROR:
                    doError();
                    break;
                case FTIRBG:
                    doFTIRBG();
                    break;
                case FTIR:
                    doFTIR();
                    break;
                case FTIRPURGE:
                    doFTIRPurge();
                    break;
                case FTIRDRAIN:
                    doFTIRDrain();
                    break;
                case ICECAL:
                    doIceCal();
                    break;
                default:
                    controlState = CONTROLSTATE.INIT;
                    break;
            }
            sleeping = true;
            try {
                Thread.sleep(refreshTime);
            }catch (InterruptedException e){
                System.out.println("Interrupted");
            }
            sleeping =  false;
        }
    }

    public void executeCommand(){
        ZMQCommand in = commands.poll();
        System.out.println(in.getCommand().toString());
        switch (in.getCommand()){
            case GETTEMP:
                System.out.println("ADC1 Temp: "+ String.valueOf(fpsComm.getFpsStatus().getAdcHeatx()));
                if(controlState == CONTROLSTATE.WAITING){
                    controlState = CONTROLSTATE.WAITINGTEST;
                }
                else if(controlState == CONTROLSTATE.WAITINGTEST){
                    controlState = CONTROLSTATE.WAITING;
                }
                break;
            case MEASURE:
                boolean started = false;
                //Store scan info for after the data has been received from the board
                scanInfo.setScanInfo(in.getData());
                scanInfo.setFWVersion(fpsComm.getFpsStatus().getFWVersion());
                if(scanInfo.getSimpleMeasurement()){
                    if(defaultData == null){
                        defaultData = fpsComm.getDefaultMeasurementData();
                    }
                    scanInfo.setMeasurementData(defaultData);
                }else {
                    fpsComm.setMeasurementData(scanInfo.getMeasurementData());
                }
                started = fpsComm.sendMeasureCommand();
                System.out.println("Command to measure accepted: " + started);
                if(started){
                    controlState = CONTROLSTATE.MEASURING;
                }else{
                    fpsComm.sendCancelCommand();
                    controlState = CONTROLSTATE.ERROR;
                }
                break;
            case RESET:
                boolean reset = false;
                reset = fpsComm.sendCancelCommand();
                break;
            case TESTMODE:
                boolean testMode = false;
                testMode = fpsComm.sendTestCommand();
                break;
            case CANCEL:
                boolean cancel = false;
                if(controlState == CONTROLSTATE.FTIR || controlState == CONTROLSTATE.FTIRBG){
                    FTComm.reset();
                    controlState = CONTROLSTATE.WAITING;
                }else if(controlState == CONTROLSTATE.MEASURING){
                    cancel = fpsComm.sendCancelCommand();
                }
                break;
            case EXIT:
                System.out.println("Exiting");
                cleanup();
                break;
            case MEASUREFTIR:
                scanInfo.setScanInfo(in.getData());
                if(FTComm.scanBackground()){
                    controlState=CONTROLSTATE.FTIRBG;
                }else {
                    controlState = CONTROLSTATE.ERROR;
                }
                break;
            case MEASUREFTIRBG:
                scanInfo.setScanInfo(in.getData());
                scanInfo.setFWVersion(fpsComm.getFpsStatus().getFWVersion());
                ftirData = new FTIRData();
                FTComm.scanBackground();
                while (FTComm.getStatus().getState() != FTIRState.MEASUREMENT_COMPLETE){FTComm.updateStatus();}
                ftirData = FTComm.getData();
                ftirData.saveSpectrumData(scanInfo.getName()+"_"+Integer.toString(10)+".csv");
                System.out.println("Done");
                break;
            case ICECAL:
                if(controlState.equals(CONTROLSTATE.WAITING)){
                    fpsComm.sendIceCal();
                    controlState = CONTROLSTATE.ICECAL;
                }else if(controlState.equals(CONTROLSTATE.ICECAL)){
                    fpsComm.sendIceCal();
                    controlState = CONTROLSTATE.FTIRDRAIN;
                }
                break;
            default:
                System.out.println("Command not understood");
                break;
        }
    }

    public void setScanInfo(String data){
        scanInfo.setScanInfo(data);
    }

    private void checkInit(){
        FTIRInitCheck = new TimerTask() {
            @Override
            public void run() {
                System.out.println("Re-checking FTIR Connection");
                if(FTComm.getStatus().getState()==FTIRState.UNINIT){
                    if(FTComm.initialize()){
                        controlState = CONTROLSTATE.WAITING;
                    }else {
                        controlState = CONTROLSTATE.ERROR;
                    }
                }
            }
        };
        timer.schedule(FTIRInitCheck,60000);
    }

    private boolean setUp(){
        System.out.println("Setting system up - 2");

        //Setup ZMQ publisher
        pub.bind("tcp://*:5556");
        sqlID = sqlManager.getNumberOfDataEntries();
        sqlID++;

        System.out.println("FPS INIT");
        fpsComm = new FPSComm();

        System.out.println("FTIR INIT");
        FTComm  = FTIRComm.getInstance(FTIR.Troposphere);
        FTComm.initialize();

        //Check for Success
        if(FTComm.getStatus().getState() != FTIRState.UNINIT && fpsComm.PORT_STATUS == SerialPortHelper.SERIAL_PORT_STATUS.SERIAL_PORT_READY){
            controlState = CONTROLSTATE.WAITING;
            return  true;
        }else {
            controlState = CONTROLSTATE.ERROR;
            return false;
        }
    }

    private void updateStatus(){
       fpsComm.updateStatus();
       FTComm.updateStatus();
       pub.send("Status",ZMQ.SNDMORE);
       pub.send(getStatusJson());
       printStatus();
       if(controlState == CONTROLSTATE.MEASURING || controlState == CONTROLSTATE.ICECAL) {
            updateCounter++;
            if (updateCounter > updateInterval) {
                sqlManager.addStatusEntry(fpsComm.getFpsStatus(),FTComm.getStatus(),scanInfo.getName(),sqlID);
                updateCounter = 0;
            }
       }
    }

    private void printStatus(){
        switch (controlState){
            case WAITING:
                System.out.println(fpsComm.getFpsStatus().getState());
                break;
            case FTIRBG:
            case FTIR:
                System.out.println(FTComm.getStatus().getState().toString() + " : " + Double.toString(FTComm.getStatus().getPercentFinished()));
                break;
            case FTIRPURGE:
            case FTIRDRAIN:
            case MEASURING:
                System.out.println(fpsComm.getFpsStatus().getState().toString() + " : " + Integer.toString(fpsComm.getFpsStatus().getElapsedTimeSeconds()));
                break;
            case ERROR:
                System.out.println(fpsComm.getFpsStatus().getState().toString() + " : " + FTComm.getStatus().getState().toString());
        }
    }

    private void doWaiting(){
        updateStatus();
        if(fpsComm.getFpsStatus().getState() == FPSStates.Finished){
            doFinished();
        }
        if(fpsComm.getFpsStatus().getState() != FPSStates.Waiting){
            fpsComm.sendCancelCommand();
            controlState = CONTROLSTATE.ERROR;
        }
     }

    private void doMeasuring(){
        updateStatus();
        if(fpsComm.getFpsStatus().getState() == FPSStates.Finished){
            controlState = CONTROLSTATE.FINISHED;
        }
    }

    private void doFinished(){
        //Get and report Data
        SensorData[] dataPoints = fpsComm.getSensorData(-1);
        measurementData = fpsComm.getMeasurementData();
        //Need to set the scan info after the measurementData has been parsed by the comm string
        scanInfo.setFWVersion(fpsComm.getFpsStatus().getFWVersion());
        measurementData.setScanInfo(scanInfo);
        System.out.println("Measurement Finished: KV40 calculated - " + measurementData.getKV40Walther().toString());
        sqlManager.addDataEntry(measurementData);
        if(ftirData != null) {

            sqlManager.addFTIRData(sqlID, ftirData.toJSON());
        }
        sqlID = sqlManager.getNumberOfDataEntries();
        if(dataPoints.length > 1) {
            for (SensorData data : dataPoints) {
                data.setSqlID(sqlID);
            }
            sqlManager.addSensorEntry(dataPoints);
        }
        //Publish Data Points
        measurementData.setSqlID(sqlID);
        scanInfo.setFWVersion(fpsComm.getFpsStatus().getFWVersion());
        measurementData.setScanInfo(scanInfo);
        pub.send("Data",ZMQ.SNDMORE);
        pub.send(measurementData.getFPSDataJSON().toString(),ZMQ.SNDMORE);
        pub.send(ftirData.toJSON());
        sqlID++;
        updateStatus();
        controlState = CONTROLSTATE.WAITING;
    }

    private void doIceCal(){
        updateStatus();
        if(fpsComm.isDataPoint()){
            SensorData[] dataPoints = fpsComm.getSensorData(-1);
            sqlID = sqlManager.getNumberOfDataEntries();
            if(dataPoints.length > 1) {
                for (SensorData data : dataPoints) {
                    data.setSqlID(sqlID);
                    System.out.println("Sensor Temperature: "+data.getpTemperature().toString());
                }
                sqlManager.addSensorEntry(dataPoints);
            }
        }
    }

    private void doCustomMeasurement(){
        //Lot of custom stuff needed here
        //define data type and interface
    }

    private void doError(){
        //ResetBoard and go back to waiting
        //How should user see this?
        //Alert box - clear on close ???
        updateStatus();
        if(fpsComm.getFpsStatus().getState()==FPSStates.Waiting){
            controlState = CONTROLSTATE.WAITING;
        }
        if(fpsComm.getFpsStatus().getState()==FPSStates.Finished){
            fpsComm.getMeasurementData();
        }
    }

    private void doFTIRBG(){
        updateStatus();
        if (FTComm.getStatus().getState() == FTIRState.MEASURING_BACKGROUND_COMPLETE){
            ftirData = new FTIRData();
            ftirData.setBackgroundData(FTComm.getData().getBackgroundData());
            fpsComm.sendFTIRMeasure();
            controlState=CONTROLSTATE.FTIRPURGE;
        }
    }

    private void doFTIR(){
        updateStatus();
        if (FTComm.getStatus().getState() == FTIRState.MEASUREMENT_COMPLETE){
            ftirData.setSpectrumData(FTComm.getData().getSpectrumData());
            if(!scanInfo.getDoFPS()){
                System.out.println("FTIR DRAIN");
                if(fpsComm.sendFTIRDrain()){
                    controlState = CONTROLSTATE.FTIRDRAIN;
                }else {
                    controlState = CONTROLSTATE.ERROR;
                }
            }else {
                if(fpsComm.sendMeasureCommand()){
                    controlState = CONTROLSTATE.MEASURING;
                }else {
                    controlState = CONTROLSTATE.ERROR;
                }
            }
        }
    }

    private void doFTIRPurge(){
        updateStatus();
        if(fpsComm.getFpsStatus().getState() == FPSStates.FTIR_InPosition) {
            FTComm.scan();
            controlState = CONTROLSTATE.FTIR;
        }
    }

    private void doFTIRDrain(){
        updateStatus();
        if(fpsComm.getFpsStatus().getState() == FPSStates.Finished) {
            controlState = CONTROLSTATE.FINISHED;
        }
    }

    private void cleanup(){
        pub.close();
        running = false;
    }

    private String getStatusJson(){
        JSONObject temp = new JSONObject();
        temp.put("controlState",controlState.toString());
        temp.put("fps",fpsComm.getFpsStatus().getJsonString());
        temp.put("ftir",FTComm.getStatus().getJsonString());
        return temp.toString();
    }
}
