package FPS;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FPSData {

    private int sqlID;
    private ScanInfo scanInfo;
    private Integer targetTemp0;
    private Double pViscosity0;
    private Double pDensity0;
    private Double pDielectricStrength0;
    private Double pTemperature0;
    private Integer pStatus0;
    private Integer targetTemp1;
    private Double pViscosity1;
    private Double pDensity1;
    private Double pDielectricStrength1;
    private Double pTemperature1;
    private Integer pStatus1;
    private Integer pStatusSystem;
    private Double KV40Walther;

    public Integer getTargetTemp0() {
        return targetTemp0;
    }

    public Double getpViscosity0() {
        return pViscosity0;
    }

    public Double getpDensity0() {
        return pDensity0;
    }

    public Double getpDielectricStrength0() {
        return pDielectricStrength0;
    }

    public Double getpTemperature0() {
        return pTemperature0;
    }

    public Integer getpStatus0() {
        return pStatus0;
    }

    public Integer getTargetTemp1() {
        return targetTemp1;
    }

    public Double getpViscosity1() {
        return pViscosity1;
    }

    public Double getpDensity1() {
        return pDensity1;
    }

    public Double getpDielectricStrength1() {
        return pDielectricStrength1;
    }

    public Double getpTemperature1() {
        return pTemperature1;
    }

    public Integer getpStatus1() {
        return pStatus1;
    }

    public Double getKV40Walther() {
        return KV40Walther;
    }


    public Integer getpStatusSystem() {
        return pStatusSystem;
    }

    public void setpStatusSystem(Integer pStatusSystem) {
        this.pStatusSystem = pStatusSystem;
    }

    public int getSqlID() {
        return sqlID;
    }

    public void setSqlID(int sqlID) {
        this.sqlID = sqlID;
    }


    public FPSData(){
        targetTemp0 = 0;
        pViscosity0 = 0.0;
        pDensity0 = 0.00;
        pDielectricStrength0 = 0.00;
        pTemperature0 = 0.00;
        pStatus0 = 0;
        targetTemp1 = 0;
        pViscosity1 = 0.00;
        pDensity1 = 0.0;
        pDielectricStrength1 = 0.00;
        pTemperature1 = 0.00;
        pStatus1 = 0;
        pStatusSystem = 0;
        KV40Walther = 00.0;
        sqlID = -1;
        scanInfo = new ScanInfo();
    }

    public FPSData(ScanInfo scanInfo,
           Integer targetTemp0,
            Double pViscosity0,
            Double pDensity0,
            Double pDielectricStrength0,
            Double pTemperature0,
            Integer pStatus0,
            Integer targetTemp1,
            Double pViscosity1,
            Double pDensity1,
            Double pDielectricStrength1,
            Double pTemperature1,
            Integer pStatus1,
            Integer pStatusSystem,
            Double KV40Walther){
        this.targetTemp0 = targetTemp0;
        this.pViscosity0 = pViscosity0;
        this.pDensity0 = pDensity0;
        this.pDielectricStrength0 = pDielectricStrength0;
        this.pTemperature0 = pTemperature0;
        this.pStatus0 = pStatus0;
        this.targetTemp1 = targetTemp1;
        this.pViscosity1 = pViscosity1;
        this.pDensity1 = pDensity1;
        this.pDielectricStrength1 = pDielectricStrength1;
        this.pTemperature1 = pTemperature1;
        this.pStatus1 = pStatus1;
        this.pStatusSystem = pStatusSystem;
        this.KV40Walther = KV40Walther;
        this.scanInfo = scanInfo;
    }

    public FPSData(ScanInfo scanInfo,
                   Integer targetTemp0,
                   Double pViscosity0,
                   Double pDensity0,
                   Double pDielectricStrength0,
                   Double pTemperature0,
                   Integer pStatus0,
                   Integer targetTemp1,
                   Double pViscosity1,
                   Double pDensity1,
                   Double pDielectricStrength1,
                   Double pTemperature1,
                   Integer pStatus1,
                   Integer pStatusSystem,
                   Double KV40Walther,
                   Integer sqlID){
        this.targetTemp0 = targetTemp0;
        this.pViscosity0 = pViscosity0;
        this.pDensity0 = pDensity0;
        this.pDielectricStrength0 = pDielectricStrength0;
        this.pTemperature0 = pTemperature0;
        this.pStatus0 = pStatus0;
        this.targetTemp1 = targetTemp1;
        this.pViscosity1 = pViscosity1;
        this.pDensity1 = pDensity1;
        this.pDielectricStrength1 = pDielectricStrength1;
        this.pTemperature1 = pTemperature1;
        this.pStatus1 = pStatus1;
        this.pStatusSystem = pStatusSystem;
        this.KV40Walther = KV40Walther;
        this.scanInfo = scanInfo;
        this.sqlID = sqlID;
    }

    public void setScanInfo(ScanInfo info){
        scanInfo = info;
    }

    public ScanInfo getScanInfo(){
        return scanInfo;
    }

    public void parseDataResponse(String data){
        String[] dataArray = data.split(",");
        try{
            targetTemp0 = Integer.valueOf(dataArray[0].replace("KV",""));
            pViscosity0 = Double.valueOf(dataArray[1]);
            pDensity0 = Double.valueOf(dataArray[2]);
            pDielectricStrength0 = Double.valueOf(dataArray[3]);
            pTemperature0 = Double.valueOf(dataArray[4]);
            if(dataArray[5].equals("0x8")){
                pStatus0 = 8;
            }else {
                pStatus0 = Integer.valueOf(dataArray[5]);
            }
            targetTemp1 = Integer.valueOf(dataArray[6].replace("KV",""));
            pViscosity1 = Double.valueOf(dataArray[7]);
            pDensity1 = Double.valueOf(dataArray[8]);
            pDielectricStrength1 = Double.valueOf(dataArray[9]);
            pTemperature1 = Double.valueOf(dataArray[10]);
            if(dataArray[11].equals("0x8")){
                pStatus1 = 8;
            }else {
                pStatus1 = Integer.valueOf(dataArray[5]);
            }
            pStatusSystem = Integer.valueOf(dataArray[12]);
            if(dataArray.length > 14){
                KV40Walther = Double.valueOf(dataArray[14]);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public JSONObject getFPSDataJSON(){
        JSONObject json = new JSONObject();
        json.put("targetTemp0", targetTemp0);
        json.put("pViscosity0",pViscosity0);
        json.put("pDensity0",pDensity0);
        json.put("pDielectricStrength0",pDielectricStrength0);
        json.put("pTemperature0",pTemperature0);
        json.put("pStatus0",pStatus0);
        json.put("targetTemp1",targetTemp1);
        json.put("pViscosity1",pViscosity1);
        json.put("pDensity1",pDensity1);
        json.put("pDielectricStrength1",pDielectricStrength1);
        json.put("pTemperature1",pTemperature1);
        json.put("pStatus1",pStatus1);
        json.put("pSystemStatus",pStatusSystem);
        json.put("KV40Walther",KV40Walther);
        json.put("scanInfo",scanInfo.getScanInfoJSON().toString());
        json.put("sqlID",sqlID);
        return json;
    }

    public void parseJSONData(String jsonData){
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try{
            json = (JSONObject) parser.parse(jsonData);
        }catch (ParseException e){
            System.out.println("Could not parse json");
        }
        for(Object key : json.keySet()){
            try{
                switch (key.toString()){
                    case"targetTemp0":{ targetTemp0 = Integer.valueOf(json.get(key).toString());}break;
                    case"pViscosity0":{pViscosity0 = (Double) json.get(key);}break;
                    case"pDensity0":{pDensity0 = (Double) json.get(key);}break;
                    case"pDielectricStrength0":{pDielectricStrength0= (Double) json.get(key);}break;
                    case"pTemperature0":{pTemperature0 = (Double) json.get(key);}break;
                    case"pStatus0":{pStatus0 = Integer.valueOf(json.get(key).toString());}break;
                    case"targetTemp1":{targetTemp1 = Integer.valueOf(json.get(key).toString());}break;
                    case"pViscosity1":{pViscosity1 = (Double) json.get(key);}break;
                    case"pDensity1":{pDensity1 = (Double) json.get(key);}break;
                    case"pDielectricStrength1":{pDielectricStrength1 = (Double) json.get(key);}break;
                    case"pTemperature1":{pTemperature1 = (Double) json.get(key);}break;
                    case"pStatus1":{pStatus1 = Integer.valueOf(json.get(key).toString());}break;
                    case"pSystemStatus":{pStatusSystem = Integer.valueOf(json.get(key).toString());}break;
                    case"KV40Walther":{KV40Walther = (Double) json.get(key);}break;
                    case"scanInfo":{
                        scanInfo.setScanInfo(json.get(key).toString());
                    }
                    break;
                    case"sqlID":{sqlID=Integer.valueOf(json.get(key).toString());}break;
                    default:{System.out.println("Data Json Parse - Did not expect " + key.toString() + " : " + json.get(key).toString());}break;
                }
            }catch (Exception e){
                System.out.println("Parse Issue -> " + key + " : " + json.get(key).toString());
            }
        }

    }

    public Double getkV0(){
        Double kv = 0.0;
        try{
            kv = pViscosity0/pDensity0;
        }catch (ArithmeticException ae){
            kv = 0.0;
        }
        if (Double.isNaN(kv)){
            kv = 0.0;
        }
        return kv;
    }

    public Double getkV1(){
        Double kv = 0.0;
        try{
            kv = pViscosity1/pDensity1;
        }catch (ArithmeticException ae){
            kv = 0.0;
        }catch (Exception e){
            kv = 0.0;
        }
        if (Double.isNaN(kv)){
            kv = 0.0;
        }
        return kv;
    }

    public JSONObject getDataExportJson(){
        JSONObject data = new JSONObject();
        data.put("scanID",Integer.toString(sqlID));
        data.put("kv100KinematicViscosity",Double.toString(getkV0()));
        data.put("kv100dynamicViscosity",Double.toString(pViscosity0));
        data.put("kv100density",Double.toString(pDensity0));
        data.put("kv100dielectricConstant",Double.toString(pDielectricStrength0));
        data.put("kv100temp",Double.toString(pTemperature0));
        data.put("kv100errorCode",Integer.toString(pStatus0));
        data.put("kv40KinematicViscosity",Double.toString(getkV1()));
        data.put("kv40dynamicViscosity",Double.toString(pViscosity1));
        data.put("kv40density",Double.toString(pDensity1));
        data.put("kv40dielectricConstant",Double.toString(pDielectricStrength1));
        data.put("kv40temp",Double.toString(pTemperature1));
        data.put("kv40errorCode",Integer.toString(pStatus1));
        data.put("systemErrorCode",Integer.toString(pStatusSystem));
        return  data;
    }
}
