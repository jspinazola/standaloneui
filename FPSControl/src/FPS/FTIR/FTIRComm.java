package FPS.FTIR;

public class FTIRComm {

    private FTIR ftir;
    protected FTIRStatus status;

    private static volatile FTIRComm singleton = null;

    public static FTIRComm getInstance(FTIR ftir) {
        if (singleton == null) {
            synchronized (NeospectraComm.class) {
                if (singleton == null) {
                    switch (ftir){
                        case NONE:
                            singleton = new FTIRComm(FTIR.NONE);
                            break;
                        case Neospectra:
                            singleton = new NeospectraComm(false);
                            break;
                        case Troposphere:
                            singleton = new TroposphereComm(FTIR.Troposphere);
                            break;
                    }
                }
            }
        }
        return singleton;
    }

    public FTIRComm(FTIR ftir){
        this.ftir = ftir;
    }

    protected void setFtir(FTIR ftir){this.ftir = ftir;}
    public FTIR getFtir(){return ftir;}

    public FTIRStatus getStatus(){return status;}
    public Boolean isWarm(){return status.isWarm();}

    //Default override in specific comms
    public boolean scan(){return false;}
    public boolean scanBackground(){return false;}
    public boolean initialize(){return false;}
    public boolean updateStatus(){return false;}
    public  boolean reset(){return false;}
    public FTIRData getData(){return new FTIRData();}

}