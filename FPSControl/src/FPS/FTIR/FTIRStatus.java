package FPS.FTIR;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE
)

public class FTIRStatus {

    @JsonProperty
    private FTIRState state = FTIRState.UNINIT;
    @JsonProperty
    private Long warmTime = 0L;
    @JsonProperty
    private Double percentFinished = 0.0;
    @JsonProperty
    private String message = "";
    private FTIRData data = new FTIRData();

    public FTIRStatus(){}

    @JsonCreator
    public FTIRStatus(
            @JsonProperty("state") FTIRState state,
            @JsonProperty("warmTime") Long warmTime,
            @JsonProperty("percentFinished") Double percentFinished,
            @JsonProperty("message") String message
    ){
        this.state = state;
        this.warmTime = warmTime;
        this.percentFinished = percentFinished;
        this.message = message;
    }

    public Long getWarmTime() {
        return warmTime;
    }
    public void setWarmTime(Long warmTime) {
        this.warmTime = warmTime;
    }

    public String getMessage(){return message;}
    public void setMessage(String message){this.message = message;}

    public boolean isWarm(){
        if(state != FTIRState.UNINIT || state != FTIRState.WARMUP || state != FTIRState.ERROR){
            return true;
        }else {
            return false;
        }
    }

    public Double getPercentFinished() {
        return percentFinished;
    }
    public void setPercentFinished(Double percentFinished) {
        this.percentFinished = percentFinished;
    }

    public FTIRData getData() {
        return data;
    }
    public void setData(FTIRData data) {
        this.data = data;
    }

    public void setFTIRState(FTIRState state){this.state = state;}
    public FTIRState getState(){return state;}

    public String getJsonString(){
        ObjectMapper ob = new ObjectMapper();
        String SystemState = "";
        try{
            SystemState = ob.writeValueAsString(this);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return SystemState;
    }

    public void updateFromJsonString(String json){
        ObjectMapper ob = new ObjectMapper();
        ObjectReader reader = ob.readerForUpdating(this);
        try {
            reader.readValue(json);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
}
