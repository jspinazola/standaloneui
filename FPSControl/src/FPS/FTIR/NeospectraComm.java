package FPS.FTIR;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sws.p3AppManager_micro.p3AppManagerImpl;
import sws.p3AppManager_micro.p3AppManager_micro;
import sws.p3AppManager_micro.utils.p3AppManagerNotification;
import sws.p3AppManager_micro.utils.p3Enumerations;
import sws.spectromost.UserInterface;

public class NeospectraComm extends FTIRComm implements Observer {

    private static final Logger LOG = LoggerFactory.getLogger(NeospectraComm.class.getName());
    private static volatile NeospectraComm singleton = null;
    private static p3AppManager_micro manager;
    private boolean isBoardInitialized = false;
    private boolean hasBackgroundSpec = false;
    private boolean spectrum = false;
    private boolean isWarm = false;
    private final int maxAttempts = 5;
    private boolean isDualBeam = true;
    private MeasurementStatus status = null;
    private FTIRData results;
    private int repeats = 0;
    private int measurementTime;
    private boolean isRunningContinuous;
    private Timer timer = new Timer();
    private TimerTask delayWarmup;

    public NeospectraComm(boolean _isDualBeam) {
        super(FTIR.Neospectra);
        manager = new p3AppManagerImpl();
        manager.addObserver(this);
        status = MeasurementStatus.IDLE;
        isDualBeam = _isDualBeam;
    }

    public static NeospectraComm getInstance(boolean _isDualBeam) {
        if (singleton == null) {
            synchronized (NeospectraComm.class) {
                if (singleton == null) {
                    singleton = new NeospectraComm(_isDualBeam);
                }
            }
        }
        return singleton;
    }

    /**
     * Attempts to initialize the FTIR board.
     * @return true when connection is successful, false otherwise.
     */
    public boolean initializeDevice() {
        int failures = 0;
        while (failures < maxAttempts) {
            //TODO Need to try this several times in a row before failing.
            p3Enumerations.p3AppManagerStatus appManagerStatus = manager.initializeCore(new String[0]);
            if (p3Enumerations.p3AppManagerStatus.NO_ERROR != appManagerStatus) {
                //TODO Why does this initialization not return?
                LOG.error("1 - Error encountered while trying to run current initialize: {}", UserInterface.convertErrorCodesToMessages(appManagerStatus));
            }
            appManagerStatus = manager.checkDeviceStatus(new String[0]);
            if (p3Enumerations.p3AppManagerStatus.NO_ERROR == appManagerStatus) {
                LOG.debug("Successfully initialized FTIR Sensor");
                System.out.println("Starting Warmup");
                warmup();
                return true;
            }
            failures++;
            LOG.error("2 - Error encountered (failure #{}) while trying to run current initialize: {}", failures, UserInterface.convertErrorCodesToMessages(appManagerStatus));
        }
        return false;
    }

    public boolean isBoardInit() {
        return isBoardInitialized;
    }

    public boolean isSpectrum() {
        return spectrum;
    }

    public boolean isHasBackgroundSpec(){return hasBackgroundSpec;}

    public p3AppManager_micro getP3Manager() {
        return manager;
    }

    public MeasurementStatus getMStatus(){return status;}

    /**
     * Runs sample spectroscopy.
     * @param n is the run time in seconds
     */
    public void runSpectrum(int n) {
        //NOTE: Background duration must equal measurement duration.
        if (isBoardInitialized) {
            if (hasBackgroundSpec) {
                p3Enumerations.p3AppManagerStatus appManagerStatus = manager.runSpec(String.valueOf(n * 1000.0D), "true");
                if (p3Enumerations.p3AppManagerStatus.NO_ERROR != appManagerStatus) {
                    LOG.error("2. Spectrum measurement failed to start: {}", UserInterface.convertErrorCodesToMessages(appManagerStatus));
                }
            }
        }
    }

    public void runBackgroundSpectrum(int n){
        hasBackgroundSpec = false;
        if(isBoardInitialized) {
            if (hasBackgroundSpec == false) {
                LOG.debug("Starting runBackgroundSpec on thread {}", Thread.currentThread().getName());
                //This operation is non-blocking so we need to wait until it is complete by sleeping.
                p3Enumerations.p3AppManagerStatus appManagerStatus = manager.runSpec(String.valueOf(n * 1000.0D), "false");
                //This only indicates that it failed to start, not that it ended or was interrupted etc.
                if (p3Enumerations.p3AppManagerStatus.NO_ERROR != appManagerStatus) {
                    LOG.error("Background measurement failed to start: {}", UserInterface.convertErrorCodesToMessages(appManagerStatus));
                }
            }
        }
    }

    public void runRepeatSpectrum(int time, int repeats){
        spectrum = false;
        this.repeats = repeats;
        this.measurementTime = time;
        results = new FTIRData();
        if(!isRunningContinuous){
            runInterSpec(time);
        }
    }

    @Override
    public boolean scan(){
        runRepeatSpectrum(20,10);
        if(p3Enumerations.p3AppManagerStatus.NO_ERROR != manager.checkDeviceStatus()){
            return false;
        }else {
            return true;
        }
    }

    public void runRestoreDefaults(){
        manager.restoreDefaultSettings(new String[0]);
    }

    public double[][] getSpectrumData() {
        return manager.getSpecData();
    }

    public void runInterSpec(int n){
        spectrum = false;
        if(isBoardInitialized){
            LOG.debug("Starting runInterSpec on thread {}", Thread.currentThread().getName());
            //This operation is non-blocking so we need to wait until it is complete by sleeping.
            p3Enumerations.p3AppManagerStatus appManagerStatus = manager.runInterSpec(Integer.toString(n*1000), "0", "2", "-1", "0", "0","1");
            //This only indicates that it failed to start, not that it ended or was interrupted etc.
            if (p3Enumerations.p3AppManagerStatus.NO_ERROR != appManagerStatus) {
                LOG.error("InterSpec measurement failed to start: {}", UserInterface.convertErrorCodesToMessages(appManagerStatus));
            }
        }

    }

    @Override
    public boolean initialize(){
        return true;
    }

    @Override
    public boolean updateStatus(){
        return true;
    }

    @Override
    public boolean reset(){
        return true;
    }

    private void runContinuousSpectrum(){
        if(isBoardInitialized){
            LOG.debug("Starting runInterSpec on thread {}", Thread.currentThread().getName());
            p3Enumerations.p3AppManagerStatus appManagerStatus = manager.runInterSpec("2000", "0", "2", "-1", "0", "1","1");
            //This only indicates that it failed to start, not that it ended or was interrupted etc.
            if (p3Enumerations.p3AppManagerStatus.NO_ERROR != appManagerStatus) {
                LOG.error("InterSpec measurement failed to start: {}", UserInterface.convertErrorCodesToMessages(appManagerStatus));
                isRunningContinuous = false;
            } else {
                isRunningContinuous = true;
            }
        }
    }

    public FTIRData getInterSpecData() {return results;}
    public double[][] getRunData() {return results.getSimpleData();}

    private void warmup(){
        delayWarmup = new TimerTask() {
            @Override
            public void run() {
                NeospectraComm comm = NeospectraComm.getInstance(false);
                comm.setWarm(true);
                System.out.println("System Warmed!");
            }
        };
        runContinuousSpectrum();
        timer.schedule(delayWarmup,600000);
        //timer.schedule(delayWarmup,30000);
    }

    public boolean getIsWarm(){return isWarm;}
    public void setWarm(boolean warm){this.isWarm = warm;}
    public int getRepeats(){return repeats;}

    public Long getWarmTime(){
        if(isBoardInitialized) {
            Date date = new Date();
            Date now = new Date();
            date.setTime(delayWarmup.scheduledExecutionTime());
            long temp = TimeUnit.SECONDS.convert(date.getTime() - now.getTime(), TimeUnit.SECONDS);
            temp = temp / 1000;
            if (temp < 0) {
                temp = 0;
            }
            return temp;
        }else {
            return 0L;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        LOG.debug("FTIRCommunicator updated with Observable {}", o);
        if (arg instanceof p3AppManagerNotification) {
            p3AppManagerNotification managerNotification = (p3AppManagerNotification) arg;
            switch (managerNotification.getAction()) {
                case 0:
                    if (managerNotification.getStatus() == 0) {
                        isBoardInitialized = true;
                        LOG.debug("INIT_COMPLETE");
                        status = (MeasurementStatus.INIT_COMPLETE);
                    }
                    else {
                        isBoardInitialized = false;
                        LOG.debug("INIT_FAILED");
                        status = (MeasurementStatus.INIT_FAILED);
                    }
                    break;
                case 1:
                    if (managerNotification.getStatus() == 0) {
                        if(repeats == 0){
                            spectrum = true;
                            LOG.debug("INTERFEROGRAM_COMPLETE");
                            status =(MeasurementStatus.INTERFEROGRAM_COMPLETE);
                            runContinuousSpectrum();

                        }else{
                            --repeats;
                            results.setSpectrumData(manager.getInterSpecData());
                            runInterSpec(measurementTime);
                        }
                    }
                    else {
                        spectrum = false;
                        LOG.debug("INTERFEROGRAM_FAILED");
                        status =(MeasurementStatus.INTERFEROGRAM_FAILED);
                    }
                case 2:
                    if (managerNotification.getStatus() == 0) {
                        LOG.debug("BACKGROUND COMPLETE. hasBGSpect true");
                        status = MeasurementStatus.BACKGROUND_COMPLETE;
                        hasBackgroundSpec = true;
                    }
                    else {
                        LOG.debug("BACKGROUND FAILED. hasBGSpect false");
                        status =(MeasurementStatus.BACKGROUND_FAILED);
                        hasBackgroundSpec = false;
                    }
                    break;
                case 3:
                    if (managerNotification.getStatus() == 0) {
                        spectrum = true;
                        hasBackgroundSpec = false;
                        LOG.debug("SPECTRUM_COMPLETE");
                        status =(MeasurementStatus.SPECTRUM_COMPLETE);
                    }
                    else {
                        spectrum = false;
                        hasBackgroundSpec = false;
                        LOG.debug("SPECTRUM_FAILED");
                        status =(MeasurementStatus.SPECTRUM_FAILED);
                    }
                    break;
                case 30:
                    if (managerNotification.getStatus() == 0) {
                        status =(MeasurementStatus.WAVELENGTH_CALIBRATIONBG_COMPLETE);
                    }
                    else {
                        status =(MeasurementStatus.WAVELENGTH_CALIBRATIONBG_FAILED);
                    }
                    break;
            }
        }
    }

}
