package FPS;

import FPS.FTIR.FTIRData;
import FPS.FTIR.FTIRStatus;
import org.json.simple.JSONObject;

import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

public class SQLManager {

    private String locationWindows = "jdbc:sqlite:C:/XOSGit/DatabaseFiles/sqlite/db/";
    private String locationLinux = "jdbc:sqlite:/XOS/Data/DatabaseFiles/sqlite/db/";
    String location = "";
    private String dbName;
    private String DataTable = "DataTable";
    private String StatusTable = "StatusTable";
    private String SensorTable = "SensorTable";

    public SQLManager() {
        String osType = System.getProperty("os.name");
        if(osType.startsWith("Windows")){
            System.out.println("Windows");
            location = locationWindows;
        }else {
            System.out.println("Linux");
            location = locationLinux;
        }
    }

    public SQLManager(String databaseName) {
        this.dbName = databaseName;
        String osType = System.getProperty("os.name");
        if(osType.startsWith("Windows")){
            System.out.println("Windows");
            location = locationWindows;
        }else {
            System.out.println("Linux");
            location = locationLinux;
        }
    }

    private Connection connect() {
        // SQLite connection string
        String url = location + dbName;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public void createNewDatabase(String fileName) {

        String url = location + fileName;
        dbName = fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createNewDataTable(String tableName) {
        // SQLite connection string
        String url = location + dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n"
                + "    id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "    time DATETIME DEFAULT CURRENT_TIMESTAMP,\n"
                + "    sampleName text NOT NULL,\n"
                + "    targetTemp0 integer NOT NULL,\n"
                + "    pViscosity0 real NOT NULL,\n"
                + "    pDensity0 real NOT NULL,\n"
                + "    pDielectricStrength0 real NOT NULL,\n"
                + "    pTemperature0 real NOT NULL,\n"
                + "    pStatus0 integer NOT NULL,\n"
                + "    targetTemp1 real NOT NULL,\n"
                + "    pViscosity1 real NOT NULL,\n"
                + "    pDensity1 real NOT NULL,\n"
                + "    pDielectricStrength1 real NOT NULL,\n"
                + "    pTemperature1 real NOT NULL,\n"
                + "    pStatus1 integer NOT NULL,\n"
                + "    pStatusSystem integer NOT NULL,\n"
                + "    KV40Walther real NOT NULL,\n"
                + "    simpleMeasurement boolean NOT NULL,\n"
                + "    setpoint0 integer NOT NULL,\n"
                + "    setpoint1 integer NOT NULL,\n"
                + "    measurementTime integer NOT NULL,\n"
                + "    pumpSpeed integer NOT NULL,\n"
                + "    flushSpeed integer NOT NULL,\n"
                + "    flushTime integer NOT NULL,\n"
                + "    purgeSpeed integer NOT NULL,\n"
                + "    purgeTime integer NOT NULL,\n"
                + "    SETPOINTS integer NOT NULL,\n"
                + "    fwVersion text,\n"
                + "    spectrum text,\n"
                + "    uploaded integer\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createNewStatusTable(String tableName) {
        // SQLite connection string
        String url = location + dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n"
                + "    id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "    time DATETIME DEFAULT CURRENT_TIMESTAMP,\n"
                + "    dataID integer NOT NULL,\n"
                + "    sampleName text NOT NULL,\n"
                + "     state text NOT NULL,\n"
                + "     elapsedTime integer NOT NULL,\n"
                + "     measurementPoint integer NOT NULL,\n"
                + "     adcSensor integer NOT NULL,\n"
                + "     adcHeatx integer NOT NULL,\n"
                + "     fluidTemp integer NOT NULL,\n"
                + "     PWM1 integer NOT NULL,\n"
                + "     PWM2 integer NOT NULL,\n"
                + "     fanStatus integer NOT NULL,\n"
                + "     tempSetpoint integer NOT NULL,\n"
                + "     corrTempSetpoint integer NOT NULL,\n"
                + "     pumpSpeed integer NOT NULL\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createNewSensorTable(String tableName) {
        // SQLite connection string
        String url = location + dbName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n"
                + "    id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "    time DATETIME DEFAULT CURRENT_TIMESTAMP,\n"
                + "    dataID integer NOT NULL,\n"
                + "    targetTemp integer NOT NULL,\n"
                + "    pViscosity real NOT NULL,\n"
                + "    pDensity real NOT NULL,\n"
                + "    pDielectricStrength real NOT NULL,\n"
                + "    pTemperature real NOT NULL,\n"
                + "    pStatus integer NOT NULL\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addDataEntry(FPSData data) {

        String sql = "INSERT INTO " + DataTable + " (\n"
                + "    sampleName,\n"
                + "    targetTemp0,\n"
                + "    pViscosity0,\n"
                + "    pDensity0,\n"
                + "    pDielectricStrength0,\n"
                + "    pTemperature0,\n"
                + "    pStatus0,\n"
                + "    targetTemp1,\n"
                + "    pViscosity1,\n"
                + "    pDensity1,\n"
                + "    pDielectricStrength1,\n"
                + "    pTemperature1,\n"
                + "    pStatus1,\n"
                + "    pStatusSystem,\n"
                + "    KV40Walther,\n"
                + "    simpleMeasurement,\n"
                + "    setpoint0,\n"
                + "    setpoint1,\n"
                + "    measurementTime,\n"
                + "    flushTime,\n"
                + "    pumpSpeed,\n"
                + "    flushSpeed,\n"
                + "    purgeSpeed,\n"
                + "    purgeTime,\n"
                + "    SETPOINTS,\n"
                + "    fwVersion,\n"
                + "    uploaded\n"
                + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, data.getScanInfo().getName());
            pstmt.setInt(2, data.getTargetTemp0());
            pstmt.setDouble(3, data.getpViscosity0());
            pstmt.setDouble(4, data.getpDensity0());
            pstmt.setDouble(5, data.getpDielectricStrength0());
            pstmt.setDouble(6, data.getpTemperature0());
            pstmt.setInt(7, data.getpStatus0());
            pstmt.setInt(8, data.getTargetTemp1());
            pstmt.setDouble(9, data.getpViscosity1());
            pstmt.setDouble(10, data.getpDensity1());
            pstmt.setDouble(11, data.getpDielectricStrength1());
            pstmt.setDouble(12, data.getpTemperature1());
            pstmt.setInt(13, data.getpStatus1());
            pstmt.setInt(14, data.getpStatusSystem());
            pstmt.setDouble(15, data.getKV40Walther());
            pstmt.setBoolean(16, data.getScanInfo().getSimpleMeasurement());
            pstmt.setInt(17, data.getScanInfo().getMeasurementData().getSetpoint0());
            pstmt.setInt(18, data.getScanInfo().getMeasurementData().getSetpoint1());
            pstmt.setInt(19, data.getScanInfo().getMeasurementData().getMeasurementTime());
            pstmt.setInt(20, data.getScanInfo().getMeasurementData().getFlushTime());
            pstmt.setInt(21, data.getScanInfo().getMeasurementData().getPumpSpeed());
            pstmt.setInt(22, data.getScanInfo().getMeasurementData().getFlushSpeed());
            pstmt.setInt(23, data.getScanInfo().getMeasurementData().getPurgeSpeed());
            pstmt.setInt(24, data.getScanInfo().getMeasurementData().getPurgeTime());
            pstmt.setInt(25, data.getScanInfo().getMeasurementData().getSETPOINTS());
            pstmt.setString(26, data.getScanInfo().getFWVersion());
            pstmt.setInt(27, 0);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addFTIRData(Integer id,String FTIRData){
        String sql = "UPDATE DataTable SET spectrum = ? WHERE id = ?";
        try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1,FTIRData);
            pstmt.setInt(2, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addStatusEntry(FPSSystemState state, FTIRStatus ftirStatus, String sampleName, int ID) {
        String sql = "INSERT INTO " + StatusTable + " (\n"
                + "    dataID,\n"
                + "    sampleName,\n"
                + "    state,\n"
                + "    elapsedTime,\n"
                + "    measurementPoint,\n"
                + "    adcSensor,\n"
                + "    adcHeatx,\n"
                + "    fluidTemp,\n"
                + "    PWM1,\n"
                + "    PWM2,\n"
                + "    fanStatus,\n"
                + "    tempSetpoint,\n"
                + "    corrTempSetpoint,\n"
                + "    pumpSpeed\n"
                + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1,ID);
            pstmt.setString(2, sampleName);
            pstmt.setString(3, state.getState().toString());
            pstmt.setInt(4,state.getElapsedTime());
            pstmt.setInt(5, state.getMeasurementPoint());
            pstmt.setInt(6, state.getAdcSensor());
            pstmt.setInt(7, state.getAdcHeatx());
            pstmt.setInt(8, state.getFluidTemp());
            pstmt.setInt(9, state.getPWM1());
            pstmt.setInt(10, state.getPWM2());
            pstmt.setInt(11, state.getFanStatus());
            pstmt.setInt(12, state.getTempSetpoint());
            pstmt.setInt(13, state.getCorrTempSetpoint());
            pstmt.setInt(14, state.getPumpSpeed());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addSensorEntry(SensorData[] data){
        for (SensorData point :data) {
            String sql = "INSERT INTO " + SensorTable + " (\n"
                    + "    dataID,\n"
                    + "    targetTemp,\n"
                    + "    pViscosity,\n"
                    + "    pDensity,\n"
                    + "    pDielectricStrength,\n"
                    + "    pTemperature,\n"
                    + "    pStatus\n"
                    + ") VALUES (?,?,?,?,?,?,?)";

            try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setInt(1, point.getSqlID());
                pstmt.setInt(2, point.getTargetTemp());
                pstmt.setDouble(3, point.getpViscosity());
                pstmt.setDouble(4, point.getpDensity());
                pstmt.setDouble(5, point.getpDielectricStrength());
                pstmt.setDouble(6, point.getpTemperature());
                pstmt.setInt(7, point.getpStatus());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public Integer getNumberOfDataEntries() {
        Integer numData = -1;
        String sql = "SELECT id, sampleName  FROM DataTable ORDER BY id DESC LIMIT 1";
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                numData = rs.getInt("id");
                System.out.println("ID : " + rs.getInt("id") + " Name : " + rs.getString("sampleName"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return numData;
    }

    public void setUploaded(int id){
        String sql = "UPDATE DataTable SET uploaded=1 WHERE id = ?";
        try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Integer getNumberOfPendingUploads(){
        Integer numData = 0;
        String sql = "SELECT id, sampleName FROM DataTable WHERE uploaded=0";
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                numData = numData + 1;
                //System.out.println("ID : " + rs.getInt("id") + " Name : " + rs.getString("sampleName"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return numData;
    }

    public ArrayList<FPSData> getPendingDataEntries(){
        ArrayList<FPSData> dataArray = new ArrayList<>();
        String sql = "SELECT "
                + "id,"
                + "time,"
                + "sampleName,"
                + "targetTemp0,"
                + "pViscosity0,"
                + "pDensity0,"
                + "pDielectricStrength0,"
                + "pTemperature0,"
                + "pStatus0,"
                + "targetTemp1,"
                + "pViscosity1,"
                + "pDensity1,"
                + "pDielectricStrength1,"
                + "pTemperature1,"
                + "pStatus1,"
                + "pStatusSystem,"
                + "KV40Walther,"
                + "simpleMeasurement,"
                + "setpoint0,"
                + "setpoint1,"
                + "measurementTime,"
                + "flushTime,"
                + "pumpSpeed,"
                + "flushSpeed,"
                + "purgeSpeed,"
                + "purgeTime,"
                + "SETPOINTS"
                + " FROM DataTable WHERE uploaded=0";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                MeasurementData mData = new MeasurementData(rs.getInt("setpoint0"),
                        rs.getInt("setpoint1"),
                        rs.getInt("measurementTime"),
                        rs.getInt("pumpSpeed"),
                        rs.getInt("flushSpeed"),
                        rs.getInt("flushTime"),
                        rs.getInt("purgeSpeed"),
                        rs.getInt("purgeTime"),
                        rs.getInt("SETPOINTS")
                );
                ScanInfo info = new ScanInfo(rs.getString("sampleName"),rs.getBoolean("simpleMeasurement"),mData);
                FPSData data = new FPSData(
                        info,
                        rs.getInt("targetTemp0"),
                        rs.getDouble("pViscosity0"),
                        rs.getDouble("pDensity0"),
                        rs.getDouble("pDielectricStrength0"),
                        rs.getDouble("pTemperature0"),
                        rs.getInt("pStatus0"),
                        rs.getInt("targetTemp1"),
                        rs.getDouble("pViscosity1"),
                        rs.getDouble("pDensity1"),
                        rs.getDouble("pDielectricStrength1"),
                        rs.getDouble("pTemperature1"),
                        rs.getInt("pStatus1"),
                        rs.getInt("pStatusSystem"),
                        rs.getDouble("KV40Walther"),
                        rs.getInt("id")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<FPSData> getDataEntries(){
        ArrayList<FPSData> dataArray = new ArrayList<>();
        String sql = "SELECT "
                + "id,"
                + "time,"
                + "sampleName,"
                + "targetTemp0,"
                + "pViscosity0,"
                + "pDensity0,"
                + "pDielectricStrength0,"
                + "pTemperature0,"
                + "pStatus0,"
                + "targetTemp1,"
                + "pViscosity1,"
                + "pDensity1,"
                + "pDielectricStrength1,"
                + "pTemperature1,"
                + "pStatus1,"
                + "pStatusSystem,"
                + "KV40Walther,"
                + "simpleMeasurement,"
                + "setpoint0,"
                + "setpoint1,"
                + "measurementTime,"
                + "flushTime,"
                + "pumpSpeed,"
                + "flushSpeed,"
                + "purgeSpeed,"
                + "purgeTime,"
                + "SETPOINTS"
                + " FROM DataTable";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                MeasurementData mData = new MeasurementData(rs.getInt("setpoint0"),
                        rs.getInt("setpoint1"),
                        rs.getInt("measurementTime"),
                        rs.getInt("pumpSpeed"),
                        rs.getInt("flushSpeed"),
                        rs.getInt("flushTime"),
                        rs.getInt("purgeSpeed"),
                        rs.getInt("purgeTime"),
                        rs.getInt("SETPOINTS")
                );
                ScanInfo info = new ScanInfo(rs.getString("sampleName"),rs.getBoolean("simpleMeasurement"),mData);
                FPSData data = new FPSData(
                        info,
                        rs.getInt("targetTemp0"),
                        rs.getDouble("pViscosity0"),
                        rs.getDouble("pDensity0"),
                        rs.getDouble("pDielectricStrength0"),
                        rs.getDouble("pTemperature0"),
                        rs.getInt("pStatus0"),
                        rs.getInt("targetTemp1"),
                        rs.getDouble("pViscosity1"),
                        rs.getDouble("pDensity1"),
                        rs.getDouble("pDielectricStrength1"),
                        rs.getDouble("pTemperature1"),
                        rs.getInt("pStatus1"),
                        rs.getInt("pStatusSystem"),
                        rs.getDouble("KV40Walther"),
                        rs.getInt("id")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<FPSData> getDataEntries(Integer numEntries){
        ArrayList<FPSData> dataArray = new ArrayList<>();
        String sql = "SELECT "
                + "id,"
                + "time,"
                + "sampleName,"
                + "targetTemp0,"
                + "pViscosity0,"
                + "pDensity0,"
                + "pDielectricStrength0,"
                + "pTemperature0,"
                + "pStatus0,"
                + "targetTemp1,"
                + "pViscosity1,"
                + "pDensity1,"
                + "pDielectricStrength1,"
                + "pTemperature1,"
                + "pStatus1,"
                + "pStatusSystem,"
                + "KV40Walther,"
                + "simpleMeasurement,"
                + "setpoint0,"
                + "setpoint1,"
                + "measurementTime,"
                + "flushTime,"
                + "pumpSpeed,"
                + "flushSpeed,"
                + "purgeSpeed,"
                + "purgeTime,"
                + "SETPOINTS"
                + " FROM DataTable ORDER BY id DESC LIMIT ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){

             pstmt.setInt(1,numEntries);
             ResultSet rs = pstmt.executeQuery();
             while (rs.next()) {
                MeasurementData mData = new MeasurementData(rs.getInt("setpoint0"),
                                                            rs.getInt("setpoint1"),
                                                            rs.getInt("measurementTime"),
                                                            rs.getInt("pumpSpeed"),
                                                            rs.getInt("flushSpeed"),
                                                            rs.getInt("flushTime"),
                                                            rs.getInt("purgeSpeed"),
                                                            rs.getInt("purgeTime"),
                                                            rs.getInt("SETPOINTS")
                        );
                ScanInfo info = new ScanInfo(rs.getString("sampleName"),rs.getBoolean("simpleMeasurement"),mData);
                FPSData data = new FPSData(
                        info,
                        rs.getInt("targetTemp0"),
                        rs.getDouble("pViscosity0"),
                        rs.getDouble("pDensity0"),
                        rs.getDouble("pDielectricStrength0"),
                        rs.getDouble("pTemperature0"),
                        rs.getInt("pStatus0"),
                        rs.getInt("targetTemp1"),
                        rs.getDouble("pViscosity1"),
                        rs.getDouble("pDensity1"),
                        rs.getDouble("pDielectricStrength1"),
                        rs.getDouble("pTemperature1"),
                        rs.getInt("pStatus1"),
                        rs.getInt("pStatusSystem"),
                        rs.getDouble("KV40Walther"),
                        rs.getInt("id")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getStackTrace());
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<FPSData> getDataEntriesFrom(Integer numEntries, Integer staringID){
        ArrayList<FPSData> dataArray = new ArrayList<>();
        String ids = "?";
        for(int i=1;i<numEntries;i++){ids = ids + " , ?";}
        String sql = "SELECT "
                + "id,"
                + "time,"
                + "sampleName,"
                + "targetTemp0,"
                + "pViscosity0,"
                + "pDensity0,"
                + "pDielectricStrength0,"
                + "pTemperature0,"
                + "pStatus0,"
                + "targetTemp1,"
                + "pViscosity1,"
                + "pDensity1,"
                + "pDielectricStrength1,"
                + "pTemperature1,"
                + "pStatus1,"
                + "pStatusSystem,"
                + "KV40Walther,"
                + "simpleMeasurement,"
                + "setpoint0,"
                + "setpoint1,"
                + "measurementTime,"
                + "flushTime,"
                + "pumpSpeed,"
                + "flushSpeed,"
                + "purgeSpeed,"
                + "purgeTime,"
                + "SETPOINTS"
                + " FROM DataTable WHERE id IN (" + ids +")";
        try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)){
            for(int i = 1; i <= numEntries ;i++){
                pstmt.setInt(i,staringID-(i-1));
            }
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                MeasurementData mData = new MeasurementData(rs.getInt("setpoint0"),
                        rs.getInt("setpoint1"),
                        rs.getInt("measurementTime"),
                        rs.getInt("pumpSpeed"),
                        rs.getInt("flushSpeed"),
                        rs.getInt("flushTime"),
                        rs.getInt("purgeSpeed"),
                        rs.getInt("purgeTime"),
                        rs.getInt("SETPOINTS")
                );
                ScanInfo info = new ScanInfo(rs.getString("sampleName"),rs.getBoolean("simpleMeasurement"),mData);
                FPSData data = new FPSData(
                        info,
                        rs.getInt("targetTemp0"),
                        rs.getDouble("pViscosity0"),
                        rs.getDouble("pDensity0"),
                        rs.getDouble("pDielectricStrength0"),
                        rs.getDouble("pTemperature0"),
                        rs.getInt("pStatus0"),
                        rs.getInt("targetTemp1"),
                        rs.getDouble("pViscosity1"),
                        rs.getDouble("pDensity1"),
                        rs.getDouble("pDielectricStrength1"),
                        rs.getDouble("pTemperature1"),
                        rs.getInt("pStatus1"),
                        rs.getInt("pStatusSystem"),
                        rs.getDouble("KV40Walther"),
                        rs.getInt("id")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        Collections.reverse(dataArray);
        return dataArray;
    }

     public ArrayList<FPSSystemState> getStateEntries(){
        ArrayList<FPSSystemState> dataArray = new ArrayList<>();
        String sql = "SELECT * FROM " + StatusTable;
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                FPSSystemState status = new FPSSystemState(FPSStates.valueOf(rs.getString("state")),
                        rs.getInt("elapsedTime"),
                        rs.getInt("measurementPoint"),
                        rs.getInt("adcSensor"),
                        rs.getInt("adcHeatx"),
                        rs.getInt("fluidTemp"),
                        rs.getInt("PWM1"),
                        rs.getInt("PWM2"),
                        rs.getInt("fanStatus"),
                        rs.getInt("tempSetpoint"),
                        rs.getInt("corrTempSetpoint"),
                        rs.getInt("pumpSpeed")
                );
                status.setId(rs.getInt("dataId"));
                status.setSampleName(rs.getString("sampleName"));
                status.setTimeCode(rs.getString("time"));
                dataArray.add(status);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<FPSSystemState> getStateEntries(int runNumber){
        ArrayList<FPSSystemState> dataArray = new ArrayList<>();
        String sql = "SELECT * FROM " + StatusTable +" WHERE dataID IS ?";
        try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setInt(1,runNumber);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                FPSSystemState status = new FPSSystemState(FPSStates.valueOf(rs.getString("state")),
                        rs.getInt("elapsedTime"),
                        rs.getInt("measurementPoint"),
                        rs.getInt("adcSensor"),
                        rs.getInt("adcHeatx"),
                        rs.getInt("fluidTemp"),
                        rs.getInt("PWM1"),
                        rs.getInt("PWM2"),
                        rs.getInt("fanStatus"),
                        rs.getInt("tempSetpoint"),
                        rs.getInt("corrTempSetpoint"),
                        rs.getInt("pumpSpeed")
                );
                status.setId(rs.getInt("dataId"));
                status.setSampleName(rs.getString("sampleName"));
                status.setTimeCode(rs.getString("time"));
                dataArray.add(status);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<SensorData> getSensorEntries(){
        ArrayList<SensorData> dataArray = new ArrayList<>();
        String sql = "SELECT * FROM " + SensorTable;
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                SensorData data = new SensorData(rs.getInt("dataID"),
                        rs.getInt("targetTemp"),
                        rs.getDouble("pViscosity"),
                        rs.getDouble("pDensity"),
                        rs.getDouble("pDielectricStrength"),
                        rs.getDouble("pTemperature"),
                        rs.getInt("pStatus")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public ArrayList<SensorData> getSensorEntries(int runNumber){
        ArrayList<SensorData> dataArray = new ArrayList<>();
        String sql = "SELECT * FROM " + SensorTable +" WHERE dataID IS ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setInt(1,runNumber);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                SensorData data = new SensorData(rs.getInt("dataID"),
                        rs.getInt("targetTemp"),
                        rs.getDouble("pViscosity"),
                        rs.getDouble("pDensity"),
                        rs.getDouble("pDielectricStrength"),
                        rs.getDouble("pTemperature"),
                        rs.getInt("pStatus")
                );
                dataArray.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dataArray;
    }

    public FTIRData getFTIRData(int id){
        FTIRData data = new FTIRData();
        String sql = "SELECT spectrum FROM DataTable WHERE id IS ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setInt(1,id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                data.parseJSON(rs.getString("spectrum"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return data;
    }
}
