package FPS;

import org.json.simple.JSONObject;

public class SensorData {

    private int sqlID;
    private Integer targetTemp;
    private Double pViscosity;
    private Double pDensity;
    private Double pDielectricStrength;
    private Double pTemperature;
    private Integer pStatus;

    public int getSqlID() {
        return sqlID;
    }

    public void setSqlID(int sqlID) {
        this.sqlID = sqlID;
    }

    public Integer getTargetTemp() {
        return targetTemp;
    }

    public void setTargetTemp(Integer targetTemp) {
        this.targetTemp = targetTemp;
    }

    public Double getpViscosity() {
        return pViscosity;
    }

    public void setpViscosity(Double pViscosity) {
        this.pViscosity = pViscosity;
    }

    public Double getpDensity() {
        return pDensity;
    }

    public void setpDensity(Double pDensity) {
        this.pDensity = pDensity;
    }

    public Double getpDielectricStrength() {
        return pDielectricStrength;
    }

    public void setpDielectricStrength(Double pDielectricStrength) {
        this.pDielectricStrength = pDielectricStrength;
    }

    public Double getpTemperature() {
        return pTemperature;
    }

    public void setpTemperature(Double pTemperature) {
        this.pTemperature = pTemperature;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    public SensorData(){
        sqlID = 0;
        targetTemp = 0;
        pViscosity = 0.0;
        pDensity = 0.0;
        pDielectricStrength = 0.0;
        pTemperature = 0.0;
        pStatus = 0;
    }

    public SensorData(int sqlID, Integer targetTemp, Double pViscosity, Double pDensity, Double pDielectricStrength, Double pTemperature, Integer pStatus) {
        this.sqlID = sqlID;
        this.targetTemp = targetTemp;
        this.pViscosity = pViscosity;
        this.pDensity = pDensity;
        this.pDielectricStrength = pDielectricStrength;
        this.pTemperature = pTemperature;
        this.pStatus = pStatus;
    }

    public SensorData(int sqlID, String data){
        this.sqlID = sqlID;
        String[] dataArray = data.split(":");
        if (dataArray.length == 0){
            new SensorData();
        }else {
            try {
                targetTemp = Integer.valueOf(dataArray[0]);
                pViscosity = Double.valueOf(dataArray[1]);
                pDensity = Double.valueOf(dataArray[2]);
                pDielectricStrength = Double.valueOf(dataArray[3]);
                pTemperature = Double.valueOf(dataArray[4]);
                if (dataArray[5] == "0x8"){
                    pStatus = 8;
                }else {
                    pStatus = Integer.valueOf(dataArray[5]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
