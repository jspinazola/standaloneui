package FPS;

import com.fazecast.jSerialComm.SerialPort;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;

public class SerialPortComm {

    private SerialPort comPort = null;
    private BufferedReader comResp = null;
    private OutputStream outputStream = null;
    private PrintStream comReq = null;
    private int readAttempts = 0;

    public  SERIAL_PORT_STATUS PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_NOTINIT;
    public enum SERIAL_PORT_STATUS {
        SERIAL_PORT_READY,
        SERIAL_PORT_FAULTED,
        SERIAL_PORT_NOTINIT
    }


    public SerialPortComm(){
        String desc = "RS485";
        comPort = getSerialPort(desc);
        if(comPort != null){
            comResp = new BufferedReader(new InputStreamReader(comPort.getInputStream()));
            outputStream = comPort.getOutputStream();
            comReq = new PrintStream(outputStream);
            PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_READY;
        }
        else{
            PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_FAULTED;
        }
    }

    @Deprecated
    private SerialPort getSerialPort(String descriptor) {
        // FT231X USB UART
        SerialPort port = null;
        boolean bPortOpened = false;

        SerialPort[] serialPorts = SerialPort.getCommPorts();
        for (SerialPort sPort : serialPorts) {
            System.out.println("serial port: " + sPort.getPortDescription() + "/" + sPort.getSystemPortName() + "/" + sPort.getDescriptivePortName());
            try {
                if (sPort.getPortDescription().contains(descriptor) && !sPort.getPortDescription().contains("FTIR")) {
                    bPortOpened = sPort.openPort();
                    System.out.println("serial port opened?: " + bPortOpened);
                    port = sPort;
                    port.setComPortParameters(19200, 8, 1, 0);
                    port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,2500,10);
                    break;
                }
            } catch (Exception ex) {
                System.out.println("exception thrown opening port: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        return port;
    }

    private SerialPort getSerialPort(HashSet<String> descriptors){
        SerialPort port = null;
        boolean bPortOpened = false;

        SerialPort[] serialPorts = SerialPort.getCommPorts();
        for(SerialPort sPort : serialPorts){
            System.out.println("serial port: " + sPort.getPortDescription() + "/" + sPort.getSystemPortName() + "/" + sPort.getDescriptivePortName());
            try {
                if (descriptors.contains(sPort.getPortDescription().toUpperCase())) {
                    bPortOpened = sPort.openPort();
                    System.out.println("serial port opened?: "+bPortOpened);
                    port = sPort;
                    port.setComPortParameters(19200, 8, 1, 0);
                    port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,1000,10);
                    break;
                }
            }
            catch (Exception ex)
            {
                System.out.println("exception thrown opening port: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        return port;
    }

    public MeasurementData getDefaultMeasurementData(){
        MeasurementData boardData = new MeasurementData();
        String[] params = {"bSP-0","bSP-1","bSP-2","bSP-3","bSP-4","bSP-5","bSP-6","bSP-7","bSP-8"};
        String[] paramsResp = new String[params.length];
        for(int i=0;i<params.length;i++){
            writeString(params[i]);
            String response = readString();
            if(response.split(",")[0].equals("bOK") && response.contains(":")){
                response = response.split(":")[1];
                System.out.println(response);
            }else {
                response = "0";
                System.out.println(response);
            }
            paramsResp[i] = response;
        }
        boardData.setSetpoint0(Integer.parseInt(paramsResp[0]));
        boardData.setSetpoint1(Integer.parseInt(paramsResp[1]));
        boardData.setMeasurementTime(Integer.parseInt(paramsResp[2]));
        boardData.setFlushTime(Integer.parseInt(paramsResp[3]));
        boardData.setPumpSpeed(Integer.parseInt(paramsResp[4]));
        boardData.setFlushSpeed(Integer.parseInt(paramsResp[5]));
        boardData.setPurgeSpeed(Integer.parseInt(paramsResp[6]));
        boardData.setPurgeTime(Integer.parseInt(paramsResp[7]));
        boardData.setSETPOINTS(Integer.parseInt(paramsResp[8]));
        return boardData;
    }

    public void setMeasurementData(MeasurementData data){
        writeString("bSP-0="+data.getSetpoint0());
        String response = readString();
        System.out.println(data.getSetpoint0() + " - "+ response);
        writeString("bSP-1="+data.getSetpoint1());
        response = readString();
        System.out.println(data.getSetpoint1() + " - "+ response);
        writeString("bSP-2="+data.getMeasurementTime());
        response = readString();
        System.out.println(data.getMeasurementTime() + " - "+ response);
        writeString("bSP-3="+data.getFlushTime());
        response = readString();
        System.out.println(data.getFlushTime() + " - "+ response);
        writeString("bSP-4="+data.getPumpSpeed());
        response = readString();
        System.out.println(data.getPumpSpeed() + " - "+ response);
        writeString("bSP-5="+data.getFlushSpeed());
        response = readString();
        System.out.println(data.getFlushSpeed() + " - "+ response);
        writeString("bSP-6="+data.getPurgeSpeed());
        response = readString();
        System.out.println(data.getPurgeSpeed() + " - "+ response);
        writeString("bSP-7="+data.getPurgeTime());
        response = readString();
        System.out.println(data.getPurgeTime() + " - "+ response);
        writeString("bSP-8="+data.getSETPOINTS());
        response = readString();
        System.out.println(data.getSETPOINTS() + " - "+ response);
    }

    public FPSSystemState updateState(){
        FPSSystemState update = new FPSSystemState();
        writeString("bUS");
        String newState = readString().replace(" ","");
        String[] updateItems = newState.split(",");
        update.setSystemState(updateItems);
        return update;
    }

    public String[] updateStateString(){
        String[] updateItems = writeRead("bUS").replace(" ","").split(",");
        return updateItems;
    }

    public String getFWVersion(){
        return writeRead("bV");
    }

    public boolean sendMeasureCommand(){
        String response = writeRead("bM");
        System.out.println(response);
        if(response.equals("bOK")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendFTIRDrain(){
        String response = writeRead("bM-e");
        System.out.println(response);
        if(response.equals("bOK")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendFTIRMeasure(){
        String response = writeRead("bM-f");
        System.out.println(response);
        if(response.equals("bOK")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendCancelCommand(){
        String response = writeRead("bC");
        System.out.println(response);
        if(response.equals("bOK")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendTestCommand(){
        String response = writeRead("bFM");
        System.out.println(response);
        if(response.equals("Test,1")){
            return true;
        }else {
            return false;
        }
    }

    public boolean sendIceCal(){
        String response = writeRead("bIC");
        System.out.println(response);
        if(response.equals("IceCal,1")){
            return true;
        }else {
            return false;
        }
    }

    public boolean isDataPoint(){
        String response = writeRead("bDA");
        System.out.println(response);
        if(response.equals("Data Ready: 1")){
            return true;
        }else {
            return false;
        }
    }

    public FPSData getMeasurementData(){
        String response = writeRead("bD");
        FPSData dataIn = new FPSData();
        if(response.startsWith("KV")){
            dataIn.parseDataResponse(response);
        }else if (readAttempts < 5) {
            try {
                Thread.sleep(100);
                return getMeasurementData();
            }catch (InterruptedException e){

            }
            readAttempts = readAttempts +1;
        }else {
            readAttempts=0;
        }
        return dataIn;
    }

    public SensorData[] getSensorData(int sqlID){
        String response = writeRead("bP").replace("bOK,","");
        String[] points = response.split(",");
        SensorData[] data = new SensorData[points.length];
        if (data.length > 1) {
            int i = 0;
            for (String point : points) {
                data[i] = new SensorData(sqlID, point);
                i++;
            }
        }
        return data;
    }

    private boolean writeStringold(String command){
        boolean bSuccess = false;
        int bytesWritten = 0;
        command = command + "\r";
        byte[] cBytes = command.getBytes();
        if(PORT_STATUS == SERIAL_PORT_STATUS.SERIAL_PORT_READY){
            bytesWritten = comPort.writeBytes(cBytes,cBytes.length);
            if(bytesWritten == cBytes.length) {
                bSuccess = true;
            }
        }
        return bSuccess;
    }

    private boolean writeString(String command){
        boolean bSuccess = false;
        int bytesWritten = 0;
        command = command + "\r";
        comReq.print(command);
        bSuccess = true;
        return bSuccess;
    }

    private String readString(){
        String response = "";
        try {
            response = comResp.readLine();
        }
        catch (Exception e){
            System.out.println("ERROR" + e.toString());
            if(readAttempts < 5) {
                try{
                    Thread.sleep(100);
                }catch (InterruptedException ie){

                }
                readAttempts++;
                System.out.println("read Attempts ++: " + Integer.toString(readAttempts));
                return readString();
            }
        }
        //System.out.println(response);
        readAttempts = 0;
        return response;
    }

    private String writeRead(String command){
        command = command + "\r";
        String response = "";
        try {
            comReq.print(command);
            response = comResp.readLine();
        }
        catch (Exception e){
            System.out.println("ERROR " + e.toString());
            if(readAttempts < 5) {
                try{
                    Thread.sleep(100);
                }catch (InterruptedException ie){

                }
                readAttempts++;
                System.out.println("read Attempts ++: " + Integer.toString(readAttempts));
                return writeRead(command);
            }
        }
        //System.out.println(response);
        readAttempts = 0;
        return response;
    }

}
