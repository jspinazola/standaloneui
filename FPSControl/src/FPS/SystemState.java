package FPS;

import FPS.FTIR.FTIRState;
import FPS.FTIR.FTIRStatus;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE
)
public class SystemState {

    @JsonProperty
    private FPSSystemState fps;
    @JsonProperty
    private FTIRStatus ftir;

    public SystemState(){
        fps = new FPSSystemState();
        ftir = new FTIRStatus();
    }

    @JsonCreator()
    public SystemState(
            @JsonProperty("fps") FPSSystemState fps,
            @JsonProperty("ftir") FTIRStatus ftir
    ){
        this.fps = fps;
        this.ftir = ftir;
    }

    public FPSSystemState getFps() {
        return fps;
    }

    public void setFps(FPSSystemState fps) {
        this.fps = fps;
    }

    public FTIRStatus getFtir() {
        return ftir;
    }

    public void setFtir(FTIRStatus ftir) {
        this.ftir = ftir;
    }

    public String getJsonString(){
        ObjectMapper ob = new ObjectMapper();
        String SystemState = "";
        try{
            SystemState = ob.writeValueAsString(this);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return SystemState;
    }

    public void updateFromJsonString(String json){
        ObjectMapper ob = new ObjectMapper();
        ObjectReader reader = ob.readerForUpdating(this);
        try {
            reader.readValue(json);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
}
