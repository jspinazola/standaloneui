package FPS;

public class ZMQCommand {

    private COMMAND command;
    private String data;

    public COMMAND getCommand() {
        return command;
    }

    public String getData() {
        return data;
    }

    public ZMQCommand(COMMAND command, String data){
        this.command = command;
        this.data = data;
    }
}
