package Util;

import com.fazecast.jSerialComm.SerialPort;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class SerialPortHelper {
    private SerialPort comPort = null;
    private BufferedReader comResp = null;
    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private PrintStream comReq = null;
    private String portName;
    private int baud;
    private int timeout;
    private int readAttempts = 0;
    private int maxAttempts = 5;
    public  SERIAL_PORT_STATUS PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_NOTINIT;

    public enum SERIAL_PORT_STATUS {
        SERIAL_PORT_READY,
        SERIAL_PORT_FAULTED,
        SERIAL_PORT_NOTINIT
    }

    public SerialPortHelper(String portName, int baud, int timeout){
        this.baud = baud;
        this.timeout = timeout;
        this.portName = portName;
        comPort = getSerialPort(portName);
        if(comPort != null){
            outputStream = comPort.getOutputStream();
            comReq = new PrintStream(outputStream);
            PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_READY;
        }
        else{
            PORT_STATUS = SERIAL_PORT_STATUS.SERIAL_PORT_FAULTED;
        }
    }

    private SerialPort getSerialPort(String descriptor) {
        // FT231X USB UART
        SerialPort port = null;
        boolean bPortOpened = false;

        SerialPort[] serialPorts = SerialPort.getCommPorts();
        for (SerialPort sPort : serialPorts) {
            System.out.println("serial port: " + sPort.getPortDescription() + "/" + sPort.getSystemPortName() + "/" + sPort.getDescriptivePortName());
            try {
                if (sPort.getPortDescription().equals(descriptor)) {
                    bPortOpened = sPort.openPort();
                    System.out.println("serial port opened?: " + bPortOpened);
                    port = sPort;
                    port.setComPortParameters(baud, 8, 1, 0);
                    port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,timeout,10);
                    break;
                }
            } catch (Exception ex) {
                System.out.println("exception thrown opening port: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        return port;
    }

    //Returns true on sucess
    public boolean writeString(String command){
        command = command + "\r";
        comReq.print(command);
        if(comReq.checkError()){
            return false;
        }else {
            return true;
        }
    }

    public int in_waiting(){
      return comPort.bytesAvailable();
    }

    public String readWaiting(){
        byte [] read = new byte[comPort.bytesAvailable()];
        comPort.readBytes(read,comPort.bytesAvailable());
        return new String(read, StandardCharsets.UTF_8);
    }

    //Will try and make sure something is read
    public String readString(){
        BufferedReader br = new BufferedReader(new InputStreamReader(comPort.getInputStream()));
        String response = "";
        try {
            response = br.readLine();
            br.close();
        }
        catch (Exception e){
            response = "";
        }
        if(response == "" && readAttempts < maxAttempts){
            readAttempts++;
            System.out.println("ReadAttempts:" + Integer.toString(readAttempts));
            return readString();
        }
        readAttempts = 0;
        return response;
    }

    public String writeRead(String command){
        if(in_waiting()>0){
            System.out.println("Stuck bits: " + readWaiting());
        }
        writeString(command);
        return readString();
    }

    public void  setMaxAttempts(int maxAttempts){this.maxAttempts = maxAttempts;}
}
