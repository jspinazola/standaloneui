package XOS.FTIR;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FTIRData {
  private static final Logger LOG = LoggerFactory.getLogger(FTIRData.class.getName());
  private double[][] spectrumData;
  private double[][] backgroundData;
  private File file;

  public FTIRData(double[][] spectrumData) {
    this.spectrumData = spectrumData;
  }

  public FTIRData(){this.spectrumData = null;this.backgroundData = null;}

  public void setBackgroundData(double[][] data){this.backgroundData = data;}
  public void setSpectrumData(double[][] data){
    if(spectrumData == null){
      double[][] in = {data[2],data[3]};
      this.spectrumData = in;
    }else {
      addMoreSpectrumData(data);
    }
  }

  public void addMoreSpectrumData(double[][] data){
    double[][] newData = new double[spectrumData.length+1][];
    for (int i = 0; i < spectrumData.length; i++) {
      newData[i] = spectrumData[i];
    }
    newData[spectrumData.length] = data[3];
    this.spectrumData = newData;
  }

  public void displaySpectrumData() {
    if (this.spectrumData != null) {
      for (int j = 0; j < spectrumData[spectrumData.length - 1][0]; ++j) {
        for (int i = 0; i < 4; ++i) {
          System.out.print(spectrumData[i][j] + ", ");
        }
        System.out.println();
      }
    }
  }

  public void displayInterSpecData(){
    if (this.backgroundData != null) {
      for (int j = 0; j < backgroundData[backgroundData.length - 1][0]; ++j) {
        for (int i = 0; i < 4; ++i) {
          System.out.print(backgroundData[i][j] + ", ");
        }
        System.out.println();
      }
    }
  }

  public void saveSpectrumData(String fileName) {
    int numPoints = 0;
    Double sum = 0.0;
    if (this.spectrumData != null) {
      if (createFile()) {
        try {
          PrintWriter output = new PrintWriter("Data/" + fileName);
          //TODO Why is this being transposed? Why is it duplicated?
          output.println("SpectrumData,");
          for (int j = 0; j < spectrumData[0].length; ++j) {
            for (int i = 0; i < spectrumData.length; ++i) {
              if(i>0){
                numPoints++;
                sum+=spectrumData[i][j];
              }
              output.print(spectrumData[i][j]);
              output.print(',');
            }
            if(numPoints!=0){
              output.print(sum/numPoints);
              output.print(',');
              numPoints=0;
              sum=0.0;
            }
            output.println();
          }
          output.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public JSONObject toJSON() {
    JSONObject jsonObject = new JSONObject();

    JSONArray wln = new JSONArray();
    JSONArray absorption = new JSONArray();

    if (this.spectrumData != null) {
      for (int j = 0; j < spectrumData[spectrumData.length - 1][0]; ++j) {
        //TODO why is wln the same as absorption??
        wln.add(spectrumData[2][j]);
        absorption.add(spectrumData[2][j]);
      }
      jsonObject.put("wln", wln);
      jsonObject.put("absorption", absorption);
    }
    System.out.println(jsonObject.toJSONString());

    return jsonObject;
  }

  private Boolean createFile() {
    file = new File("Data");
    if (file.exists()) {
      return true;
    }
    return file.mkdir();
  }
}
