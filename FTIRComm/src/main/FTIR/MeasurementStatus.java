package XOS.FTIR;

public enum MeasurementStatus {
  IDLE("Idle"),
  MEASURING("Measuring"),
  INIT_COMPLETE("Initialization of NeoSpectra Micro kit completed successfully!"),
  INIT_FAILED("Initialization of NeoSpectra Micro kit was not completed!"),
  BACKGROUND_COMPLETE("Background measurement completed successfully."),
  BACKGROUND_FAILED("Background measurement was not completed."),
  SPECTRUM_COMPLETE("Spectrum measurement completed successfully."),
  SPECTRUM_FAILED("Spectrum measurement was not completed."),
  INTERFEROGRAM_COMPLETE("Interferogram measurement completed successfully."),
  INTERFEROGRAM_FAILED("Interferogram measurement was not completed."),
  WAVELENGTH_CALIBRATIONBG_COMPLETE("Wavelength calibration of background completed successfully."),
  WAVELENGTH_CALIBRATIONBG_FAILED("Wavelength calibration of background failed.");

  private String name;

  MeasurementStatus(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}
