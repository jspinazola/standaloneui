package main;

import FPS.FTIR.FTIRComm;
import FPS.FTIR.FTIRData;

public class RunFTIR {

    private static FTIRComm ftirComm;
    private static int FTIRMeasurementTime = 15;
    private static FTIRData ftirData;

    public static void main(String[] args) {
        ftirComm = FTIRComm.getInstance(false);
        ftirComm.initializeDevice();
        ftirComm.runRepeatSpectrum(2,10);
        while (!ftirComm.isSpectrum()){};
        ftirData = ftirComm.getInterSpecData();
        System.out.println("Done");
    }
}