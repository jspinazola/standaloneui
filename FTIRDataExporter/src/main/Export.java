package main;

import FPS.SQLManager;
import FPS.FTIR.FTIRData;

import java.util.ArrayList;

public class Export {

    private static SQLManager manager;
    private static FTIRData data = new FTIRData();
    private static int offset = 21;

    public static void main(String [] args){
        manager = new SQLManager("FPSDatabase");
        for (int i = 0; i < 16 ; i++) {
            System.out.println(i);
            FTIRData xfer = new FTIRData();
            xfer = manager.getFTIRData(offset+i);
            data.setSpectrumData(xfer.getSpectrumData());
            data.setBackgroundData(xfer.getBackgroundData());
        }
        data.saveSpectrumData("CompiledData.csv");
    }
}
