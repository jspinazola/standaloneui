import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

public class Engine{

    private int cylinders = -1;
    private int buffHorses = -1;

    public Engine(){

    }

    @JsonCreator
    public Engine(@JsonProperty("cylinders") int cylinders, @JsonProperty("buffHorses") int buffHorses){
        this.buffHorses = buffHorses;
        this.cylinders = cylinders;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public int getBuffHorses() {
        return buffHorses;
    }

    public void setBuffHorses(int buffHorses) {
        this.buffHorses = buffHorses;
    }

    public void updateEngineFromJSON(String Value){
        try{
            ObjectMapper map = new ObjectMapper();
            ObjectReader read = map.readerForUpdating(this);
            read.readValue(Value);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}