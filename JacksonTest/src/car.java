import com.fasterxml.jackson.annotation.*;

public class car{

    private String make = null;
    private String model = null;
    private int doors = 0;
    private boolean running = false;
    private Engine engine = new Engine();

    public car(){}

    @JsonCreator
    public car(
            @JsonProperty("make") String make,
            @JsonProperty("model") String model,
            @JsonProperty("doors") int doors,
            @JsonProperty("running") boolean running,
            @JsonProperty("engine") Engine engine
    ){
        this.make = make;
        this.model = model;
        this.doors = doors;
        this.running = running;
        this.engine = engine;
    }
    public boolean isRunning() {
        return running;
    }
    public void setRunning(boolean running) {
        this.running = running;
    }

    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public int getDoors() {
        return doors;
    }
    public void setDoors(int doors) {
        this.doors = doors;
    }

    public Engine getEngine() {
        return engine;
    }
    public void setEngine(Engine engine) {
        this.engine = engine;
    }
    public void updateEngine(String newValue){engine.updateEngineFromJSON(newValue);}
}