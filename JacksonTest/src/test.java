
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;


import java.util.Enumeration;

public class test{

    public static void main(String [] args){
        car mycar = new car("Ford","Ranger",4,true,new Engine(4,270));
        System.out.println(mycar.getMake());
        ObjectMapper ob = new ObjectMapper();
        String jsonCar = "";
        try {
            jsonCar = ob.writeValueAsString(mycar);
        }
        catch(Exception e ){
                System.out.println(e.getMessage());
        }
        System.out.println(jsonCar);
        car convertedCar = new car();
        try{
            convertedCar = ob.readValue(jsonCar, car.class);
        }catch(Exception e ){
            System.out.println(e.getMessage());
        }
        System.out.println(convertedCar.getModel());
        System.out.println(convertedCar.getEngine().getBuffHorses());

        String fString = "{\"model\":\"Ranger\",\"doors\":4,\"running\":true,\"engine\":{\"cylinders\":4,\"buffHorses\":270}}";

        car fcar = new car();
        try{
            fcar = ob.readValue(fString, car.class);
        }catch(Exception e ){
            System.out.println(e.getMessage());
        }
        System.out.println(fcar.getMake());
        System.out.println(fcar.getEngine().getBuffHorses());
        fcar.updateEngine("{\"cylinders\":4,\"buffHorses\":350}");
        System.out.println(fcar.getEngine().getBuffHorses());

        /*
        ZMQ.Context ctx = ZMQ.context(1);
        ZMQ.Socket req = ctx.socket(SocketType.REQ);
        String addressHeader = "tcp://localhost:";
        //private String addressHeader = "tcp://10.128.54.53:";
        int portReq = 5557;
        req.connect(addressHeader+Integer.toString(portReq));
        req.send("MEASUREFTIR",ZMQ.SNDMORE);
        req.send("{\"measurementData\":{\"setpoint1\":0,\"setpoint0\":0,\"purgeTime\":0,\"purgeSpeed\":0,\"flushSpeed\":0,\"measurementTime\":0,\"pumpSpeed\":0,\"flushTime\":0,\"SETPOINTS\":0},\"ftirTime\":2,\"repeats\":20,\"ftir\":true,\"name\":\"\",\"fps\":false,\"simpleMeasurement\":true,\"fwVersion\":\"V0.0.0.0\"}");
        String resp = "";
        resp = req.recvStr();
        System.out.println(resp);

         */

        ZMQ.Context ctx = ZMQ.context(1);
        ZMQ.Socket pub = ctx.socket(SocketType.PUB);
        String addressHeader = "tcp://*:";
        int portRep = 5557;
        int portPub = 5556;
        pub.bind("tcp://*:5556");


        /*
        FPSSystemState testState = new FPSSystemState();
        try {
            jsonCar = ob.writeValueAsString(testState);
        }
        catch(Exception e ){
            System.out.println(e.getMessage());
        }
        jsonCar = jsonCar.replace("0","1");
        System.out.println(jsonCar);
        System.out.println(" ----------- FPSSystemState as String --------------");


        FPSSystemState convertedState = new FPSSystemState();
        try{
            convertedState = ob.readValue(jsonCar, FPSSystemState.class);
        }catch(Exception e ){
            System.out.println(e.getMessage());
        }
        System.out.println(convertedState.getFWVersion());
        System.out.println(" ----------- FPSSystemState converted from String --------------");

        SystemState newState = new SystemState();
        jsonCar = newState.getJsonString();
        jsonCar = jsonCar.replace("0","2");
        jsonCar = jsonCar.replace("null","2");
        System.out.println(jsonCar);
        System.out.println(" ----------- SystemState as String --------------");

        newState.setFromJsonString(jsonCar);
        System.out.println(newState.getFps().getFWVersion());
        System.out.println(newState.getFtir().getWarmTime());
        System.out.println(" ------------  SystemState converted from String -------------");

         */

    }

}