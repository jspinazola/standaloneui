package sample;

import sample.AbstractPanelController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public abstract class AbstractFxmlPanelController extends AbstractPanelController {
    private final URL fxmlURL;
    private final ResourceBundle resources;

    /**
     * Base constructor for invocation by the subclasses.
     *
     * @param fxmlURL the URL of the FXML file to be loaded (cannot be null)
     * @param editorController the editor controller (cannot be null)
     */

    protected AbstractFxmlPanelController(URL fxmlURL, ResourceBundle resources) {
        super();
        this.fxmlURL = fxmlURL;
        this.resources = resources;
        assert fxmlURL != null : "Check the name of the FXML file used by "
                + getClass().getSimpleName();
    }

    /**
     * This implementation loads the FXML file using the URL passed to
     * {@link AbstractFxmlPanelController}. Subclass implementation should make
     * sure that this method can be invoked outside of the JavaFX thread
     */
    @Override
    protected void makePanel() {
        final FXMLLoader loader = new FXMLLoader();
        loader.setController(this);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        try {
            setPanelRoot((Parent) loader.load());

        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }


    /**
     * Allows any controllers extending this class to have a generic method that
     * they can override and have executed before their content is shown.
     */
    public void setUp(){

    }

}

