package sample;

import javafx.scene.Parent;

/**
 *
 * @author bbobier
 */
public abstract class AbstractPanelController {

  private Parent panelRoot;

  /**
   * Returns the root FX object of this panel. When called the first time, this
   * method invokes {@link #makePanel()} to build the FX components of the
   * panel.
   *
   * @return the root object of the panel (never null)
   */
  public Parent getPanelRoot() {
    if (panelRoot == null) {
      makePanel();
      assert panelRoot != null;
    }
    return panelRoot;
  }

  /*
   * To be implemented by subclasses
   */
  /**
   * Creates the FX object composing the panel. This routine is called by
   * {@link AbstractPanelController#getPanelRoot}. It *must* invoke
   * {@link AbstractPanelController#setPanelRoot}.
   */
  protected abstract void makePanel();

  /**
   * Set the root of this panel controller. This routine must be invoked by
   * subclass's makePanel() routine.
   *
   * @param panelRoot the root panel (non null).
   */
  protected final void setPanelRoot(Parent panelRoot) {
    assert panelRoot != null;
    this.panelRoot = panelRoot;
  }
}
