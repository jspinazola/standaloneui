package sample;

import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import org.json.simple.JSONObject;

public class ControlBoardInterface implements Runnable{

    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket sub = ctx.socket(SocketType.SUB);
    private String addressHeader = "tcp://*:";
    //private String addressHeader = "tcp://10.128.55.91:";
    private int portReq = 5557;
    private int portSub = 5556;
    private boolean running = true;
    private SidecarModel model;
    private StringProperty messageHolder;
    private StringProperty dataMessage;
    private StringProperty settingsMessage;
    private StringProperty filmAdvanceMessage;

    public void run() {
        setUp();
         while (running){
             String topic = sub.recvStr();
             String data = " ";
             if(sub.hasReceiveMore()){
                 data = sub.recvStr();
             }
             switch (topic){
                 case "Status": {
                     String update = data;
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             messageHolder.set(update);
                         }
                     });
                 }
                 break;
                 case "Data":{
                     String update = data;
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             dataMessage.set(update);
                         }
                     });
                }
                break;
                 case "Settings":{
                     String update = data;
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             settingsMessage.set(update);
                         }
                     });
                 }
                 break;
                 case "FilmAdvance":{
                     String update = data;
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             filmAdvanceMessage.set(update);
                         }
                     });
                 }
                 break;
                 default: {
                     System.out.println("Unidentified Topic - " + topic + " : " + data);
                 }
             }
         }

    }

    public void setMessageHolder(StringProperty message){
        messageHolder = message;
    }

    public void setDataMessageHolder(StringProperty message){
        dataMessage = message;
    }
    public void setSettingsMessageHolder(StringProperty message){
        settingsMessage = message;
    }

    public void setFilmAdvanceMessageHolder(StringProperty message){
        filmAdvanceMessage = message;
    }

    public void setModel(SidecarModel model){
        this.model = model;
    }

    private void setUp(){
        if(model.getConfig().getRemoteIP() != ""){
            System.out.println("ControlBoardInterface IP:" +  model.getConfig().getRemoteIP());
            sub.connect("tcp://" + model.getConfig().getRemoteIP() + ":" +Integer.toString(portSub));
        }else {
            sub.connect(addressHeader+Integer.toString(portSub));
        }
        sub.subscribe("");
    }
}
