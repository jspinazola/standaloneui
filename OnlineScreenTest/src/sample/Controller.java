package sample;

import FPS.OnlineSystemSetting;
import de.jensd.fx.glyphs.GlyphIcons;
import de.jensd.fx.glyphs.fontawesome.utils.FontAwesomeIconFactory;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Labeled;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import org.zeromq.SocketType;

import sample.MainPresenter;
import sample.SidecarModel;

import org.zeromq.ZMQ;
import org.zeromq.ZContext;

import java.io.File;

public class Controller{

    private static volatile Controller singleton = null;
    private static final Object lock = new Object();
    private SidecarModel model;
    private MainPresenter view;
    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket req = ctx.socket(SocketType.REQ);
    private String addressHeader = "tcp://localhost:";
    //private String addressHeader = "tcp://10.128.54.53:";
    private int portReq = 5557;

    public static Controller getInstance() {
        if (singleton == null) {
            // double-checked is faster
            synchronized (lock) {
                if (singleton == null) {
                    singleton = new Controller();
                }
            }
        }
        return singleton;
    }

    private Controller(){

    }

    public void connect(){
        if(model.getConfig().getRemoteIP() != ""){
            req.connect("tcp://" + model.getConfig().getRemoteIP() + ":" +Integer.toString(portReq));
        }else {
            req.connect(addressHeader+Integer.toString(portReq));
        }
    }

    public Stage getPrimaryStage() {
        return view.getPrimaryStage();
    }

    public void setButtonIcons(Labeled btn, GlyphIcons icon, ContentDisplay pos, Color color) {
        setButtonIcons(btn, icon, pos, color, 25);
    }

    public void setButtonIcons(Labeled btn, GlyphIcons icon, ContentDisplay pos, Color color, int height) {
        try {
            Text text = FontAwesomeIconFactory.get().createIcon(icon, Integer.toString(height) + "px");
            text.setFill(color);
            btn.setGraphic(text);
            btn.setContentDisplay(pos);
        } catch (NoSuchMethodError nsme) {
            //LOG.error("Unable to load Glyph. {}", nsme);
        }
    }


    public void ICECAL(){
        req.send("ICECAL",ZMQ.SNDMORE);
        req.send("nothing");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String resp = "";
                resp = req.recvStr();
                model.setRep(resp);
                if(resp == "0,Invalid Command"){
                    System.out.println("Error Command Not Received");
                }
            }
        });
    }

    public void  MEASURE(){
        System.out.println(model.getScanInfo().getScanInfoJSON().toString());
        req.send(model.getScanInfo().getScanInfoJSON().toString());
        view.setContent(SidecarPageName.STATUS,false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String resp = "";
                resp = req.recvStr();
                model.setRep(resp);
                if(resp == "0,Invalid Command"){
                    System.out.println("Error Command Not Received");
                }
            }
        });
    }

    public void  SENDSETTINGS(){
        System.out.println(model.getNewOnlineSettings().getJSONString());
        req.sendMore("SETTINGS");
        req.send(model.getNewOnlineSettings().getJSONString());
        model.updateOnlineSettings(new OnlineSystemSetting());
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String resp = "";
                resp = req.recvStr();
                model.setRep(resp);
                if(resp == "0,Invalid Command"){
                    System.out.println("Error Command Not Received");
                }
            }
        });
    }

    public void  SENDSETTINGS(OnlineSystemSetting setting){
        System.out.println(setting.getJSONString());
        req.sendMore("SETTINGS");
        req.send(setting.getJSONString());
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String resp = "";
                resp = req.recvStr();
                model.setRep(resp);
                if(resp == "0,Invalid Command"){
                    System.out.println("Error Command Not Received");
                }
            }
        });
    }

    public void CANCEL(){
        req.send("CANCEL",ZMQ.SNDMORE);
        req.send("NOW");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String resp = "";
                resp = req.recvStr();
                model.setRep(resp);
                if(resp == "0,Invalid Command"){
                    System.out.println("Error Command Not Received");
                }
            }
        });
    }

    public void TESTMODE(){
    }



    public void setModel(SidecarModel model){
        this.model = model;
    }
    public SidecarModel getModel(){
        return model;
    }

    public void setView(MainPresenter view){this.view = view;}


    public void hideModalMessage() {
        view.hideModalMessage();
    }

    public void showModalMessage(Node node, boolean blockOutsideMouseEvents) {
        view.showModalMessage(node, blockOutsideMouseEvents);
    }
}
