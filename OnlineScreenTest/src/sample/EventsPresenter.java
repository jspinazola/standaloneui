package sample;

import FPS.EventData;
import FPS.OnlineSystemData;
import FPS.OnlineSystemStatus;
import FPS.SQLManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;

import java.util.ArrayList;

public class EventsPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;
    private int lastMeasurementID;
    private int numDisplayed = 12;
    private SQLManager sql;

    @FXML
    private TableView<EventsRow> tableView;
    private ObservableList<EventsRow> tableData;

    @FXML
    private Label scanCounter;
    @FXML
    private Button previous;
    @FXML
    private Button next;

    public EventsPresenter(){
        super(HistoryPresenter.class.getResource("Events.fxml"), I18N.getBundle());
        makePanel();
        tableData = FXCollections.observableArrayList();
        control = Controller.getInstance();
        model = control.getModel();
        sql = model.getSql();
        getHistory();
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().addAll(
                makeTableColumn("Id","#",40),
                makeTableColumn("Timestamp","Time",40),
                makeTableColumn("Name","Event",40),
                makeTableColumn("Value", "Status", 40)
        );
        tableView.setItems(tableData);
    }

    private void getHistory(){
        tableData.clear();
        lastMeasurementID = sql.getNumberOfOnlineEventEntries();
        ArrayList<EventData> history = sql.getEventDataFrom(numDisplayed,lastMeasurementID);
        if(history.size() > 0) {
            lastMeasurementID = history.get(0).getId();
            for (EventData data : history) {
                tableData.add(new EventsRow(data));
            }
            tableView.setItems(tableData);
            setCounterLabel();
        }
        next.setDisable(true);
        previous.setDisable(false);
    }

    private void setCounterLabel(){
        String text = Integer.toString(tableData.get(0).getId()) + " of " + Integer.toString(lastMeasurementID);
        scanCounter.setText(text);
    }

    private TableColumn makeTableColumn(String factoryStr, String label, double prefWidth) {
        TableColumn column = new TableColumn(label);
        column.prefWidthProperty().set(prefWidth);
        column.minWidthProperty().set(prefWidth);
        column.setSortable(false);
        column.setCellValueFactory(new PropertyValueFactory<WeakRef<EventsRow>, String>(factoryStr));
        return column;
    }

    @FXML
    private void handlePrevious(){
        //Find next top row ID
        int nextStart = tableData.get(0).getId() - numDisplayed;
        if (nextStart <= numDisplayed){
            nextStart = numDisplayed;
            previous.setDisable(true);
        }
        update(sql.getEventDataFrom(numDisplayed,nextStart));
        next.setDisable(false);
    }

    @FXML
    private void handleNext(){
        //Find next top row ID
        int nextStart = tableData.get(0).getId() + numDisplayed;
        if (nextStart >= lastMeasurementID){
            nextStart = lastMeasurementID;
            next.setDisable(true);
        }
        update(sql.getEventDataFrom(numDisplayed,nextStart));
        previous.setDisable(false);
    }

    private void update(ArrayList<EventData> event){
        tableData.clear();
        for (EventData data : event) {
            tableData.add(new EventsRow(data));
        }
        tableView.setItems(tableData);
        setCounterLabel();
    }


    public void refreshContent(){
        getHistory();
    }

}
