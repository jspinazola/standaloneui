package sample;

import FPS.EventData;

public class EventsRow {
    private EventData data;

    public EventsRow(EventData data){
        this.data = data;
    }

    public EventData getData(){
        return data;
    }

    public String getTimestamp(){
        return data.getTimestamp();
    }

    public String getName(){
        return data.getName();
    }

    public Integer getId(){
        return data.getId();
    }

    public Integer getValue(){
        return data.getValue();
    }
}
