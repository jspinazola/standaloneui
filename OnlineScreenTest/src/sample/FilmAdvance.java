package sample;

import FPS.OnlineSystemFilmAdvance;
import FPS.OnlineSystemStatus;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FilmAdvance extends AbstractFxmlPanelController{

    private OnlineSystemStatus status;
    private OnlineSystemFilmAdvance advance;
    private Controller control;
    private HashMap<String, Label> labels = new HashMap<>();

    @FXML
    private FlowPane pane;
    @FXML
    private Label paneTitle;
    @FXML
    private Label FILMADVANC_INDEXCOUNT_Label;
    @FXML
    private Label FILMADVANC_INDEXCOUNT;
    @FXML
    private Label FILMADVANC_FILMCOUNT_label;
    @FXML
    private Label FILMADVANC_FILMCOUNT;
    @FXML
    private Label FILMADVANC_DRAINOPENED_X;
    @FXML
    private Label FILMADVANC_INLETOPENED_X;
    @FXML
    private Label FILMADVANC_DRIFTCORR_X;
    @FXML
    private Label FILMADVANC_FILMPRESTR_X;
    @FXML
    private Label FILMADVANC_FILMRESEAL_X;
    @FXML
    private Label FILMADVANC_FILMADVANC_X;
    @FXML
    private Label FILMADVANC_SEALRELEASED_X;
    @FXML
    private Label FILMADVANC_DRAINCLOSED_X;
    @FXML
    private Label FILMADVANC_DWELL_X;
    @FXML
    private Label FILMADVANC_N2CLOSED_X;
    @FXML
    private Label FILMADVANC_N2OPEN_X;
    @FXML
    private Label FILMADVANC_INLETCLOSED_X;

    public FilmAdvance(){
        super(ResultsSummary.class.getResource("FilmAdvance.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        status = new OnlineSystemStatus();
        advance = new OnlineSystemFilmAdvance();
        populateHash();
        updateStatus();
    }

    private void populateHash(){
        labels.put("FILMADVANC_INLETCLOSED_X",FILMADVANC_INLETCLOSED_X);
        labels.put("FILMADVANC_N2OPEN_X",FILMADVANC_N2OPEN_X);
        labels.put("FILMADVANC_N2CLOSED_X",FILMADVANC_N2CLOSED_X);
        labels.put("FILMADVANC_DWELL_X",FILMADVANC_DWELL_X);
        labels.put("FILMADVANC_DRAINCLOSED_X",FILMADVANC_DRAINCLOSED_X);
        labels.put("FILMADVANC_SEALRELEASED_X",FILMADVANC_SEALRELEASED_X);
        labels.put("FILMADVANC_FILMADVANC_X",FILMADVANC_FILMADVANC_X);
        labels.put("FILMADVANC_FILMRESEAL_X",FILMADVANC_FILMRESEAL_X);
        labels.put("FILMADVANC_FILMPRESTR_X",FILMADVANC_FILMPRESTR_X);
        labels.put("FILMADVANC_DRIFTCORR_X",FILMADVANC_DRIFTCORR_X);
        labels.put("FILMADVANC_INLETOPENED_X",FILMADVANC_INLETOPENED_X);
        labels.put("FILMADVANC_DRAINOPENED_X",FILMADVANC_DRAINOPENED_X);

        for (Map.Entry<String, Label> entry : labels.entrySet()) {
            entry.getValue().setText(I18N.getString(entry.getKey()));
        }

    }

    public void setStatus(OnlineSystemStatus status){
        this.status = status;
        updateStatus();
    }

    public void setAdvance(OnlineSystemFilmAdvance data){
        this.advance = data;
        updateStatus();
    }

    public FlowPane getPane(){
        return pane;
    }

    public ObservableList<Node> getChildren(){
        return pane.getChildren();
    }

    private void updateStatus(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        paneTitle.setText(formatter.format(date));
        for (Map.Entry<String, Label> entry : labels.entrySet()) {
            if(advance.getValue(entry.getKey()) == 1){
                entry.getValue().setStyle("-fx-background-color: -fx-color-green");
            }else {
                entry.getValue().setStyle("-fx-background-color: -fx-color-light-grey");
            }
        }
    }
}
