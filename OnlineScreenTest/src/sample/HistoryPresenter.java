package sample;

import FPS.FPSData;
import FPS.OnlineSystemData;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.AbstractFxmlPanelController;
import FPS.SQLManager;
import sample.WeakRef;

import java.util.ArrayList;

public class HistoryPresenter extends AbstractFxmlPanelController {

    private SQLManager sql = new SQLManager("OnlineDatabase");
    private Controller control;
    private SidecarModel model;

    @FXML
    private TableView<HistoryRow> tableView;
    private ObservableList<HistoryRow> tableData;
    private int lastMeasurementID;
    private int numDisplayed = 12;

    @FXML
    private Label scanCounter;
    @FXML
    private Button previous;
    @FXML
    private Button view;
    @FXML
    private Button next;
    @FXML
    private Button export;

    public HistoryPresenter(){
        super(HistoryPresenter.class.getResource("History.fxml"), I18N.getBundle());
        makePanel();
        tableData = FXCollections.observableArrayList();
        control = Controller.getInstance();
        model = control.getModel();
        model.getOnlineDataProperty().addListener(((observable, oldValue, newValue) -> {getHistory();}));
        getHistory();
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().addAll(
                makeTableColumn("SqlID","#",40),
                makeTableColumn("Timestamp","Time",40),
                makeTableColumn("PPM","PPM",40),
                makeTableColumn("PPMCounts", "Counts", 40),
                makeTableColumn("PPMRange", "Range",40)
        );
        tableView.setItems(tableData);
    }

    private void getHistory(){
        tableData.clear();
        lastMeasurementID = sql.getNumberOfOnlineDataEntries();
        ArrayList<OnlineSystemData> history = sql.getOnlineDataEntriesFrom(numDisplayed,lastMeasurementID);
        if(history.size() > 0) {
            lastMeasurementID = history.get(0).getSqlid();
            for (OnlineSystemData data : history) {
                tableData.add(new HistoryRow(data));
            }
            tableView.setItems(tableData);
            setCounterLabel();
        }
        next.setDisable(true);
        previous.setDisable(false);
    }

    private void setCounterLabel(){
        String text = Integer.toString(tableData.get(0).getSqlID()) + " of " + Integer.toString(lastMeasurementID);
        scanCounter.setText(text);
    }

    private void update(ArrayList<OnlineSystemData> history){
        export.setDisable(false);
        tableData.clear();
        for (OnlineSystemData data : history) {
            tableData.add(new HistoryRow(data));
        }
        tableView.setItems(tableData);
        setCounterLabel();
    }

    private TableColumn makeTableColumn(String factoryStr, String label, double prefWidth) {
        TableColumn column = new TableColumn(label);
        column.prefWidthProperty().set(prefWidth);
        column.minWidthProperty().set(prefWidth);
        column.setSortable(false);
        column.setCellValueFactory(new PropertyValueFactory<WeakRef<HistoryRow>, String>(factoryStr));
        return column;
    }

    @FXML
    private void handleDetail(){
        final ReadOnlyObjectProperty<HistoryRow> row = tableView.getSelectionModel().selectedItemProperty();
        if(row.get() != null){
            QuickViewPresenter qv = new QuickViewPresenter();
            //TODO move to Online Data
            qv.setData(row.get().getData());
            qv.show();
        }else {
            System.out.println("Nothing Selected");
        }
    }

    @FXML
    private void handlePrevious(){
        //Find next top row ID
        int nextStart = tableData.get(0).getSqlID() - numDisplayed;
        if (nextStart <= numDisplayed){
            nextStart = numDisplayed;
            previous.setDisable(true);
        }
        update(sql.getOnlineDataEntriesFrom(numDisplayed,nextStart));
        next.setDisable(false);
    }

    @FXML
    private void handleNext(){
        //Find next top row ID
        int nextStart = tableData.get(0).getSqlID() + numDisplayed;
        if (nextStart >= lastMeasurementID){
            nextStart = lastMeasurementID;
            next.setDisable(true);
        }
        update(sql.getOnlineDataEntriesFrom(numDisplayed,nextStart));
        previous.setDisable(false);
    }

    @FXML
    private void handleExport(){
        System.out.println("Export");
        export.setDisable(true);
        ExcelWriter ex = new ExcelWriter();
        ex.setExportAll(true);
        Platform.runLater(ex);
    }

    public void refreshContent(){

    }

}
