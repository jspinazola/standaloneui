package sample;

import FPS.FPSData;
import FPS.OnlineSystemData;

public class HistoryRow {

    private OnlineSystemData data;

    public int getSqlID() {
        return data.getSqlid();
    }

    public String getTimestamp(){return data.getTimestamp();}

    public String getPPM(){return String.format("%.2f", data.getConvertedValue("ppm_process"));}

    //public int getPPMTrend(){return data.getValue("ppm_trend");}

    public int getPPMCounts(){return data.getValue("last_counts");}

    public int getPPMRange(){return data.getValue("ppm_range");}

    public HistoryRow(OnlineSystemData data){
        this.data = data;
    }

    public OnlineSystemData getData() {
        return data;
    }
}
