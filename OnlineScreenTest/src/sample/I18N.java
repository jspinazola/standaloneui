package sample;

import java.text.MessageFormat;
import java.util.ResourceBundle;


/**
 *From SceneBuilderKit
 */
public class I18N {

  private static ResourceBundle bundle;

  public static String getString(String key) {
    return getBundle().getString(key);
  }

  public static String getString(String key, Object... arguments) {
    final String pattern = getString(key);
    return MessageFormat.format(pattern, arguments);
  }

  public static synchronized ResourceBundle getBundle() {
    if (bundle == null) {
      final String packageName = I18N.class.getPackage().getName();
      bundle = ResourceBundle.getBundle(packageName + ".Bundle"); //NOI18N
    }
    return bundle;
  }
}
