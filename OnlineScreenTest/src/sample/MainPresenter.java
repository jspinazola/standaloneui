package sample;

import FPS.FPSData;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import com.sun.javafx.scene.control.skin.FXVK;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXML;
import javafx.scene.DepthTest;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.I18N;
import sample.Controller;
import sample.AbstractFxmlPanelController;

import java.util.HashMap;

public class MainPresenter extends AbstractFxmlPanelController{

    @FXML
    private StackPane rootStack;

    private Controller control;
    private SidecarModel model;
    private QuickViewPresenter quickViewPresenter;
    private SidecarPageName  currentPage;

    @FXML public ModalDimmer modalDimmer;

    @FXML
    private BorderPane body;
    @FXML
    ToggleButton btnScanInfo;
    @FXML
    ToggleButton btnResults;
    @FXML
    ToggleButton btnSettings;
    @FXML
    ToggleButton btnHistory;
    @FXML
    ToggleButton btnStatus;
    @FXML
    ToggleButton btnStartup;
    @FXML
    ToggleButton btnMaintenance;
    @FXML
    ToggleButton btnEvents;
    @FXML
    ToggleGroup toggleGroup;
    @FXML
    Label rampStatus;
    @FXML
    Label shutterStatus;
    @FXML
    Label doorStatus;
    @FXML
    Label alarmStatus;
    @FXML
    Label codeVersion;

    private HashMap<SidecarPageName, AbstractFxmlPanelController> pages;

    private Stage primaryStage;

    public MainPresenter(){
        super(MainPresenter.class.getResource("main.fxml"), I18N.getBundle());
        makePanel();
        quickViewPresenter = new QuickViewPresenter();
    }

    public void setUp(){

        control = Controller.getInstance();
        control.setModel(model);
        control.connect();
        control.setView(this);
        pages = new HashMap<>();

        //control.setButtonIcons(lblBack, FontAwesomeIcon.CHEVRON_LEFT, ContentDisplay.LEFT, Color.web(XosColors.XOS_ORANGE));
        control.setButtonIcons(btnSettings, FontAwesomeIcon.COGS, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnResults, FontAwesomeIcon.BAR_CHART, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnStatus, FontAwesomeIcon.TACHOMETER, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnHistory, FontAwesomeIcon.HISTORY, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnStartup, FontAwesomeIcon.HOME, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnMaintenance, FontAwesomeIcon.GAVEL, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnEvents, FontAwesomeIcon.EXCLAMATION_TRIANGLE, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        //control.setButtonIcons(btnQuickView, FontAwesomeIcon.EYE, ContentDisplay.TOP, Color.web(XosColors.XOS_BLUE));
        setContent(SidecarPageName.HISTORY,false);
        setContent(SidecarPageName.STATUS,false);
        setContent(SidecarPageName.SETTINGS,false);
        setContent(SidecarPageName.RESULTS,false);
        setContent(SidecarPageName.EVENTS,false);
        setContent(SidecarPageName.MAINTENANCE,false);
        setContent(SidecarPageName.STARTUP,false);

        model.getLastMeasurementProperty().addListener(new ChangeListener<FPSData>() {
            @Override
            public void changed(ObservableValue<? extends FPSData> observable, FPSData oldValue, FPSData newValue) {
                setContent(SidecarPageName.RESULTS,false);
            }
        });
        model.getOnlineStatusProperty().addListener(((observable, oldValue, newValue) -> {refreshBottom();}));
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, ke -> handleKeyEvent(ke));
    }

    public void setContent(Node content){
        body.setCenter(content);
        body.setDepthTest(DepthTest.DISABLE);
    }

    public void setContent(SidecarPageName page, boolean addToHistory){
        if (getPage(page) == null) {
            AbstractFxmlPanelController afpc = page.getPageFactory().create();
            afpc.setUp();
            generateContent(page);
            addPage(page, afpc);
            //updatePageCss(page);
        }
        currentPage = page;
        selectToggle(page);
        refreshContent();
        refreshBottom();
        setContent(getPage(page).getPanelRoot());
    }

    public void generateContent(SidecarPageName page){
        if (getPage(page) == null) {
            AbstractFxmlPanelController afpc = page.getPageFactory().create();
            afpc.setUp();
            addPage(page, afpc);
            getPage(page).getPanelRoot();
            //updatePageCss(page);
        }
        refreshContent();
    }

    private void refreshContent(){
        if(null != currentPage){
            switch (currentPage) {
                case STATUS:
                    ((StatusPresenter) pages.get(SidecarPageName.STATUS)).refreshContent();
                    break;
                case HISTORY:
                    ((HistoryPresenter) pages.get(SidecarPageName.HISTORY)).refreshContent();
                    break;
                case SCANINFO:
                    ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).refreshContent();
                    break;
                case SETTINGS:
                    ((SettingsPresenter) pages.get(SidecarPageName.SETTINGS)).refreshContent();
                    break;
                case RESULTS:
                    ((ResultsPresenter) pages.get(SidecarPageName.RESULTS)).refreshContent();
                    break;
                case EVENTS:
                    ((EventsPresenter) pages.get(SidecarPageName.EVENTS)).refreshContent();
                    break;
                case MAINTENANCE:
                    ((MaintenancePresenter) pages.get(SidecarPageName.MAINTENANCE)).refreshContent();
                    break;
                case STARTUP:
                    ((StartupScreenPresenter) pages.get(SidecarPageName.STARTUP)).refreshContent();
                    break;
                default:
                    break;
            }
        }
    }

    private void refreshBottom(){
        if(model.getOnlineStatus().getValue("shutter_Open") == 1){
            shutterStatus.setText("Shutter Open");
        }else {
            shutterStatus.setText("Shutter Closed");
        }
        if(model.getOnlineStatus().getValue("kV_mon") > 100){
            rampStatus.setText("Beam Voltage On");
        }else {
            rampStatus.setText("Beam Voltage Off");
        }
        if(model.getOnlineStatus().getValue("door_Open") == 1){
            doorStatus.setText("Door Open");
        }else {
            doorStatus.setText("Door Closed");
        }
        if(model.getOnlineStatus().getValue("process_alarm") == 1 || model.getOnlineStatus().getValue("high_level_alarm")==1){
            alarmStatus.setText("ALARM ACTIVE");
            alarmStatus.setStyle("-fx-background-color: -fx-color-red");
        }else {
            alarmStatus.setText("Alarm Clear");
            alarmStatus.setStyle("-fx-background-color: -fx-color-light-grey");
        }
        codeVersion.setText("Code Version: " + String.valueOf(model.getOnlineData().getValue("software_version")));
    }

    private void addPage(SidecarPageName name, AbstractFxmlPanelController page) {
        //TODO This implementation does not check if the key exists, but simply overwrites if it does. This may or may not be desirable.
        pages.put(name, page);
    }

    public AbstractFxmlPanelController getPage(SidecarPageName name) {
        return pages.get(name);
    }

    private void selectToggle(SidecarPageName page) {
        if (null != page) {
            switch (page) {
                case STARTUP:
                    btnStartup.setSelected(true);
                    break;
                case STATUS:
                    btnStatus.setSelected(true);
                    break;
                case HISTORY:
                    btnHistory.setSelected(true);
                    break;
                case SETTINGS:
                    btnSettings.setSelected(true);
                    break;
                case EVENTS:
                    btnEvents.setSelected(true);
                    break;
                case MAINTENANCE:
                    btnMaintenance.setSelected(true);
                    break;
                case RESULTS:
                    btnResults.setSelected(true);
                    break;
                default:
                    break;
            }
        }
    }

    @FXML
    public void handleStatus(){
        setContent(SidecarPageName.STATUS,false);
    }
    @FXML
    public void handleStartup(){
        setContent(SidecarPageName.STARTUP,false);
    }
    @FXML
    void handleHistory(){
        setContent(SidecarPageName.HISTORY,false);
    }
    @FXML
    void handleScanInfo(){
        setContent(SidecarPageName.SCANINFO,false);
    }
    @FXML
    void handleSettings(){
        setContent(SidecarPageName.SETTINGS,false);
    }
    @FXML
    void handleResults(){
        setContent(SidecarPageName.RESULTS,false);
    }
    @FXML
    void handleEvents(){
        setContent(SidecarPageName.EVENTS,false);
    }
    @FXML
    void handleMaintenance(){
        setContent(SidecarPageName.MAINTENANCE,false);
    }

    public void setModel(SidecarModel model){
        this.model = model;
    }

    public SidecarModel getModel(){
        return model;
    }

    public void showModalMessage(Node message, boolean blockOutsideMouseEvents) {
        modalDimmer = new ModalDimmer();
        modalDimmer.setId("ModalDimmer");
        modalDimmer.setControl(control);
        rootStack.getChildren().add(modalDimmer);
        modalDimmer.showModalMessage(message, blockOutsideMouseEvents);
    }

    /**
     * Hide any modal message that is shown
     */
    public void hideModalMessage() {
        PlatformHelper.run(() -> {
            if (modalDimmer != null) {
                modalDimmer.hideModalMessage();
            }
            rootStack.getChildren().remove(modalDimmer);
            modalDimmer = null;
        });
    }

    public void handleKeyEvent(KeyEvent ke) {
        /*if (currentPage == SidecarPageName.SCANINFO) {
            ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).addText();
        }
        else{
            setContent(SidecarPageName.SCANINFO,false);
            ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).addText();
        }*/
        if (ke.getCode() == KeyCode.O && ke.isShortcutDown()) {
            //openFile();
        }
        else if (ke.getCode() == KeyCode.ESCAPE) {
            modalDimmer.hideModalMessage();
        }
    }



}
