package sample;

import FPS.OnlineSystemData;
import FPS.OnlineSystemSetting;
import FPS.OnlineSystemStatus;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class MaintenancePresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;

    @FXML
    VBox box;

    @FXML
    FlowPane flowPane;
    @FXML
    Label nameLabel;
    @FXML
    TextField filmRemaining;
    @FXML
    TextField vacHours;
    @FXML
    TextField beamHours;
    @FXML
    Label filmLabel;
    @FXML
    Label vacLabel;
    @FXML
    Label beamLabel;

    @FXML
    Button film_adv;
    @FXML
    Button validation;
    @FXML
    Button drift_corr;


    private OnlineSystemData data;
    private OnlineSystemStatus status;
    private FilmAdvance filmAdvance;

    public MaintenancePresenter(){
        super(ResultsPresenter.class.getResource("Maintenance.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        filmAdvance = new FilmAdvance();
        model.getOnlineDataProperty().addListener(((observable, oldValue, newValue) -> {update();}));
        model.getOnlineFilmAdvanceProperty().addListener(((observable, oldValue, newValue) -> {update();}));
        model.getControlStateProperty().addListener(((observable, oldValue, newValue) -> {changedControlState((SidecarModel.CONTROLSTATE) newValue, (SidecarModel.CONTROLSTATE) oldValue);}));
        update();
    }

    private void changedControlState(SidecarModel.CONTROLSTATE newState, SidecarModel.CONTROLSTATE oldState){
        switch (newState){
            case WAITING,MEASURING,FINISHED:
                film_adv.setDisable(false);
                box.getChildren().remove(filmAdvance.getPane());
                if (!box.getChildren().contains(flowPane)){
                    box.getChildren().add(2,flowPane);
                }
                break;
            case FILM_ADVANCE:
                box.getChildren().remove(flowPane);
                film_adv.setDisable(true);
                if (!box.getChildren().contains(filmAdvance.getPane())){
                    box.getChildren().add(2,filmAdvance.getPane());
                }
                break;
            default:
                break;
        }
        update();
    }

    private void update(){
        data = model.getOnlineData();
        status = model.getOnlineStatus();
        nameLabel.setText("Current System Time: " + String.valueOf(status.getValue("ms_counter")/1000/60));
        filmRemaining.setPromptText(String.valueOf(status.getValue("film_adv_cnt")));
        vacHours.setPromptText(String.valueOf(status.getValue("vac_pump_hrs")));
        beamHours.setPromptText(String.valueOf(status.getValue("xray_beam_hrs")));
        filmLabel.setText("Film Advance Counter: ");
        vacLabel.setText("Hours on Vacuum Pump: ");
        beamLabel.setText("Hours on Beam: ");
        if(model.getControlState() == SidecarModel.CONTROLSTATE.FILM_ADVANCE){
            filmAdvance.setAdvance(model.getFilmAdvance());
        }
    }

    public void refreshContent(){
        update();
    }

    @FXML
    private void handleFilmAdv(){
        System.out.println("Film Advance Requested");
        OnlineSystemSetting command = new OnlineSystemSetting();
        command.updateValue("man_film_adv",1);
        film_adv.setDisable(true);
        control.SENDSETTINGS(command);
    }
    @FXML
    private void handleVal(){System.out.println("Manual Validation Requested");}
    @FXML
    private void handleDriftCorr(){System.out.println("Manual Drift Correction Requested");}

}
