package sample;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModalDimmer extends StackPane {

    private Controller control;
    private final EventHandler consumeEvent = new EventHandler() {
        @Override
        public void handle(Event t) {
            t.consume();
        }
    };

    public ModalDimmer() {
        super();
        setVisible(false);
    }
    public void setControl(Controller control) {
        this.control = control;
    }

    private final EventHandler<MouseEvent> allowClickAwayToClosePopover
            = (MouseEvent t) -> {
        t.consume();
        final double currentX = t.getSceneX()
                - getSceneShiftX(getChildren().get(0));
        final double currentY = t.getSceneY()
                - getSceneShiftY(getChildren().get(0));

        if (!getChildren().get(0).contains(currentX, currentY)) {
            //Calling control.hideModalMessage() will release references to <code>this</code>
            //and prevent a memory leak, as control explicitly removes the modalDimmer
            //from the scene graph and nullifies the object.
            control.hideModalMessage();
        }
    };

    /**
     * Computes the relative shift on the X axis of the passed Node with respect
     * to entire scene.
     *
     * @param node The node for which the relative shift on the X axis is to be
     * computed
     * @return The number of pixels by which the provided node is shifted
     */
    private double getSceneShiftX(Node node) {
        double shift = 0;
        do {
            shift += node.getLayoutX();
            node = node.getParent();
        } while (node != null);
        return shift;
    }
    /**
     * Computes the relative shift on the Y axis of the passed Node with respect
     * to entire scene.
     *
     * @param node The node for which the relative shift on the Y axis is to be
     * computed
     * @return The number of pixels by which the provided node is shifted
     */
    private double getSceneShiftY(Node node) {
        double shift = 0;
        do {
            shift += node.getLayoutY();
            node = node.getParent();
        } while (node != null);
        return shift;
    }

    /**
     * Use doNothingKeyEvent whenever not blocking events outside of the popover
     * and want to allow click away to close the popover.
     */
    private final EventHandler<KeyEvent> doNothingKeyEvent = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent t) {
            //Do nothing
        }
    };
    /**
     * Show the given node as a floating dialog over the whole application, with
     * the rest of the application dimmed out and blocked from mouse events.
     *
     * @param message
     * @param blockOutsideMouseEvents When true, any mouse clicks that are not
     * within the popover will not cause the popover to disappear. When false,
     * the popover will disappear.
     */
    public void showModalMessage(Node message, boolean blockOutsideMouseEvents) {
        if(blockOutsideMouseEvents){
            this.setOnMousePressed(consumeEvent);
            this.setOnKeyTyped(consumeEvent);
        }
        else {
            this.setOnMousePressed(allowClickAwayToClosePopover);
            this.setOnKeyTyped(doNothingKeyEvent);
        }
        getChildren().stream().forEach(e -> {
            e = null;
        });
        this.getChildren().add(message);
        this.setOpacity(1);
        this.setVisible(true);
    }

    /**
     * Hide any modal message that is shown
     */
    public void hideModalMessage() {
        if (!isVisible()) {
            return;
        }
        setOpacity(0);
        setVisible(false);
    }
}

