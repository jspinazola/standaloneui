package sample;

import javafx.application.Platform;

/**
 *
 * @author bbobier
 */
public class PlatformHelper {

  public static void run(Runnable runner) {
    assert runner != null;

    if (Platform.isFxApplicationThread()) {
      runner.run();
    }
    else {
      Platform.runLater(runner);
    }
  }
}
