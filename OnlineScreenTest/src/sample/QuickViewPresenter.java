package sample;

import FPS.FPSData;
import FPS.OnlineSystemData;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import sample.Controller;
//import xos.limitsEditor.demo.Control;

public class QuickViewPresenter extends AbstractFxmlPanelController{

    @FXML
    BorderPane bpRoot;
    @FXML
    Button btnClose;

    private OnlineSystemData data;
    private Controller control;

    public QuickViewPresenter() {
        super(QuickViewPresenter.class.getResource("quickView.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        control.setButtonIcons(btnClose, FontAwesomeIcon.CLOSE, ContentDisplay.TOP, Color.web(XosColors.XOS_BLUE));
    }

    public void setData(OnlineSystemData data) {
        this.data = data;
    }

    public void show(){
        ResultsSummary summary = new ResultsSummary();
        summary.setData(data);
        summary.refreshContent();
        bpRoot.setCenter(summary.getPanelRoot());
        control.showModalMessage(getPanelRoot(),true);
    }

    @FXML
    void handleClose() {
        control.hideModalMessage();
    }
}
