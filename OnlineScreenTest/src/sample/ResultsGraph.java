package sample;

import FPS.OnlineSystemData;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import javafx.util.converter.LocalDateTimeStringConverter;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class ResultsGraph extends AbstractFxmlPanelController{

    private ArrayList<OnlineSystemData> data;
    private Controller control;

    @FXML
    private VBox box;
    NumberAxis xAxis = new NumberAxis();
    NumberAxis yAxis = new NumberAxis();
    //creating the chart
    LineChart lineChart = new LineChart(xAxis,yAxis);
    XYChart.Series series = new XYChart.Series();

    public ResultsGraph() {
        super(ResultsSummary.class.getResource("resultsGraph.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        xAxis.setLabel("Measurement");
        yAxis.setLabel("Concentration: PPM");
        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            private final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd HH:mm");
            @Override
            public String toString(Number object) {
                LocalDateTime time = LocalDateTime.ofEpochSecond(object.longValue(),0,ZoneOffset.UTC);
                return formatter.format(time);
            }
            @Override
            public Number fromString(String string) {
                return null;
            }
        });
        xAxis.setAutoRanging(false);
    }

    private void update(){
        int i = 0;
        box.getChildren().clear();
        lineChart.getData().clear();
        series.getData().clear();
        Long min = null;
        Long max = null;
        for (OnlineSystemData point : data) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime date = LocalDateTime.parse(point.getTimestamp(),formatter);
            Double value = point.getConvertedValue("ppm_process");
            long time = date.toEpochSecond(ZoneOffset.UTC);
            if(min == null || time < min){
                min = time;
            }
            if(max == null || time > max){
                max = time;
            }
            series.getData().add(new XYChart.Data(time,value));
            i++;
        }
        xAxis.setLowerBound(min);
        xAxis.setUpperBound(max);
        xAxis.setTickUnit((max-min)/10);
        lineChart.getData().add(series);
        lineChart.setLegendVisible(false);
        box.getChildren().add(lineChart);
    }

    public void setData(ArrayList<OnlineSystemData> data){
        this.data = data;
        update();
    }
}
