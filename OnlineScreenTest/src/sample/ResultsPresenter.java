package sample;

import FPS.FPSData;
import FPS.OnlineSystemData;
import FPS.OnlineSystemStatus;
import FPS.SQLManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;


public class ResultsPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;
    private ResultsSummary summary;
    private ResultsGraph graph;
    private SQLManager sql;

    @FXML
    VBox box;

    private OnlineSystemData data;
    private OnlineSystemStatus status;

    public ResultsPresenter(){
        super(ResultsPresenter.class.getResource("Results.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        sql = model.getSql();
        summary = new ResultsSummary();
        graph = new ResultsGraph();
        box.getChildren().add(summary.getPanelRoot());
        box.getChildren().add(graph.getPanelRoot());
        model.getOnlineDataProperty().addListener(((observable, oldValue, newValue) -> {update();}));
        update();
    }

    private void update(){
        data = model.getOnlineData();
        status = model.getOnlineStatus();
        summary.setData(data);
        summary.refreshContent();
        graph.setData(sql.getOnlineDataEntriesFrom(100,sql.getNumberOfOnlineDataEntries()));
    }

    public void refreshContent(){
        update();
    }

}

