package sample;

import FPS.FPSData;
import FPS.OnlineSystemData;
import FPS.OnlineSystemStatus;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

public class ResultsSummary extends AbstractFxmlPanelController{

    @FXML
    FlowPane flowPane;
    @FXML
    Label TestLabel;
    @FXML
    Label kvValue;
    @FXML
    Label maValue;
    @FXML
    Label detValue;
    @FXML
    Label timeValue;
    @FXML
    Label valIntValue;
    @FXML
    Label lastValValue;
    @FXML
    Label calBGValue;
    @FXML
    Label calSlopeValue;

    @FXML
    Label ppmValue;
    @FXML
    Label driftCorrValue;
    @FXML
    Label countsValue;

    private OnlineSystemData data;
    private Controller control;

    public ResultsSummary() {
        super(ResultsSummary.class.getResource("resultsSummary.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        flowPane.setAlignment(Pos.CENTER);
    }

    public void refreshContent(){
        TestLabel.setText(data.getTimestamp());
        kvValue.setText(String.format("%.2f",data.getConvertedValue("kv_ctl")));
        maValue.setText(String.format("%.2f",data.getConvertedValue("ma_ctl")));
        detValue.setText(String.format("%.2f",data.getConvertedValue("kV_det_ctl")));
        timeValue.setText(String.valueOf(data.getValue("counting_time")));
        valIntValue.setText(String.format("%.2f",data.getConvertedValue("val_interval")));
        lastValValue.setText(String.format("%.2f",data.getConvertedValue("last_val_ppm")));
        calBGValue.setText(String.valueOf(data.getValue("cal_background_factor")));
        calSlopeValue.setText(String.format("%.2f",data.getConvertedValue("cal_slope")));
        ppmValue.setText(String.format("%.2f",data.getConvertedValue("ppm_process")));
        driftCorrValue.setText(String.format("%.2f",data.getConvertedValue("ppm_drift_corr")));
        countsValue.setText(String.valueOf(data.getValue("last_counts")));
    }

    public void setData(OnlineSystemData data){
        this.data = data;
    }
}
