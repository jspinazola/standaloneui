package sample;


import FPS.MeasurementData;
import com.sun.javafx.scene.control.skin.FXVK;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import FPS.ScanInfo;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

public class ScanInfoPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;
    private String url1 = "http://ucld.us/q.ashx?b=";
    private String url2 = "https://ucld.us/q.ashx?b=";

    //Flush and Purge Labeling Reversed to Accommodate Sam + Eric

    @FXML
    private Label mainLabel;
    @FXML
    private Label historyLabel;
    @FXML
    private Button setStatusButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField entry;
    @FXML
    private FlowPane flowPane;
    @FXML
    private TextField setpoint0;
    @FXML
    private TextField setpoint1;
    @FXML
    private TextField measurementTime;
    @FXML
    private TextField pumpSpeed;
    @FXML
    private TextField flushSpeed;
    @FXML
    private TextField flushTime;
    @FXML
    private TextField purgeSpeed;
    @FXML
    private TextField purgeTime;
    private int numCharacters = 0;

    public ScanInfoPresenter(){
        super(ScanInfoPresenter.class.getResource("ScanInfo.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        model.getControlStateProperty().addListener(((observable, oldValue, newValue) -> {
            if(model.getControlState() != SidecarModel.CONTROLSTATE.MEASURING){
                setStatusButton.setDisable(false);
                cancelButton.setVisible(false);
                cancelButton.setDisable(true);
                //entry.setOnAction(this::onEnter);
                entry.clear();
                entry.setPromptText(I18N.getString("ScanInfoPromptText"));
            }
        }));
        model.getConfigObjectProperty().addListener(((observable, oldValue, newValue) -> {updateStatus();}));
        model.getFTIRStatusProperty().addListener(((observable, oldValue, newValue) -> {
            toggleCountdown();
        }));
        entry.setOnMouseClicked(event -> {FXVK.attach(entry);});
        updateStatus();
    }

    private void toggleCountdown(){
    }

    private void updateStatus(){
    }

    /*
    @FXML
    public void onEnter(ActionEvent ae){
        if(model.getSimpleMeasurementMode()){

        }
        handleRun();
        entry.setOnAction(null);
    }*/

    @FXML
    public void handleCancel(){
        control.CANCEL();
        cancelButton.setDisable(true);
    }

    private boolean validateInput(){
        boolean validated = true;
        try {
            if (Integer.parseInt(setpoint0.getText()) > 105 || Integer.parseInt(setpoint0.getText()) < 40) {
                validated = false;
                setpoint0.clear();
            }
            if (Integer.parseInt(measurementTime.getText()) > 600 || Integer.parseInt(measurementTime.getText()) < 60) {
                validated = false;
                measurementTime.clear();
            }
            if (Integer.parseInt(pumpSpeed.getText()) > 10 || Integer.parseInt(pumpSpeed.getText()) < 1) {
                validated = false;
                pumpSpeed.clear();
            }
            if (Integer.parseInt(flushSpeed.getText()) > 10 || Integer.parseInt(flushSpeed.getText()) < 1) {
                validated = false;
                flushSpeed.clear();
            }
            if (Integer.parseInt(flushTime.getText()) > 300 || Integer.parseInt(flushTime.getText()) < 20) {
                validated = false;
                flushTime.clear();
            }
            if (Integer.parseInt(purgeSpeed.getText()) > 10 || Integer.parseInt(purgeSpeed.getText()) < 1) {
                validated = false;
                flushSpeed.clear();
            }
            if (Integer.parseInt(purgeTime.getText()) > 300 || Integer.parseInt(purgeTime.getText()) < 20) {
                validated = false;
                flushTime.clear();
            }
            return validated;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    @FXML
    public void handleRun(){
        int runSetpoints = 0;
        control.MEASURE();
        setStatusButton.setDisable(true);
        cancelButton.setVisible(true);
        cancelButton.setDisable(false);
    }

    /**
     * Appends a character to the text content of <code>tfSampleID</code> as
     * follows:
     * Once an "=" character has been entered, clear the contents of
     * <code>tfSampleID</code> and reset the number of characters that have been
     * entered. For the following NUM_CHARS_IN_LABEL (i.e. 10) characters,
     * append them to the value displayed in <code>tfSampleID</code>. Finally,
     * set the flag <code>allowTextEntry</code> allowTextEntry to false, so that
     * when the next QR code is read in, the process can start again.
     * <p>
     * The raw string that is read from the sample bottle provided to us is:
     * http://ucld.us/q.ashx?b=b000160267
     * <p>
     * Note: if the user has an attached keyboard and types "=", the following
     * 10 characters they type will be entered into <code>tfSampleID</code>.
     * A more robust solution will need to note the amount of time between
     * keystrokes, as the QR scanner enters the text very quickly.
     * See: http://stackoverflow.com/questions/31513374/how-can-read-input-from-barcode-scanner-with-javafx
     *
     * @param character The character to be appended.
     */
    public void appendCharacter(String character) {
        if ("=".equals(character)) {
            entry.setText("");
            numCharacters = 0;
        }
        //Check that the character is not empty, as on OS X at least, the character string includes a preceding Shift
        // key for an upper case character
        else if (!character.isEmpty()) {
            entry.appendText(character);
            numCharacters++;
        }
    }

    public void addText(){
        if(entry.getText().equals("")) {
            entry.requestFocus();

        }
        if (entry.getText().equals(url1) || entry.getText().equals(url2)){
            entry.setText("");
            FXVK.detach();
        }
    }

    public void refreshContent(){
        if(model.getControlState() != SidecarModel.CONTROLSTATE.MEASURING) {
            setStatusButton.setDisable(false);
            cancelButton.setVisible(false);
            cancelButton.setDisable(true);
        }
        updateStatus();
    }
}
