package sample;

import FPS.OnlineSystemData;
import FPS.OnlineSystemSetting;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.controlsfx.control.ToggleSwitch;
import javafx.scene.control.Label;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class SettingsPresenter extends AbstractFxmlPanelController {
    private Controller control;
    private SidecarModel model;
    private InetAddress ipAddress;
    private OnlineSystemSetting setting;
    private OnlineSystemSetting newSettings;
    private OnlineSystemData data;

    @FXML
    private Label IP;

    @FXML
    private Label FW;

    @FXML
    private Label filmAdvanceIntervalLabel;
    @FXML
    private TextField filmAdvanceInterval;
    @FXML
    private Label trendCountingTimeLabel;
    @FXML
    private TextField trendCountingTime;
    @FXML
    private Label measureCountingTimeLabel;
    @FXML
    private TextField measureCountingTime;
    @FXML
    private Label windowFlushTimeLabel;
    @FXML
    private TextField windowFlushTime;
    @FXML
    private Label valRepeatsLabel;
    @FXML
    private TextField valRepeats;
    @FXML
    private Label driftCorrRepeatsLabel;
    @FXML
    private TextField driftCorrRepeats;
    @FXML
    private Label ppmLowScaleLabel;
    @FXML
    private TextField ppmLowScale;
    @FXML
    private Label ppmHighScalelabel;
    @FXML
    private TextField ppmHighScale;
    @FXML
    private Label valIntervalLabel;
    @FXML
    private TextField valInterval;
    @FXML
    private Label driftIntervalLabel;
    @FXML
    private TextField driftInterval;
    @FXML
    private Label analyzerLoopFlowLabel;
    @FXML
    private TextField analyzerLoopFlow;
    @FXML
    private Label fastLoopFlowLabel;
    @FXML
    private TextField fastLoopFlow;
    @FXML
    private ToggleSwitch lowFilmAlertEnable;
    @FXML
    private ToggleSwitch autoRestartEnable;
    @FXML
    private ToggleSwitch driftCorrAdjEnable;
    @FXML
    private ToggleSwitch SIVSEnable;
    @FXML
    private ToggleSwitch switchStreamsEnable;
    @FXML
    private ToggleSwitch filmAdvanceEnable;
    @FXML
    private Button sendSettings;

    public SettingsPresenter(){
        super(SettingsPresenter.class.getResource("settings.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();

        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ipAddress = socket.getLocalAddress();
        }catch (SocketException e){
            System.out.println("Can't IP 1");
        }catch (UnknownHostException ie){ System.out.println("Can't IP 2");}
        IP.setText(ipAddress.getHostAddress().trim());
        model.getOnlineDataProperty().addListener(((observable, oldValue, newValue) -> {
            refreshContent();
        }));
        filmAdvanceInterval.textProperty().addListener(((observable, oldValue, newValue) -> {
            if(filmAdvanceInterval.getText() != ""){
                model.addNewSetting("film_adv_int",Integer.parseInt(filmAdvanceInterval.getText()));
            }
        }));
        trendCountingTime.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("counting_time_trend",trendCountingTime.getText());
        }));
        measureCountingTime.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("counting_time",(measureCountingTime.getText()));
        }));
        windowFlushTime.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("dwm_flush_time",windowFlushTime.getText());
        }));
        valRepeats.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("val_repeats",(valRepeats.getText()));
        }));
        driftCorrRepeats.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("drift_corr_repeats",(driftCorrRepeats.getText()));
        }));
        ppmLowScale.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("ppm_low_scale",(ppmLowScale.getText()));
        }));
        ppmHighScale.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("ppm_high_scale",(ppmHighScale.getText()));
        }));
        valInterval.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("val_interval",(valInterval.getText()));
        }));
        driftInterval.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("drift_correction_interval",(driftInterval.getText()));
        }));
        analyzerLoopFlow.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("analyzer_low_flow",(analyzerLoopFlow.getText()));
        }));
        fastLoopFlow.textProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewSetting("fast_loop_low_flow",(fastLoopFlow.getText()));
        }));
        lowFilmAlertEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("enable_low_film_alert",lowFilmAlertEnable.isSelected());
        }));
        autoRestartEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("enable_autorestart",autoRestartEnable.isSelected());
        }));
        driftCorrAdjEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("enable_driftcorradj",driftCorrAdjEnable.isSelected());
        }));
        SIVSEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("enable_sivs",SIVSEnable.isSelected());
        }));
        switchStreamsEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("stream_switching",switchStreamsEnable.isSelected());
        }));
        filmAdvanceEnable.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.addNewBooleanSetting("enable_film_adv",filmAdvanceEnable.isSelected());
        }));
    }

    public void refreshContent(){
        setting = model.getOnlineSettings();
        data = model.getOnlineData();
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ipAddress = socket.getLocalAddress();
        }catch (SocketException e){
            System.out.println("Can't IP 1");
        }catch (UnknownHostException ie){ System.out.println("Can't IP 2");}
        IP.setText(ipAddress.getHostAddress().trim());
        FW.setText(String.valueOf(data.getValue("software_version")));
        filmAdvanceIntervalLabel.setText("Film Advance Interval");
        filmAdvanceInterval.promptTextProperty().set(String.valueOf(setting.getValue("film_adv_int")));
        trendCountingTimeLabel.setText("Trend Counting Time");
        trendCountingTime.promptTextProperty().set(String.valueOf(setting.getValue("counting_time_trend")));
        measureCountingTimeLabel.setText("Measure Counting Time");
        measureCountingTime.promptTextProperty().set(String.valueOf(setting.getValue("counting_time")));
        windowFlushTimeLabel.setText("Window Flush Time");
        windowFlushTime.promptTextProperty().set(String.valueOf(setting.getValue("dwm_flush_time")));
        valRepeatsLabel.setText("Val Repeats");
        valRepeats.promptTextProperty().set(String.valueOf(setting.getValue("val_repeats")));
        driftCorrRepeatsLabel.setText("Drift Corr Repeats");
        driftCorrRepeats.promptTextProperty().set(String.valueOf(setting.getValue("drift_corr_repeats")));
        ppmLowScaleLabel.setText("PPM Low Scale");
        ppmLowScale.promptTextProperty().set(String.valueOf(setting.getValue("ppm_low_scale")));
        ppmHighScalelabel.setText("PPM High Scale");
        ppmHighScale.promptTextProperty().set(String.valueOf(setting.getValue("ppm_high_scale")));
        valIntervalLabel.setText("Val Interval");
        valInterval.promptTextProperty().set(String.valueOf(setting.getValue("val_interval")));
        driftIntervalLabel.setText("Drift Corr Interval");
        driftInterval.promptTextProperty().set(String.valueOf(setting.getValue("drift_correction_interval")));
        analyzerLoopFlowLabel.setText("Analyzer min Flow");
        analyzerLoopFlow.promptTextProperty().set(String.valueOf(setting.getValue("analyzer_low_flow")));
        fastLoopFlowLabel.setText("Fast Loop min Flow");
        fastLoopFlow.promptTextProperty().set(String.valueOf(setting.getValue("fast_loop_low_flow")));
        lowFilmAlertEnable.setSelected(setting.getBoolean("enable_low_film_alert"));
        autoRestartEnable.setSelected(setting.getBoolean("enable_autorestart"));
        driftCorrAdjEnable.setSelected(setting.getBoolean("enable_driftcorradj"));
        SIVSEnable.setSelected(setting.getBoolean("enable_sivs"));
        switchStreamsEnable.setSelected(setting.getBoolean("stream_switching"));
        filmAdvanceEnable.setSelected(setting.getBoolean("enable_film_adv"));
    }
    @FXML
    private void handleSend(){
        control.SENDSETTINGS();
        filmAdvanceInterval.clear();
        trendCountingTime.clear();
        measureCountingTime.clear();
        windowFlushTime.clear();
        valRepeats.clear();
        driftCorrRepeats.clear();
        ppmLowScale.clear();
        ppmHighScale.clear();
        valInterval.clear();
        driftInterval.clear();
        analyzerLoopFlow.clear();
        fastLoopFlow.clear();
    }

}
