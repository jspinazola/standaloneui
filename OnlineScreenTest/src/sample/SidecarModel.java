package sample;

import FPS.*;
import FPS.FTIR.FTIRData;
import FPS.FTIR.FTIRStatus;
import Util.SystemConfig;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import FPS.FPSSystemState;
import FPS.FTIR.FTIRStatus;
import sample.Util.SidecarConfig;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SidecarModel{

    private StringProperty message = new SimpleStringProperty();
    private StringProperty settingsMessage = new SimpleStringProperty();
    private StringProperty dataMessage = new SimpleStringProperty();
    private StringProperty filmAdvanceMessage =new SimpleStringProperty();

    private ObjectProperty<SystemConfig> config = new SimpleObjectProperty();

    private ObjectProperty<ScanInfo> scanInfo = new SimpleObjectProperty<>();
    private ObjectProperty<FPSData> lastMeasurement = new SimpleObjectProperty<>();
    private ObjectProperty<FTIRData> lastFTIR = new SimpleObjectProperty<>();
    private ObjectProperty<CONTROLSTATE> controlState = new SimpleObjectProperty<>();
    private ObjectProperty<FPSSystemState> fpsStatus = new SimpleObjectProperty<>();
    private ObjectProperty<FTIRStatus> ftirStatus = new SimpleObjectProperty<>();

    private ObjectProperty<OnlineSystemStatus> onlineStatus = new SimpleObjectProperty<>();
    private ObjectProperty<OnlineSystemData> onlineData = new SimpleObjectProperty<>();
    private ObjectProperty<OnlineSystemEvent> onlineEvent = new SimpleObjectProperty<>();
    private ObjectProperty<OnlineSystemSetting> onlineSetting = new SimpleObjectProperty<>();
    private OnlineSystemSetting newSettings = new OnlineSystemSetting();
    private ObjectProperty<OnlineSystemFilmAdvance> filmAdvance = new SimpleObjectProperty<>();

    private StringProperty rep = new SimpleStringProperty();
    private BooleanProperty readingQRProperty = new SimpleBooleanProperty();

    private SQLManager sql = new SQLManager("OnlineDatabase");

    public enum CONTROLSTATE {
        UNINIT,
        INIT,
        WAITING,
        WAITINGTEST,
        MEASURING,
        FINISHED,
        CUSTOM_MEASUREMENT,
        FTIR,
        FTIRBG,
        FTIRPURGE,
        FTIRDRAIN,
        DISCONNECTED,
        FILM_ADVANCE,
        ERROR
    }


    public SidecarModel (){
        config.setValue(new SystemConfig());
        lastMeasurement.setValue(new FPSData());
        scanInfo.setValue(new ScanInfo());
        controlState.setValue(CONTROLSTATE.UNINIT);
        fpsStatus.setValue(new FPSSystemState());
        ftirStatus.setValue(new FTIRStatus());
        onlineStatus.setValue(new OnlineSystemStatus());
        onlineSetting.setValue(new OnlineSystemSetting());
        onlineData.setValue(sql.getLastOnlineDataPoint());
        filmAdvance.setValue(new OnlineSystemFilmAdvance());
        config.getValue().readFromFile("SidecarConfig.json");
        System.out.println("Did it take: " + config.getValue().getRemoteIP());
        message.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                convertToJson();
            }
        });
        settingsMessage.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                convertToJsonSettings();
            }
        });
        dataMessage.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                convertToJsonData();
            }
        });
        filmAdvanceMessage.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                convertToJsonFilm();
            }
        });
    }

    public SQLManager getSql(){
        return sql;
    }

    public StringProperty getMessage() {
        return message;
    }
    public StringProperty getSettingsMessage(){return settingsMessage;}
    public StringProperty getDataMessage(){return dataMessage;}
    public StringProperty getFilmAdvanceMessage(){return filmAdvanceMessage;}

    public void setMessage(StringProperty message){
        this.message = message;
    }

    private void convertToJson(){
        //System.out.println( "Converting Message - " + message.get());
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(message.get());
        }catch (ParseException e){
            System.out.println("Could Not Parse JSON");
        }
        for(Object key : json.keySet()){
            try {
                switch (key.toString()) {
                    case "controlState":{
                        controlState.setValue(CONTROLSTATE.valueOf(json.get(key).toString()));
                    }
                    break;
                    case "ftir":{
                        ObjectMapper ob = new ObjectMapper();
                        try{
                            ftirStatus.setValue(ob.readValue(json.get(key).toString(),FTIRStatus.class));
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                    case "fps":{
                        ObjectMapper ob = new ObjectMapper();
                        try{
                            fpsStatus.setValue(ob.readValue(json.get(key).toString(),FPSSystemState.class));
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                    case "onlineStatus":{
                        try{
                            OnlineSystemStatus status = new OnlineSystemStatus();
                            status.parseJSONData(json.get(key).toString());
                            onlineStatus.setValue(status);
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                    default: {
                        System.out.println(key.toString());
                    }
                    break;
                }
            }catch (Exception e){
                System.out.println(key);
                System.out.println(json.get(key).toString());
           }
        }
    }

    private void convertToJsonSettings(){
        //System.out.println( "Converting Message - " + message.get());
        updateOnlineSettings(settingsMessage.getValue());
    }

    private void convertToJsonData(){
        //System.out.println( "Converting Message - " + message.get());
        updateOnlineData(dataMessage.getValue());
    }

    private void convertToJsonFilm(){
        //System.out.println( "Converting Message - " + message.get());
        updateOnlineFilmAdvance(filmAdvanceMessage.getValue());
    }

    public CONTROLSTATE getControlState(){return controlState.getValue();}
    public ObjectProperty getControlStateProperty(){return controlState;}
    public StringProperty getRep(){return rep;}
    public void setRep(String value){rep.setValue(value);}


    public void setLastMeasurement(String data){
        FPSData newData = new FPSData();
        newData.parseJSONData(data);
        lastMeasurement.setValue(newData);
    }

    public void updateOnlineFilmAdvance(String data){
        OnlineSystemFilmAdvance newFilm = new OnlineSystemFilmAdvance();
        newFilm.parseJSONData(data);
        filmAdvance.setValue(newFilm);
    }

    public void updateOnlineData(String data){
        OnlineSystemData newData = new OnlineSystemData();
        newData.parseJSONData(data);
        onlineData.setValue(newData);
    }

    public void updateOnlineSettings(String settings){
        OnlineSystemSetting newSettings = new OnlineSystemSetting();
        newSettings.parseJSONData(settings);
        onlineSetting.setValue(newSettings);
    }
    public void updateOnlineSettings(OnlineSystemSetting settings){this.newSettings = settings;}
    public OnlineSystemSetting getNewOnlineSettings(){
        return newSettings;
    }
    public void addNewSetting(String setting, int value){
        newSettings.updateValue(setting,value);
    }
    public void addNewSetting(String setting, String value){
        newSettings.updateValue(setting,value);
    }
    public void addNewBooleanSetting(String setting, boolean value){
        newSettings.updateBooleanValue(setting,value);
    }

    public FPSData getLastMeasurement(){ return lastMeasurement.getValue();}
    public ObjectProperty getLastMeasurementProperty(){return lastMeasurement;}

    public void setLastFTIR(String data){
       FTIRData newData = new FTIRData();
       newData.parseJSON(data);
       lastFTIR.setValue(newData);
    }
    public FTIRData getLastFTIR(){return lastFTIR.getValue();}
    public ObjectProperty getLastFTIRProperty(){return lastFTIR;}

    public void setScanInfo(ScanInfo scanInfo) {
        this.scanInfo.setValue(scanInfo);
    }
    public ScanInfo getScanInfo(){return scanInfo.get();}

    public boolean isReadingQR() {
        return readingQRProperty.get();
    }

    public BooleanProperty readingQRProperty() {
        return readingQRProperty;
    }

    public void setReadingQR(boolean readingQR) {
        this.readingQRProperty.set(readingQR);
    }

    public ObjectProperty getFPSStatusProperty(){return fpsStatus;}
    public FPSSystemState getFPSStatus(){return fpsStatus.getValue();}

    public ObjectProperty getFTIRStatusProperty(){return ftirStatus;}
    public FTIRStatus getFTIRStatus(){return ftirStatus.getValue();}

    public ObjectProperty getOnlineStatusProperty(){return onlineStatus;}
    public OnlineSystemStatus getOnlineStatus(){return onlineStatus.getValue();}

    public ObjectProperty getOnlineDataProperty(){return onlineData;}
    public OnlineSystemData getOnlineData(){return onlineData.getValue();}

    public ObjectProperty getOnlineSettingsProperty(){return onlineSetting;}
    public OnlineSystemSetting getOnlineSettings(){return onlineSetting.getValue();}

    public ObjectProperty getOnlineFilmAdvanceProperty(){return filmAdvance;}
    public OnlineSystemFilmAdvance getFilmAdvance(){return filmAdvance.getValue();}

    public SystemConfig getConfig(){return config.getValue();}
    public void setConfig(SystemConfig config) {this.config.set(config);}
    public ObjectProperty getConfigObjectProperty(){return config;}
}
