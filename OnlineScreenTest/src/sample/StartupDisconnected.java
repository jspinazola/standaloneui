package sample;

import FPS.OnlineSystemStatus;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import sample.AbstractFxmlPanelController;
import sample.Controller;
import sample.I18N;
import sample.ResultsSummary;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StartupDisconnected extends AbstractFxmlPanelController {

    @FXML
    private FlowPane pane;
    @FXML
    private Label paneTitle;

    public StartupDisconnected(){
        super(ResultsSummary.class.getResource("startupDisconnected.fxml"), I18N.getBundle());
        makePanel();
        update();
    }

    public FlowPane getPane(){
        return pane;
    }

    public ObservableList<Node> getChildren(){
        update();
        return pane.getChildren();
    }

    private void update(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        paneTitle.setText(formatter.format(date));
    }

}
