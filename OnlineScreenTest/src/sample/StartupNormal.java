package sample;

import FPS.OnlineSystemData;
import FPS.OnlineSystemStatus;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StartupNormal extends AbstractFxmlPanelController{

    private OnlineSystemStatus status;
    private OnlineSystemData data;
    private Controller control;

    @FXML
    private FlowPane pane;
    @FXML
    private Label paneTitle;
    @FXML
    private Label timeTitle;
    @FXML
    private Label timeRemaining;
    @FXML
    private Label timeUnit;
    @FXML
    private Label countsTitle;
    @FXML
    private Label counts;
    @FXML
    private Label lastPPMTitle;
    @FXML
    private Label lastPPM;
    @FXML
    private Label trendTitle;
    @FXML
    private Label trend;

    public StartupNormal(){
        super(ResultsSummary.class.getResource("startupNormal.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        status = new OnlineSystemStatus();
        data = new OnlineSystemData();
        updateStatus();
    }

    public void setStatus(OnlineSystemStatus status){
        this.status = status;
        updateStatus();
    }

    public void setData(OnlineSystemData data){
        this.data = data;
    }

    public FlowPane getPane(){
        return pane;
    }


    public ObservableList<Node> getChildren(){
        return pane.getChildren();
    }

    private void updateStatus(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        paneTitle.setText(formatter.format(date));
        timeTitle.setText("Time Remaining: ");
        timeRemaining.setText(String.valueOf(status.getValue("measure_time_remaining")));
        timeUnit.setText("sec");
        countsTitle.setText("Counts: ");
        counts.setText(String.valueOf(status.getValue("counts")));
        lastPPMTitle.setText("Last PPM: ");
        lastPPM.setText(String.format("%.2f",data.getConvertedValue("ppm_process")));
        trendTitle.setText("Trend: ");
        trend.setText(String.format("%.2f",status.getConvertedValue("ppm_trend")));
    }
}
