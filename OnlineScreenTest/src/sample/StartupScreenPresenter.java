package sample;

import FPS.OnlineSystemStatus;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class StartupScreenPresenter extends AbstractFxmlPanelController{
    private Controller control;
    private SidecarModel model;
    private OnlineSystemStatus status;
    private StartupNormal normal;
    private StartupDisconnected disconnected;
    private FilmAdvance filmAdvance;

    @FXML
    private Label doorStatus;
    @FXML
    private Label codeVersion;
    @FXML
    private ImageView logo;
    //@FXML
    //private FlowPane pane;
    @FXML
    private VBox box;
    @FXML
    private Button abort;
    @FXML
    private Button alert;
    @FXML
    private Button start;


    public StartupScreenPresenter() {
        super(StartupScreenPresenter.class.getResource("StartupScreen.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        model.getOnlineStatusProperty().addListener(((observable, oldValue, newValue) -> {updateStatus();}));
        model.getControlStateProperty().addListener(((observable, oldValue, newValue) -> {changedControlState((SidecarModel.CONTROLSTATE) newValue, (SidecarModel.CONTROLSTATE) oldValue);}));
        Image pic = new Image("file:SINDIE 7039 logo.png");
        logo.setImage(pic);
        status = new OnlineSystemStatus();
        normal = new StartupNormal();
        normal.setStatus(status);
        disconnected = new StartupDisconnected();
        filmAdvance = new FilmAdvance();
    }

    private void changedControlState(SidecarModel.CONTROLSTATE newState, SidecarModel.CONTROLSTATE oldState){
        switch (newState){
            case WAITING,MEASURING,FINISHED:
                box.getChildren().remove(disconnected.getPane());
                if (!box.getChildren().contains(normal.getPane())){
                    box.getChildren().add(normal.getPane());
                }
                break;
            case DISCONNECTED:
                box.getChildren().remove(normal.getPane());
                if (!box.getChildren().contains(disconnected.getPane())){
                    box.getChildren().add(disconnected.getPane());
                }
                break;
            default:
                break;
        }
        updateStatus();
    }

    private void updateStatus(){
        status = model.getOnlineStatus();
        if(model.getOnlineStatus().getValue("process_alarm") == 1 || model.getOnlineStatus().getValue("high_level_alarm")==1){
            alert.setVisible(true);
        }else {
            alert.setVisible(false);
        }

        if(model.getControlState() == SidecarModel.CONTROLSTATE.MEASURING || model.getControlState() == SidecarModel.CONTROLSTATE.WAITING || model.getControlState() == SidecarModel.CONTROLSTATE.FINISHED ){
            abort.setVisible(true);
            start.setVisible(false);
            normal.setStatus(status);
            normal.setData(model.getOnlineData());
        }else if(model.getControlState() == SidecarModel.CONTROLSTATE.DISCONNECTED){
        }
        else{
            abort.setVisible(false);
            start.setVisible(true);
        }
    }

    @FXML
    private void handleAlert(){
        System.out.println("Alert");
    }
    @FXML
    private void handleAbort(){
        System.out.println("Abort");
    }
    @FXML
    private void handleStart(){
        System.out.println("Start");
    }

    public void refreshContent(){
        updateStatus();
    }
}
