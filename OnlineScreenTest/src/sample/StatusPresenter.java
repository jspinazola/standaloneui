package sample;

import FPS.FPSSystemState;
import FPS.FTIR.FTIRState;
import FPS.OnlineSystemStatus;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import eu.hansolo.medusa.*;

public class StatusPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;
    private FPSSystemState converter;
    private OnlineSystemStatus onlineSystemStatustatus;

    @FXML
    private Label statusLabel;
    @FXML
    private Label elapsedTime;
    @FXML
    private Label mAMonitor;
    @FXML
    private Label kVMonitor;
    @FXML
    private Label vacuumMonitor;
    @FXML
    private Label tempMonitor;
    @FXML
    private Label status;
    @FXML
    private Gauge mAGauge;
    @FXML
    private Gauge kVGauge;
    @FXML
    private Gauge vacGauge;
    @FXML
    private Gauge tempGauge;
    @FXML
    private Gauge detGauge;
    @FXML
    private Gauge airTempGauge;
    @FXML
    private Gauge filCurrentGauge;
    @FXML
    private Gauge fuelPressureGauge;
    @FXML
    private Label mAGaugeLabel;
    @FXML
    private Label kVGaugeLabel;
    @FXML
    private Label vacGaugeLabel;
    @FXML
    private Label tempGaugeLabel;

    public StatusPresenter() {
        super(StatusPresenter.class.getResource("status.fxml"), I18N.getBundle());
        makePanel();
        converter = new FPSSystemState();
        control = Controller.getInstance();
        model = control.getModel();
        model.getFPSStatusProperty().addListener((observable, oldValue, newValue) -> updateStatus());
        model.getOnlineStatusProperty().addListener((observable, oldValue, newValue) -> updateStatus());
        setUpGauge();
        updateStatus();
    }

    private void setUpGauge(){
        kVGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        kVGauge.setMaxValue(60.0);
        kVGauge.setTitle("Source kV");
        kVGauge.setDecimals(2);
        mAGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        mAGauge.setMaxValue(1.60);
        mAGauge.setDecimals(2);
        mAGauge.setTitle("Source mA");
        vacGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        vacGauge.setMaxValue(25.0);
        vacGauge.setDecimals(2);
        vacGauge.setTitle("Vacuum (Torr)");
        detGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        detGauge.setMaxValue(2.0);
        detGauge.setDecimals(2);
        detGauge.setTitle("Detector Voltage (kV)");
        airTempGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        airTempGauge.setMaxValue(100);
        airTempGauge.setDecimals(2);
        airTempGauge.setTitle("Box Temp (F)");

        filCurrentGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        filCurrentGauge.setMaxValue(3.5);
        filCurrentGauge.setDecimals(2);
        filCurrentGauge.setTitle("Filament Current (A)");

        fuelPressureGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        fuelPressureGauge.setMaxValue(100);
        fuelPressureGauge.setDecimals(1);
        fuelPressureGauge.setTitle("Fuel Pressure (PSI)");

        tempGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        tempGauge.setMaxValue(160.0);
        tempGauge.setTitle("Source Temperature (F)");
        mAGauge.managedProperty().bind(mAGauge.visibleProperty());
        kVGauge.managedProperty().bind(kVGauge.visibleProperty());
        vacGauge.managedProperty().bind(vacGauge.visibleProperty());
        tempGauge.managedProperty().bind(tempGauge.visibleProperty());

    }

    private void updateStatus() {
        onlineSystemStatustatus = model.getOnlineStatus();
        if(model.getControlState() == SidecarModel.CONTROLSTATE.MEASURING){
            status.setText(model.getControlState().toString());
        }else if(model.getControlState() == SidecarModel.CONTROLSTATE.WAITING || model.getControlState() == SidecarModel.CONTROLSTATE.ERROR){
            status.setText(model.getControlState().toString());
        }
        else {
            if(model.getControlState() != null){
                status.setText(model.getControlState().toString());
            }
        }
        elapsedTime.setText("Elapsed Time: " + Integer.toString(onlineSystemStatustatus.getValue("measure_time_remaining")));
        mAGauge.setValue(onlineSystemStatustatus.getConvertedValue("mA_mon"));
        kVGauge.setValue(onlineSystemStatustatus.getConvertedValue("kV_mon"));
        vacGauge.setValue(onlineSystemStatustatus.getConvertedValue("chamber_Vac_mon"));
        detGauge.setValue(onlineSystemStatustatus.getConvertedValue("det_kV_Mon"));
        tempGauge.setValue(onlineSystemStatustatus.getConvertedValue("source_temp"));
        airTempGauge.setValue(onlineSystemStatustatus.getConvertedValue("box_air_temp"));
        filCurrentGauge.setValue(onlineSystemStatustatus.getConvertedValue("fil_current"));
        fuelPressureGauge.setValue(onlineSystemStatustatus.getConvertedValue("fuel_press_mon"));
    }


    private void setModel(SidecarModel model){
        this.model = model;
    }

    public void refreshContent(){

    }
}