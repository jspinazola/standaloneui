package sample;

import FPS.FPSData;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import eu.hansolo.medusa.*;

public class TimerPresenter extends AbstractFxmlPanelController{
    @FXML
    BorderPane bpRoot;
    @FXML
    Button btnClose;
    @FXML
    Label info;
    @FXML
    Gauge time;

    private SidecarModel model;
    private Controller control;

    public TimerPresenter() {
        super(TimerPresenter.class.getResource("timerView.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        control.setButtonIcons(btnClose, FontAwesomeIcon.CLOSE, ContentDisplay.TOP, Color.web(XosColors.XOS_BLUE));
        model = control.getModel();
        setupGauges();
        model.getFTIRStatusProperty().addListener(((observable, oldValue, newValue) -> {
            update();
        }));
    }

    private void setupGauges(){
        info.setText("FTIR Warmup in progress ...");
        time.setSkinType(Gauge.SkinType.TILE_TEXT_KPI);
        time.setTitle("FTIR Warmup Progress");
        time.setValue(0.1);
    }

    public void update(){
        double value = (double) model.getFTIRStatus().getWarmTime()/600L;
        value = 1 - value;
        time.setValue(value*100);
        if(value>=1){
            control.hideModalMessage();
        }
    }

    public void show(){
        control.showModalMessage(getPanelRoot(),true);
    }

    @FXML
    void handleClose() {
        control.hideModalMessage();
    }
}
