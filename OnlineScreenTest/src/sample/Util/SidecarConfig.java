package sample.Util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import java.io.FileReader;
import java.nio.file.Paths;

public class SidecarConfig {

    private boolean simpleMeasurementMode = true;
    private boolean setpointsMeasurementMode = false;
    private boolean runFPS = true;
    private boolean runFTIR = true;
    private boolean testMode = false;
    private boolean pwmViewMode = false;
    private boolean readingQR = false;
    private int ftirTime = 2;
    private int ftirRepeats = 10;
    private String name = "SidecarConfig";
    private String remoteIP = "";

    public SidecarConfig(){}

    @JsonCreator
    public SidecarConfig(
            @JsonProperty("simpleMeasurementMode") boolean simpleMeasurementMode,
            @JsonProperty("runFPS") boolean runFPS,
            @JsonProperty("runFTIR") boolean runFTIR,
            @JsonProperty("testMode") boolean testMode,
            @JsonProperty("pwmViewMode") boolean pwmViewMode,
            @JsonProperty("readingQR") boolean readingQR,
            @JsonProperty("ftirTime") int ftirTime,
            @JsonProperty("remoteIP") String remoteIP
    ){
        this.simpleMeasurementMode = simpleMeasurementMode;
        this.runFPS = runFPS;
        this.runFTIR = runFTIR;
        this.testMode = testMode;
        this.pwmViewMode = pwmViewMode;
        this.readingQR = readingQR;
        this.ftirTime = ftirTime;
        this.remoteIP = remoteIP;
    }

    public void readFromFile(String filename){
        try{
            ObjectMapper mapper = new ObjectMapper();
            ObjectReader reader = mapper.readerForUpdating(this);
            reader.readValue(Paths.get(filename).toFile());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("RemoteIP : " + this.remoteIP);
    }

    public void SaveConfig(){
        try{
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(Paths.get(name + ".json").toFile(),this);
        }catch(Exception e){
            System.out.println("Cannot Save: " + e.getMessage());
        }
    }

    public boolean isSimpleMeasurementMode() {
        return simpleMeasurementMode;
    }

    public void setSimpleMeasurementMode(boolean simpleMeasurementMode) {
        this.simpleMeasurementMode = simpleMeasurementMode;
    }

    public boolean isRunFPS() {
        return runFPS;
    }

    public void setRunFPS(boolean runFPS) {
        this.runFPS = runFPS;
    }

    public boolean isRunFTIR() {
        return runFTIR;
    }

    public void setRunFTIR(boolean runFTIR) {
        this.runFTIR = runFTIR;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public boolean isPwmViewMode() {
        return pwmViewMode;
    }

    public void setPwmViewMode(boolean pwmViewMode) {
        this.pwmViewMode = pwmViewMode;
    }

    public boolean isReadingQR() {
        return readingQR;
    }

    public void setReadingQR(boolean readingQR) {
        this.readingQR = readingQR;
    }

    public int getFtirTime() {
        return ftirTime;
    }

    public void setFtirTime(int ftirTime) {
        this.ftirTime = ftirTime;
    }

    public int getFtirRepeats() {
        return ftirRepeats;
    }

    public void setFtirRepeats(int ftirRepeats) {
        this.ftirRepeats = ftirRepeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSetpointsMeasurementMode() {
        return setpointsMeasurementMode;
    }

    public void setSetpointsMeasurementMode(boolean setpointsMeasurementMode) {
        this.setpointsMeasurementMode = setpointsMeasurementMode;
    }

    public String getRemoteIP() {
        return remoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }
}