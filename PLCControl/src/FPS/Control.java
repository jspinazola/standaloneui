package FPS;

import FPS.FPSControlBoard;
import FPS.COMMAND;

import java.util.Scanner;

import Util.PLC_Comm;
import org.json.simple.JSONObject;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

public class Control {

    private static SQLManager sqlManager = new SQLManager();

    private static FPSControlBoard board;
    private static Thread boardThread;
    private static boolean running = true;
    private static ZMQ.Context ctx = ZMQ.context(1);
    private static ZMQ.Socket rep = ctx.socket(SocketType.REP);
    private static String addressHeader = "tcp://*:";
    private static int portRep = 5557;

    public static void main(String [] args){
        //Check for database connection
        sqlManager.createNewDatabase("OnlineDatabase");
        sqlManager.createNewOnlineDataTable("OnlineData");
        sqlManager.createNewOnlineEventTable("OnlineEvents");

        //Setup FPS Board and zmq req
        setUp();

        //Get input from zmq
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a command ...");

        while(running){
            receiveCommand();
        }
    }

    private static void setUp(){
        board = new FPSControlBoard();
        boardThread = new Thread(board,"boardThread");
        boardThread.start();
        rep.bind(addressHeader+Integer.toString(portRep));
    }

    private static void receiveCommand(){
        String zmqCommand = "";
        String zmqInfo = "";
        zmqCommand = rep.recvStr();
        if(rep.hasReceiveMore()){
            zmqInfo = rep.recvStr();
        }
        try{
            COMMAND request = COMMAND.valueOf(zmqCommand);
            ZMQCommand command = new ZMQCommand(request,zmqInfo);
            System.out.println("Incomming Command: " + request);
            board.addCommand(command);
            //Stop this Control Interface running if told to exit
            if(request == COMMAND.EXIT){
                running = false;
            }
            if(board.isSleeping()){
                boardThread.interrupt();
            }
            rep.send("1,Requested");
        }
        catch (IllegalArgumentException iea){
            System.out.println("Illegal Command");
            rep.send("0,Invalid Command");
        }
    }
}
