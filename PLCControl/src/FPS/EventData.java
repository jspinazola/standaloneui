package FPS;

public class EventData {
    private String timestamp;
    private int id;
    private String name;
    private int value;

    public EventData(){
        timestamp ="";
        id = -1;
        this.name = "";
        this.value = -1;
    }

    public EventData(String name, int value){
        timestamp ="";
        id = -1;
        this.name = name;
        this.value = value;
    }

    public EventData(String timestamp, int id, String name, int value){
        this.timestamp = timestamp;
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
