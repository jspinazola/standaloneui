package FPS;

import FPS.FTIR.*;
import Util.DisconnectedException;
import Util.PLC_Comm;
import Util.SerialPortHelper;
import Util.SystemConfig;
import org.json.simple.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.*;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

public class FPSControlBoard implements Runnable {

    private SystemConfig config;

    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket pub = ctx.socket(SocketType.PUB);
    private String addressHeader = "tcp://*:";
    private int portRep = 5557;
    private int portPub = 5556;

    private SQLManager sqlManager = new SQLManager("OnlineDatabase");
    private int sqlID;

    private CONTROLSTATE controlState = CONTROLSTATE.INIT;

    private PLC_Comm plcComm = new PLC_Comm();
    private OnlineSystemStatus OL_Status = new OnlineSystemStatus();
    private OnlineSystemData OL_Data = new OnlineSystemData();
    private OnlineSystemEvent OL_Event = new OnlineSystemEvent();
    private OnlineSystemEvent OL_lastEvent = new OnlineSystemEvent();
    private OnlineSystemSetting OL_Setting = new OnlineSystemSetting();
    private OnlineSystemFilmAdvance OL_FilmAdv = new OnlineSystemFilmAdvance();

    private boolean running = true;
    private boolean sleeping = false;
    private int refreshTime = 1000;
    private Queue<ZMQCommand> commands= new LinkedList<>();

    private int updateInterval = 5;
    private int updateCounter = 0;

    public enum CONTROLSTATE {
        INIT,
        WAITING,
        MEASURING,
        FINISHED,
        CUSTOM_MEASUREMENT,
        ERROR,
        DISCONNECTED,
        FILM_ADVANCE
    }

    public void addCommand(ZMQCommand command){
        commands.add(command);
    }

    public boolean isSleeping(){
        return sleeping;
    }

    public CONTROLSTATE getControlState(){
        return controlState;
    }

    public void run(){
        boolean passedSetup = false;
        while (running){
            if(commands.size() > 0){
                executeCommand();
            }
            switch (controlState) {
                case WAITING:
                    doWaiting();
                    break;
                case INIT:
                    passedSetup = setUp();
                    break;
                case MEASURING:
                    doMeasuring();
                    break;
                case FINISHED:
                    doFinished();
                    break;
                case CUSTOM_MEASUREMENT:
                    doCustomMeasurement();
                    break;
                case ERROR:
                    doError();
                    break;
                case DISCONNECTED:
                    doDisconnected();
                    break;
                case FILM_ADVANCE:
                    doFilmAdvance();
                    break;
                default:
                    controlState = CONTROLSTATE.INIT;
                    break;
            }
            sleeping = true;
            try {
                Thread.sleep(refreshTime);
            }catch (InterruptedException e){
                System.out.println("Interrupted");
            }
            sleeping =  false;
        }
    }

    public void executeCommand(){
        ZMQCommand in = commands.poll();
        System.out.println(in.getCommand().toString());
        switch (in.getCommand()){
            case MEASURE:
                boolean started = false;
                //Store scan info for after the data has been received from the board
                break;
            case RESET:
                boolean reset = false;
                break;
            case CANCEL:
                boolean cancel = false;
                break;
            case EXIT:
                System.out.println("Exiting");
                cleanup();
                break;
            case SETTINGS:
                System.out.println("New Settings");
                OnlineSystemSetting temp = new OnlineSystemSetting();
                temp.parseJSONData(in.getData());
                uploadSettings(temp);
                updateSettings();
                break;
            default:
                System.out.println("Command not understood");
                break;
        }
    }

    private boolean setUp(){
        System.out.println("Setting system up - 2");
        config = new SystemConfig();
        config.readFromFile("SystemConfig.json");

        //Setup ZMQ publisher
        pub.bind("tcp://*:5556");

        plcComm.Connect(config.getPLCAddress(),502);

        //Check for Success
        if(plcComm.isConnected()){
            updateData();
            updateSettings();
            controlState = CONTROLSTATE.WAITING;
            return  true;
        }else {
            controlState = CONTROLSTATE.DISCONNECTED;
            return false;
        }
    }

    private void doDisconnected(){
        try {
           plcComm.Connect(config.getPLCAddress(),502);
        }catch (Exception e){
            System.out.println("Connection Exception");
        }
        if(plcComm.isConnected()){
            updateStatus();
            updateSettings();
            controlState = CONTROLSTATE.WAITING;
        }
        pub.sendMore("Status");
        pub.send(getStatusJson());
        System.out.println("Disconnected");
    }

    private void doFilmAdvance(){
        updateStatus();
        if(OL_Status.getValue("film_advance_In_Progress") != 1){
            controlState = CONTROLSTATE.WAITING;
            return;
        }
        for(Map.Entry<String, PLC_Resource> entry : OL_FilmAdv.getResources()) {
            try {
                OL_FilmAdv.updateValue((String) entry.getKey(), plcComm.read_PLC_Resource(entry.getValue()));
            }catch (DisconnectedException de){
                controlState = CONTROLSTATE.DISCONNECTED;
                System.out.println("Can't Update: Disconnected");
                break;
            }
        }
        pub.sendMore("FilmAdvance");
        pub.send(OL_FilmAdv.getJSONString());
    }

    private void updateStatus(){
        if(!plcComm.isConnected()){
            controlState = CONTROLSTATE.DISCONNECTED;
            return;
        }
        for(Map.Entry<String, PLC_Resource> entry : OL_Status.getResources()) {
            try {
                OL_Status.updateValue((String) entry.getKey(), plcComm.read_PLC_Resource(entry.getValue()));
            }catch (DisconnectedException de){
                controlState = CONTROLSTATE.DISCONNECTED;
                System.out.println("Can't Update: Disconnected");
                break;
            }
        }
        if(OL_Status.getValue("process_alarm")==1 || OL_Status.getValue("high_level_alarm")==1){
            for (Map.Entry<String, PLC_Resource> entry : OL_Event.getResources()) {
                try {
                    OL_Event.updateValue(entry.getKey(),plcComm.read_PLC_Resource(entry.getValue()));
                }catch (DisconnectedException de){
                    controlState = CONTROLSTATE.DISCONNECTED;
                    System.out.println("Can't Update: Disconnected");
                    break;
                }
            }
            HashMap changed = OL_Event.getActivatedAlarms(OL_lastEvent);
            sqlManager.addNewOnlineEvents(changed);
            OL_lastEvent = OL_Event;
            System.out.println("Number of activated alarms: " + String.valueOf(changed.size()));
        }
        String status = getStatusJson();

        //Check if film advance is started
        if(OL_Status.getValue("film_advance_In_Progress") == 1){
            //Need to reset command
            if(controlState != CONTROLSTATE.FILM_ADVANCE){
               OnlineSystemSetting command = new OnlineSystemSetting();
               command.updateValue("man_film_adv",0);
               uploadSettings(command);
            }
            controlState = CONTROLSTATE.FILM_ADVANCE;
        }
        pub.sendMore("Status");
        pub.send(status);
        printStatus();
        if(controlState == CONTROLSTATE.MEASURING ) {
            updateCounter++;
            if (updateCounter > updateInterval) {
                updateCounter = 0;
            }
        }
    }

    private void updateData(){
        for(Map.Entry<String, PLC_Resource> entry : OL_Data.getResources()) {
            try {
                OL_Data.updateValue((String) entry.getKey(), plcComm.read_PLC_Resource(entry.getValue()));
            }catch (DisconnectedException de){
                controlState = CONTROLSTATE.DISCONNECTED;
                System.out.println("Can't Update: Disconnected");
                break;
            }
        }
        sqlManager.addNewOnlineDataPoint(OL_Data);
        String data = OL_Data.getJSONString();
        pub.sendMore("Data");
        pub.send(data);
        sqlID++;
    }

    private void updateSettings(){
        for(Map.Entry<String, PLC_Resource> entry : OL_Setting.getResources()) {
            try {
                OL_Setting.updateValue((String) entry.getKey(),plcComm.read_PLC_Resource(entry.getValue()));
            }catch (DisconnectedException de){
                controlState = CONTROLSTATE.DISCONNECTED;
                System.out.println("Can't Update: Disconnected");
                break;
            }
        }
        String settings = OL_Setting.getJSONString();
        pub.sendMore("Settings");
        pub.send(settings);
    }

    private void uploadSettings(OnlineSystemSetting newSettings){
        for(Map.Entry<String, PLC_Resource> entry : newSettings.getResources()) {
            if(!plcComm.isConnected()){
                controlState = CONTROLSTATE.DISCONNECTED;
                break;
            }
            if(entry.getValue().getValue() != -1){
                System.out.println(entry.getKey() + " : "+ entry.getValue().getValue());
                plcComm.write_PLC_Resource(entry.getValue());
            }
        }
        updateSettings();
    }

    private void printStatus(){
        switch (controlState){
            case WAITING:
                System.out.println("Flag 17:"+OL_Status.getValue("measurement_In_Progress") + " System Time: " + (OL_Status.getValue("ms_counter")/1000/60));
                break;
            case MEASURING:
                System.out.println("Flag 17:"+OL_Status.getValue("measurement_In_Progress") + " Time Remaining: " + OL_Status.getValue("measure_time_remaining"));
                break;
            case ERROR:
                System.out.println("Error");
        }
    }

    private void doWaiting(){
        updateStatus();
        if(OL_Status.getValue("measurement_In_Progress") == 1){
            controlState = CONTROLSTATE.MEASURING;
        }
        if(OL_Status.getValue("film_advance_In_Progress") == 1){

        }else if(OL_Status.getValue("process_alarm") == 1 || OL_Status.getValue("high_level_alarm")==1){

        }
     }

    private void doMeasuring(){
        updateStatus();
        if(OL_Status.getValue("measurement_In_Progress") == 0){
            controlState = CONTROLSTATE.FINISHED;
        }
        if(OL_Status.getValue("film_advance_In_Progress") == 1){

        }else if(OL_Status.getValue("process_alarm") == 1 || OL_Status.getValue("high_level_alarm")==1){

        }
    }

    private void doFinished(){
        //Get and report Data
        updateSettings();
        updateData();
        updateStatus();
        controlState = CONTROLSTATE.WAITING;
    }


    private void doCustomMeasurement(){
        //Lot of custom stuff needed here
        //define data type and interface
    }

    private void doError(){
        //ResetBoard and go back to waiting
        //How should user see this?
        //Alert box - clear on close ???
        updateStatus();
    }

    private void cleanup(){
        pub.close();
        running = false;
    }

    private String getStatusJson(){
        JSONObject temp = new JSONObject();
        temp.put("controlState",controlState.toString());
        temp.put("onlineStatus",OL_Status.getJSONString());
        return temp.toString();
    }
}
