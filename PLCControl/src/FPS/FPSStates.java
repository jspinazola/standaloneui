package FPS;

import java.util.HashMap;

public enum FPSStates {
    Initialize(0),
    Waiting(1),
    FTIR_Purge(2),
    FTIR_InPosition(3),
    FTIR_Drain(4),
    FTIR_Finished(5),
    Start(6),
    Heat(7),
    Purge(8),
    Measure(9),
    Cooldown(10),
    Drain(11),
    Finished(12),
    tuneHeat(13),
    tuneCool(14),
    reportXducer(15);

    private int stateCode;
    private static HashMap map = new HashMap<>();

    FPSStates(int stateCode){
        this.stateCode = stateCode;
    }

    static {
        for(FPSStates fpsState : FPSStates.values()){
            map.put(fpsState.stateCode,fpsState);
        }
    }

    public static FPSStates valueOf(int value){
        return (FPSStates) map.get(value);
    }

    public int getStateCode(){
        return this.stateCode;
    }

}
