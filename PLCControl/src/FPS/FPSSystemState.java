package FPS;

import FPS.FPSStates;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.fasterxml.jackson.annotation.*;

@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE
)

public class FPSSystemState {

    @JsonProperty
    private FPSStates state;

    @JsonProperty
    private int elapsedTime;
    @JsonProperty
    private int measurementPoint;
    @JsonProperty
    private int adcSensor;
    @JsonProperty
    private int adcHeatx;
    @JsonProperty
    private int fluidTemp;
    @JsonProperty
    private int PWM1;
    @JsonProperty
    private int PWM2;
    @JsonProperty
    private int fanStatus;
    @JsonProperty
    private int tempSetpoint;
    @JsonProperty
    private int corrTempSetpoint;
    @JsonProperty
    private int pumpSpeed;
    @JsonProperty
    private String sampleName;
    @JsonProperty
    private int sampleId;
    @JsonProperty
    private String FWVersion ;
    private String timeCode;

    public String getTimeCode() {
        return timeCode;
    }

    public void setElapsedTime(int time){elapsedTime = time;}

    public void setAdcHeatx(int adcHeatx) {
        this.adcHeatx = adcHeatx;
    }

    public void setTimeCode(String timeCode) {
        this.timeCode = timeCode;
    }
    /*
    @JsonProperty
    private Boolean isFTIRWarm;
    @JsonProperty
    private Long ftirWarmTime;
     */

    public String getSampleName() {
        return sampleName;
    }

    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }

    public int getId() {
        return sampleId;
    }

    public void setId(int id) {
        this.sampleId = id;
    }

    public FPSSystemState(){
        this(FPSStates.Initialize,0,0,0,0,0,0,0,0,0,0,0);
    }

    @JsonCreator
    public FPSSystemState(
            @JsonProperty("state") String state,
            @JsonProperty("elapsedTime") int elapsedTime,
            @JsonProperty("measurementPoint") int measurementPoint,
            @JsonProperty("adcSensor") int adcSensor,
            @JsonProperty("adcHeatx") int adcHeatx,
            @JsonProperty("fluidTemp") int fluidTemp,
            @JsonProperty("PWM1") int PWM1,
            @JsonProperty("PWM2") int PWM2,
            @JsonProperty("fanStatus") int fanStatus,
            @JsonProperty("tempSetpoint") int tempSetpoint,
            @JsonProperty("corrTempSetpoint") int corrTempSetpoint,
            @JsonProperty("pumpSpeed") int pumpSpeed,
            @JsonProperty("FWVersion") String FWVersion
            //@JsonProperty("isFTIRWarm") boolean isFTIRWarm,
            //@JsonProperty("ftirWarmTime") Long ftirWarmTime
    ) {
        this.state = FPSStates.valueOf(state);
        this.elapsedTime = elapsedTime;
        this.measurementPoint = measurementPoint;
        this.adcSensor = adcSensor;
        this.adcHeatx = adcHeatx;
        this.fluidTemp = fluidTemp;
        this.PWM1 = PWM1;
        this.PWM2 = PWM2;
        this.fanStatus = fanStatus;
        this.tempSetpoint = tempSetpoint;
        this.corrTempSetpoint = corrTempSetpoint;
        this.pumpSpeed = pumpSpeed;
        this.FWVersion = FWVersion;
        //this.isFTIRWarm = isFTIRWarm;
        //this.ftirWarmTime = ftirWarmTime;
    }

    public FPSSystemState(
            FPSStates state,
            int elapsedTime,
            int measurementPoint,
            int adcSensor,
            int adcHeatx,
            int fluidTemp,
            int PWM1,
            int PWM2,
            int fanStatus,
            int tempSetpoint,
            int corrTempSetpoint,
            int pumpSpeed
    ) {
        this.state = state;
        this.elapsedTime = elapsedTime;
        this.measurementPoint = measurementPoint;
        this.adcSensor = adcSensor;
        this.adcHeatx = adcHeatx;
        this.fluidTemp = fluidTemp;
        this.PWM1 = PWM1;
        this.PWM2 = PWM2;
        this.fanStatus = fanStatus;
        this.tempSetpoint = tempSetpoint;
        this.corrTempSetpoint = corrTempSetpoint;
        this.pumpSpeed = pumpSpeed;
        FWVersion = "V0.0.0.0";
    }

    public FPSStates getState(){
        return this.state;
}

    public void setState(FPSStates state) {
        this.state = state;
    }

    public void setStringState(String state){
        this.state = FPSStates.valueOf(state);
    }

    public void setSystemState(String[] values){
        for(String value : values){
            String[] split = value.split(":");
            if(split.length == 2){
                updateValueSwitch(split[0],split[1]);
            }
        }
    }

    public int getElapsedTime(){
        return elapsedTime;
    }

    public int getElapsedTimeSeconds(){
        double et = elapsedTime/105;
        return (int) et;
    }

    public int getMeasurementPoint() {
        return measurementPoint;
    }

    public int getAdcSensor() {
        return adcSensor;
    }

    public int getAdcHeatx() {
        return adcHeatx;
    }

    public int getFluidTemp() {
        return fluidTemp;
    }

    public int getPWM1() {
        return PWM1;
    }

    public int getPWM2() {
        return PWM2;
    }

    public int getFanStatus() {
        return fanStatus;
    }

    public int getTempSetpoint() {
        return tempSetpoint;
    }

    public int getCorrTempSetpoint() {
        return corrTempSetpoint;
    }

    public int getPumpSpeed() {
        return pumpSpeed;
    }

    public String getFWVersion(){return FWVersion;}

    public void setFWVersion(String version){FWVersion = version;}

    /*
    public Boolean getFTIRWarm() {
        return isFTIRWarm;
    }

    public void setFTIRWarm(Boolean FTIRWarm) {
        isFTIRWarm = FTIRWarm;
    }


    public Long getFtirWarmTime() {
        return ftirWarmTime;
    }

    public void setFtirWarmTime(Long ftirWarmTime) {
        this.ftirWarmTime = ftirWarmTime;
    }*/

    public JSONObject getSystemState(){
        JSONObject json = new JSONObject();
        json.put("State", state.getStateCode());
        json.put("elapsedTime",elapsedTime);
        json.put("measurementPoint",measurementPoint);
        json.put("adcSensor",adcSensor);
        json.put("adcHeatX",adcHeatx);
        json.put("fluidTemp",fluidTemp);
        json.put("PWM1",PWM1);
        json.put("PWM2",PWM2);
        json.put("fanStatus",fanStatus);
        json.put("tempSetpoint",tempSetpoint);
        json.put("corrTempSetpoint",corrTempSetpoint);
        json.put("pumpSpeed",pumpSpeed);
        json.put("fwVersion",FWVersion);
        //json.put("ftirWarm",isFTIRWarm);
        //json.put("ftirWarmupTime",ftirWarmTime);
        return json;
    }

    private void updateValueSwitch(String key, String Value){
        key = key.toLowerCase();
        try {
            switch (key) {
                case "state": { state = FPSStates.valueOf(Integer.parseInt(Value));}break;
                case "elapsedtime": { elapsedTime = Integer.parseInt(Value); }break;
                case "measurementpoint": { measurementPoint = Integer.parseInt(Value); }break;
                case "adcsensor": { adcSensor = Integer.parseInt(Value); }break;
                case "adcheatx":{adcHeatx = Integer.parseInt(Value);}break;
                case "fluidtemp":{fluidTemp = Integer.parseInt(Value);}break;
                case "pwm1":{PWM1 = Integer.parseInt(Value);}break;
                case "pwm2":{PWM2 = Integer.parseInt(Value);}break;
                case "fanstatus":{fanStatus = Integer.parseInt(Value);}break;
                case "tempsetpoint":{tempSetpoint = Integer.parseInt(Value);}break;
                case "corrtempsetpoint":{corrTempSetpoint = Integer.parseInt(Value);}break;
                case "pumpspeed":{pumpSpeed = Integer.parseInt(Value);}break;
                default:{;}break;
            }
        }
        catch (Exception e){}
    }

    public double convertToC(int adcValue){
        return (double) adcValue/18.072048;
    }

    public String getJsonString(){
        ObjectMapper ob = new ObjectMapper();
        String SystemState = "";
        try{
            SystemState = ob.writeValueAsString(this);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return SystemState;
    }

    public void updateFromJsonString(String json){
        ObjectMapper ob = new ObjectMapper();
        ObjectReader reader = ob.readerForUpdating(this);
        try {
            reader.readValue(json);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

}
