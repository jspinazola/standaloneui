package FPS.FTIR;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.attribute.standard.DocumentName;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

public class FTIRData {
  private static final Logger LOG = LoggerFactory.getLogger(FTIRData.class.getName());
  private double[][] spectrumData;
  private double[][] backgroundData;
  private File file;

  public FTIRData(double[][] spectrumData) {
    this.spectrumData = spectrumData;
  }

  public FTIRData(){this.spectrumData = null;this.backgroundData = null;}

  public void setBackgroundData(double[][] data){
    if(data != null) {
      if (backgroundData == null) {
        if (data.length > 2) {
          double[][] in = {data[2], data[3]};
          this.backgroundData = in;
        } else if (data.length == 2) {
          this.backgroundData = data;
        }
      } else {
        addMoreBackgroundData(data);
      }
    }
  }

  public void setSpectrumData(double[][] data){
    if(spectrumData == null){
      if(data.length > 3){
        double[][] in = {data[2],data[3]};
        this.spectrumData = in;
      }else{
        this.spectrumData = data;
      }
    }else {
      addMoreSpectrumData(data);
    }
  }

  private void addMoreSpectrumData(double[][] data){
        double[][] newData = new double[spectrumData.length+1][];
        for (int i = 0; i < spectrumData.length; i++) {
            newData[i] = spectrumData[i];
        }
        if(data.length > 2){
          newData[spectrumData.length] = data[3];
        }else {
          newData[spectrumData.length] = data[1];
        }
        this.spectrumData = newData;
    }

    private void addMoreBackgroundData(double[][] data){
        double[][] newData = new double[backgroundData.length+1][];
        for (int i = 0; i < backgroundData.length; i++) {
            newData[i] = backgroundData[i];
        }
      if(data.length > 2){
        newData[backgroundData.length] = data[3];
      }else {
        newData[backgroundData.length] = data[1];
      }
        this.backgroundData = newData;
    }


  public void displaySpectrumData() {
    if (this.spectrumData != null) {
      for (int j = 0; j < spectrumData[spectrumData.length - 1][0]; ++j) {
        for (int i = 0; i < 4; ++i) {
          System.out.print(spectrumData[i][j] + ", ");
        }
        System.out.println();
      }
    }
  }

  public void displayInterSpecData(){
    if (this.backgroundData != null) {
      for (int j = 0; j < backgroundData[backgroundData.length - 1][0]; ++j) {
        for (int i = 0; i < 4; ++i) {
          System.out.print(backgroundData[i][j] + ", ");
        }
        System.out.println();
      }
    }
  }

  public void saveSpectrumData(String fileName) {
    int numPoints = 0;
    Double sum = 0.0;
    if (this.spectrumData != null) {
      if (createFile()) {
        try {
          PrintWriter output = new PrintWriter("Data/" + fileName);
          //TODO Why is this being transposed? Why is it duplicated?
          output.println("SpectrumData,");
          for (int j = 0; j < spectrumData[0].length; ++j) {
            for (int i = 0; i < spectrumData.length; ++i) {
              if(i>0){
                numPoints++;
                sum+=spectrumData[i][j];
              }
              output.print(spectrumData[i][j]);
              output.print(',');
            }
            if(numPoints!=0){
              output.print(sum/numPoints);
              output.print(',');
              numPoints=0;
              sum=0.0;
            }
            output.println();
          }
          output.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public String toJSON() {
    return toJSONObject().toString();
  }

  public JSONObject toJSONObject(){
    JSONObject json = new JSONObject();
    if(spectrumData == null) {
      json.put("spectrum", "NA");
    }
    else {
      JSONArray array = new JSONArray();
      if(backgroundData != null) {
          for (int i = 0; i < backgroundData.length; i++) {
              JSONArray row = new JSONArray();
              for (int j = 0; j < backgroundData[i].length; j++) {
                  row.add(backgroundData[i][j]);
              }
              array.add(row);
          }
      }

      for (int i = 0; i < spectrumData.length; i++) {
        JSONArray row = new JSONArray();
        for (int j = 0; j < spectrumData[i].length; j++) {
          row.add(spectrumData[i][j]);
        }
        array.add(row);
      }

      json.put("spectrum", array);
    }

    return json;
  }

  public void parseJSON(String data){
    JSONParser parser = new JSONParser();
    JSONObject json = new JSONObject();
    double[][] dxfer = new double[2][];
    try{
      json = (JSONObject) parser.parse(data);
    }catch (ParseException e){
      System.out.println("Could not parse json");
    }
    try {
      if(json.get("spectrum").equals("NA") == false){
        JSONArray jsonArray = (JSONArray) json.get("spectrum");
        JSONArray rowArray = (JSONArray) jsonArray.get(0);
        if(jsonArray.size()==4) {
          double[] xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[0] = xfer;
          rowArray = (JSONArray) jsonArray.get(1);
          xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[1] = xfer;
          backgroundData = dxfer;
          dxfer = new double[2][];
          rowArray = (JSONArray) jsonArray.get(2);
          xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[0] = xfer;
          rowArray = (JSONArray) jsonArray.get(3);
          xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[1] = xfer;
          spectrumData = dxfer;
        }else if(jsonArray.size()==2){
          dxfer = new double[2][];
          rowArray = (JSONArray) jsonArray.get(0);
          double[] xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[0] = xfer;
          rowArray = (JSONArray) jsonArray.get(1);
          xfer = new double[rowArray.size()];
          for (int i = 0; i < rowArray.size(); i++) {
            xfer[i] = Double.valueOf(rowArray.get(i).toString());
          }
          dxfer[1] = xfer;
          spectrumData = dxfer;
        }
      }
    }catch(Exception e){
      System.out.println(e.getMessage());
      System.out.println(e.getLocalizedMessage());
      System.out.println("Parse Issue -> ");
    }
  }

  public double[][] getSimpleData(){
    return new double[][]{spectrumData[0],spectrumData[spectrumData.length-1]};
  }
  public double[][] getSimpleBackgroundData(){
    return new double[][]{backgroundData[0],backgroundData[backgroundData.length-1]};
  }

  public double[][] getSpectrumData(){return spectrumData;}
  public double[][] getBackgroundData(){return backgroundData;}

  public boolean isNull(){
    if (spectrumData == null){
      return true;
    }else {
      return false;
    }
  }

  private Boolean createFile() {
    file = new File("Data");
    if (file.exists()) {
      return true;
    }
    return file.mkdir();
  }
}
