package FPS.FTIR;

public enum FTIRState {
        UNINIT("uninit"),
        WARMUP("warmup"),
        IDLE("idle"),
        MEASURING_BACKGROUND("measuring_background"),
        MEASURING_BACKGROUND_COMPLETE("measuring_background_complete"),
        MEASURING("measuring"),
        COLLECTING_DATA("collecting_data"),
        MEASUREMENT_COMPLETE("measurement_complete"),
        ERROR("error");

    private String state;

    FTIRState(String name) {
        this.state = name;
    }

    public String getState() {
        return this.state;
    }
}
