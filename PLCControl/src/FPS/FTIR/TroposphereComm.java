package FPS.FTIR;

import Util.SerialPortHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class TroposphereComm extends FTIRComm{

    private final int expectedUpdatesDuringScan = 385;
    private int updatesDuringScan = 0;
    private SerialPortHelper device;
    private FTIRData data = new FTIRData();
    private String incommingData;
    private TimerTask dataTimeout;
    private TimerTask measurementTimeout;
    private Timer timer = new Timer();

    public TroposphereComm(FTIR ftir) {
        super(ftir);
        status = new FTIRStatus();
        status.setFTIRState(FTIRState.UNINIT);
    }

    @Override
    public boolean scanBackground(){
        if(status.getState() == FTIRState.IDLE){
            status.setFTIRState(FTIRState.MEASURING_BACKGROUND_COMPLETE);
            return true;
        }else {
            return false;
        }
    }

    @Override
    //Starts FTIR Scan
    public boolean scan(){
        if(status.getState() == FTIRState.IDLE || status.getState() == FTIRState.MEASURING_BACKGROUND_COMPLETE){
            device.writeString("SCAN");
            String properResponse = "SCAN_BEGIN";
            int attempts = 0;
            String resp = "";
            while (attempts < 15 && !resp.contains(properResponse)){
                String temp = device.readString();
                if(temp != null){
                    resp = resp + temp;
                }
                attempts++;
            }
            System.out.println(resp);
            if(resp.contains(properResponse)){
                status.setFTIRState(FTIRState.MEASURING);
                status.setWarmTime(0L);
                incommingData = "";
                startMeasureTimeout();
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    @Override
    //Connects and initializes FTIR
    public boolean initialize(){
        device = new SerialPortHelper("FTIR_RS485",57600,1000);
        device.writeString("RESET");
        if(device.PORT_STATUS == SerialPortHelper.SERIAL_PORT_STATUS.SERIAL_PORT_READY){
            String properResponse = "READY_TO_SCAN";
            int attempts = 0;
            String resp = "";
            while (attempts < 15 && !resp.contains(properResponse)){
                String temp = device.readString();
                if(temp != null){
                    resp = resp + temp;
                }
                attempts++;
            }
            System.out.println(resp);
            if(resp.contains(properResponse)){
                status.setFTIRState(FTIRState.IDLE);
                status.setWarmTime(0L);
                return true;
            }
        }
        return false;
    }

    @Override
    //Refresh Status of FTIR System
    public boolean updateStatus(){
        if(status.getState() == FTIRState.MEASURING){
            return updateMeasuring();
        }else if(status.getState()==FTIRState.COLLECTING_DATA){
            return updateData();
        }else {
            return true;
        }
    }

    private boolean updateMeasuring(){
        if(device.in_waiting()>0) {
            String newStatus = device.readWaiting().replace("\r", "");
            newStatus = newStatus.replace("\n","");
            updatesDuringScan = updatesDuringScan + (int) newStatus.chars().filter(ch -> ch == '.').count();
            status.setPercentFinished((double)updatesDuringScan/expectedUpdatesDuringScan);
            newStatus.replace(".", "");
            incommingData = incommingData + newStatus;
            if (incommingData.contains("ERROR")) {
                status.setFTIRState(FTIRState.ERROR);
                status.setMessage(newStatus);
                System.out.println(incommingData);
                return false;
            }else if(incommingData.contains("SCAN_END")) {
                timer.cancel();
                timer.purge();
                device.writeString("DATA\r");
                incommingData = "";
                status.setFTIRState(FTIRState.COLLECTING_DATA);
                startDataTimeout();
                return true;
            }
            else {
                return true;
            }
        }
        return true;
    }

    private boolean updateData(){
        if(device.in_waiting()>0) {
            String newStatus = device.readWaiting().replace("\n","");
            System.out.println(newStatus);
            incommingData = incommingData + newStatus;
            if(incommingData.contains("END")){
                parseIncomingData();
                timer.cancel();
                timer.purge();
                status.setFTIRState(FTIRState.MEASUREMENT_COMPLETE);
            }
        }
        return true;
    }

    private void parseIncomingData(){
        String[] dataIn = incommingData.split("\r");
        double[] wavelength = new double[dataIn.length];
        double[] transmittance = new double[dataIn.length];
        int i = 0;
        for (String point:dataIn) {
            System.out.println(point);
            if(point.contains(",")){
                String[] points = point.split(",");
                if(points.length == 3){
                    wavelength[i]=Double.valueOf(points[0]);
                    transmittance[i] = Double.valueOf(points[1]);
                }
            }
            i++;
        }
        data.setSpectrumData(new double[][]{wavelength,transmittance});
        System.out.println(Arrays.deepToString(data.getSpectrumData()));
    }

    @Override
    public FTIRData getData(){
        if(status.getState() == FTIRState.MEASUREMENT_COMPLETE){
            updatesDuringScan = 0;
            status.setPercentFinished(0.0);
            status.setFTIRState(FTIRState.IDLE);
        }
        return data;
    }

    @Override
    //RESET FTIR
    public boolean reset(){
        return true;
    }

    private void startDataTimeout(){
        dataTimeout = new TimerTask() {
            @Override
            public void run() {
                System.out.println("DataTimeout");
                device.writeString("DATA\r");
                incommingData = "";
                status.setFTIRState(FTIRState.COLLECTING_DATA);
            }
        };
        timer = new Timer();
        timer.schedule(dataTimeout,600000L);
    }

    private void startMeasureTimeout(){
        measurementTimeout = new TimerTask() {
            @Override
            public void run() {
                System.out.println("MeasureTimeout");
                device.writeString("DATA\r");
                incommingData = "";
                status.setFTIRState(FTIRState.COLLECTING_DATA);
            }
        };
        timer = new Timer();
        timer.schedule(measurementTimeout,2400000L);
    }

}
