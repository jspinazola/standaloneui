package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MeasurementData {

    private int setpoint0;
    private int setpoint1;
    private int pumpSpeed;
    private int measurementTime;
    private int flushTime;
    private int flushSpeed;
    private int purgeSpeed;
    private int purgeTime;
    private int SETPOINTS;

    public int getPurgeSpeed() {
        return purgeSpeed;
    }

    public void setPurgeSpeed(int purgeSpeed) {
        this.purgeSpeed = purgeSpeed;
    }

    public int getPurgeTime() {
        return purgeTime;
    }

    public void setPurgeTime(int purgeTime) {
        this.purgeTime = purgeTime;
    }

    public int getSETPOINTS() {
        return SETPOINTS;
    }

    public void setSETPOINTS(int SETPOINTS) {
        this.SETPOINTS = SETPOINTS;
    }

    public int getSetpoint0() {
        return setpoint0;
    }

    public void setSetpoint0(int setpoint0) {
        this.setpoint0 = setpoint0;
    }

    public int getSetpoint1() {
        return setpoint1;
    }

    public void setSetpoint1(int setpoint1) {
        this.setpoint1 = setpoint1;
    }

    public int getPumpSpeed() {
        return pumpSpeed;
    }

    public void setPumpSpeed(int pumpSpeed) {
        this.pumpSpeed = pumpSpeed;
    }

    public int getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(int measurementTime) {
        this.measurementTime = measurementTime;
    }

    public int getFlushTime() {
        return flushTime;
    }

    public void setFlushTime(int flushTime) {
        this.flushTime = flushTime;
    }

    public int getFlushSpeed() {
        return flushSpeed;
    }

    public void setFlushSpeed(int flushSpeed) {
        this.flushSpeed = flushSpeed;
    }

    public MeasurementData(){
        this.setpoint0 = 0;
        this.setpoint1 = 0;
        this.measurementTime = 0;
        this.pumpSpeed = 0;
        this.flushSpeed = 0;
        this.flushTime = 0;
        this.purgeSpeed = 0;
        this.purgeTime = 0;
        this.SETPOINTS = 0;
    }

    public MeasurementData(int setpoint0, int setpoint1, int measurementTime, int pumpSpeed, int flushTime, int flushSpeed,int purgeSpeed, int purgeTime, int SETPOINTS){
        this.setpoint0 = setpoint0;
        this.setpoint1 = setpoint1;
        this.measurementTime = measurementTime;
        this.pumpSpeed = pumpSpeed;
        this.flushSpeed = flushSpeed;
        this.flushTime = flushTime;
        this.purgeSpeed = purgeSpeed;
        this.purgeTime = purgeTime;
        this.SETPOINTS = SETPOINTS;
    }

    public MeasurementData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try{
            json = (JSONObject) parser.parse(jsonData);
        }catch (ParseException e){
            System.out.println("Could not parse json");
        }
        for(Object key : json.keySet()){
            try{
                switch (key.toString()){
                    case "setpoint0":{ this.setpoint0 = Integer.valueOf(json.get(key).toString());}break;
                    case "setpoint1":{ this.setpoint1 = Integer.valueOf(json.get(key).toString());}break;
                    case "measurementTime":{ this.measurementTime = Integer.valueOf(json.get(key).toString());}break;
                    case "pumpSpeed":{ this.pumpSpeed = Integer.valueOf(json.get(key).toString());}break;
                    case "flushSpeed":{ this.flushSpeed = Integer.valueOf(json.get(key).toString());}break;
                    case "flushTime":{ this.flushTime = Integer.valueOf(json.get(key).toString());}break;
                    case "purgeSpeed":{ this.purgeSpeed = Integer.valueOf(json.get(key).toString());}break;
                    case "purgeTime":{ this.purgeTime = Integer.valueOf(json.get(key).toString());}break;
                    case "SETPOINTS":{ this.SETPOINTS = Integer.valueOf(json.get(key).toString());}break;
                    default:{System.out.println("Data Json Parse - Did not expect " + key.toString() + " : " + json.get(key).toString());}break;
                }
            }catch (Exception e){
                System.out.println("Parse Issue -> " + key + " : " + json.get(key).toString());
            }
        }
    }

    public JSONObject getJson(){
        JSONObject json = new JSONObject();
        json.put("setpoint0",setpoint0);
        json.put("setpoint1",setpoint1);
        json.put("measurementTime",measurementTime);
        json.put("pumpSpeed",pumpSpeed);
        json.put("flushSpeed",flushSpeed);
        json.put("flushTime",flushTime);
        json.put("purgeSpeed",purgeSpeed);
        json.put("purgeTime",purgeTime);
        json.put("SETPOINTS",SETPOINTS);
        return json;
    }

}
