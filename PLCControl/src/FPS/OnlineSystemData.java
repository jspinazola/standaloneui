package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OnlineSystemData {
    private HashMap resources = new HashMap<String, PLC_Resource>();
    private int sqlid;
    private String timestamp = " ";

    public OnlineSystemData(){
        resources.put("software_version", new PLC_Resource(599,ResourceTypes.REGISTER));
        resources.put("counting_time", new PLC_Resource(513,ResourceTypes.REGISTER));
        resources.put("cal_background_factor", new PLC_Resource(514,ResourceTypes.REGISTER,.01));
        resources.put("cal_slope", new PLC_Resource(515,ResourceTypes.REGISTER,.01));
        resources.put("cal_time", new PLC_Resource(562,ResourceTypes.REGISTER));
        resources.put("val_high", new PLC_Resource(518,ResourceTypes.REGISTER));
        resources.put("val_low", new PLC_Resource(519,ResourceTypes.REGISTER));
        resources.put("film_adv_int", new PLC_Resource(578,ResourceTypes.REGISTER));
        resources.put("counting_time_trend", new PLC_Resource(589,ResourceTypes.REGISTER));
        resources.put("drift_correction_interval", new PLC_Resource(646,ResourceTypes.REGISTER));
        resources.put("stream_switching", new PLC_Resource(598,ResourceTypes.REGISTER));
        resources.put("drift_corr_active", new PLC_Resource(608,ResourceTypes.REGISTER));
        resources.put("enable_film_adv", new PLC_Resource(610,ResourceTypes.REGISTER));
        resources.put("default_stream", new PLC_Resource(785,ResourceTypes.REGISTER));
        resources.put("last_counts", new PLC_Resource(248,ResourceTypes.REGISTER));
        resources.put("pressure_bleed_time", new PLC_Resource(649,ResourceTypes.REGISTER));
        resources.put("kv_high_counts", new PLC_Resource(651,ResourceTypes.REGISTER));
        resources.put("kv_low_counts", new PLC_Resource(652,ResourceTypes.REGISTER));
        resources.put("beam_high_counts", new PLC_Resource(653,ResourceTypes.REGISTER));
        resources.put("beam_low_counts", new PLC_Resource(654,ResourceTypes.REGISTER));
        resources.put("kv_ctl", new PLC_Resource(501,ResourceTypes.REGISTER,.001));
        resources.put("ma_ctl", new PLC_Resource(504,ResourceTypes.REGISTER,.0002));
        resources.put("kV_det_ctl", new PLC_Resource(507,ResourceTypes.REGISTER,.001));
        resources.put("ppm_process", new PLC_Resource(247,ResourceTypes.REGISTER,.01));
        resources.put("ppm_drift_corr", new PLC_Resource(641,ResourceTypes.REGISTER,.01));
        //resources.put("ppm_trend", new PLC_Resource(591,ResourceTypes.REGISTER,.01)); //Moved to Status
        resources.put("ppm_val", new PLC_Resource(17,ResourceTypes.REGISTER,.01));
        resources.put("val_failed", new PLC_Resource(292,ResourceTypes.REGISTER));
        resources.put("drift_corr_failed", new PLC_Resource(294,ResourceTypes.REGISTER));
        resources.put("last_val_ppm", new PLC_Resource(770,ResourceTypes.REGISTER,.01));
        resources.put("adv_retry_delay", new PLC_Resource(619,ResourceTypes.REGISTER));
        resources.put("val_interval", new PLC_Resource(774,ResourceTypes.REGISTER));
        resources.put("ppm_range", new PLC_Resource(801,ResourceTypes.REGISTER)); //Max High Range
        resources.put("dwm_flush_time", new PLC_Resource(808,ResourceTypes.REGISTER));
        resources.put("stream_b_selected", new PLC_Resource(1014,ResourceTypes.REGISTER));
        resources.put("ppm_low_scale", new PLC_Resource(801,ResourceTypes.REGISTER));
        resources.put("ppm_high_scale", new PLC_Resource(809,ResourceTypes.REGISTER));
        resources.put("val_repeats", new PLC_Resource(787,ResourceTypes.REGISTER));
        resources.put("drift_corr_repeats", new PLC_Resource(788,ResourceTypes.REGISTER));
        resources.put("analyzer_low_flow", new PLC_Resource(558,ResourceTypes.REGISTER,.01));
        resources.put("fast_loop_low_flow", new PLC_Resource(557,ResourceTypes.REGISTER,.01));
        sqlid = -1;
    }

    public int getSqlid(){
        return sqlid;
    }

    public void setSqlid(int id){
        sqlid = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue();
        }catch (Exception e){
            return -1;
        }
    }

    public double getConvertedValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue()*updated.getConversion_factor()+updated.getOffset();
        }catch (Exception e){
            return -1.0;
        }

    }

    public void updateValue(String key, int value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            updated.setValue(value);
            resources.put(key, updated);
        }
        catch (Exception e) {
            System.out.println("Data: Could not update Value: "+key );
        }
    }

    public Set<Map.Entry<String,PLC_Resource>> getResources() {
        return resources.entrySet();
    }

    public String getJSONString(){
        JSONObject json = new JSONObject();
        resources.forEach((k, v) -> {
            PLC_Resource temp = (PLC_Resource) v;
            int value = temp.getValue();
            json.put(k, String.valueOf(value));
        });
        return json.toJSONString();
    }

    public void parseJSONData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
            for(Object key : json.keySet()) {
                try {
                    updateValue(key.toString(),Integer.parseInt(json.get(key).toString()));
                }catch (Exception e){
                    System.out.println("Could not get value: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not parse json: " + e.getMessage());
        }
    }
}
