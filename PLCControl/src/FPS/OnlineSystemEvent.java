package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OnlineSystemEvent {
    private HashMap<String, PLC_Resource> resources = new HashMap<String, PLC_Resource>();

    public OnlineSystemEvent(){
        resources.put("door_dwm_interlock",new PLC_Resource(150,ResourceTypes.REGISTER));
        resources.put("fuel_pressure_high",new PLC_Resource(151,ResourceTypes.REGISTER));
        resources.put("fuel_pressure_low",new PLC_Resource(152,ResourceTypes.REGISTER));
        resources.put("beam_voltage_low",new PLC_Resource(157,ResourceTypes.REGISTER));
        resources.put("beam_voltage_high",new PLC_Resource(158,ResourceTypes.REGISTER));
        resources.put("detector_voltage_high",new PLC_Resource(159,ResourceTypes.REGISTER));
        resources.put("detector_voltage_low",new PLC_Resource(160,ResourceTypes.REGISTER));
        resources.put("engine_vacuum_poor",new PLC_Resource(165,ResourceTypes.REGISTER));
        resources.put("air_temp_high",new PLC_Resource(167,ResourceTypes.REGISTER));
        resources.put("air_temp_low",new PLC_Resource(168,ResourceTypes.REGISTER));
        resources.put("beam_temp_high",new PLC_Resource(169,ResourceTypes.REGISTER));
        resources.put("beam_temp_low",new PLC_Resource(170,ResourceTypes.REGISTER));
        resources.put("shutter_malfunction",new PLC_Resource(171,ResourceTypes.REGISTER));
        resources.put("shutter_did_not_close",new PLC_Resource(172,ResourceTypes.REGISTER));
        resources.put("shutter_did_not_open",new PLC_Resource(173,ResourceTypes.REGISTER));
        resources.put("leak",new PLC_Resource(175,ResourceTypes.REGISTER));
        resources.put("low_n2",new PLC_Resource(176,ResourceTypes.REGISTER));
        resources.put("low_air_press",new PLC_Resource(177,ResourceTypes.REGISTER));
        resources.put("enclosure_purge_fault",new PLC_Resource(178,ResourceTypes.REGISTER));
        resources.put("low_film",new PLC_Resource(179,ResourceTypes.REGISTER));
        resources.put("out_of_film",new PLC_Resource(180,ResourceTypes.REGISTER));
        resources.put("tank_high",new PLC_Resource(181,ResourceTypes.REGISTER));
        resources.put("film_advance_fault",new PLC_Resource(197,ResourceTypes.REGISTER));
        for (String key: resources.keySet()) {
            resources.get(key).setValue(0);
        }
    }

    public int getValue(String key){
        PLC_Resource updated = (PLC_Resource) resources.get(key);
        return updated.getValue();
    }

    public void updateValue(String key, int value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            updated.setValue(value);
            resources.put(key, updated);
        }
        catch (Exception e) {
            System.out.println("Event: Could not update Value: "+key );
        }
    }

    public Set<Map.Entry<String,PLC_Resource>> getResources() {
        return resources.entrySet();
    }

    public HashMap<String, PLC_Resource> getActivatedAlarms(OnlineSystemEvent old) {
        HashMap changed = new HashMap<String, PLC_Resource>();
        for(Map.Entry<String, PLC_Resource> entry : getResources()) {
            if(entry.getValue().getValue() != old.getValue(entry.getKey())){
                changed.put(entry.getKey(),entry.getValue());
            }
        }
        return changed;
    }

    public String getJSONString(){
        JSONObject json = new JSONObject();
        resources.forEach((k, v) -> {
            PLC_Resource temp = (PLC_Resource) v;
            int value = temp.getValue();
            json.put(k, String.valueOf(value));
        });
        return json.toJSONString();
    }

    public void parseJSONData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
            for(Object key : json.keySet()) {
                try {
                    updateValue(key.toString(),Integer.parseInt(json.get(key).toString()));
                }catch (Exception e){
                    System.out.println("Could not get value: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not parse json: " + e.getMessage());
        }
    }
}
