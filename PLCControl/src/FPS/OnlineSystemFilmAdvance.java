package FPS;

import FPS.PLC_Resource;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OnlineSystemFilmAdvance {
    private HashMap resources = new HashMap<String, PLC_Resource>();

    public OnlineSystemFilmAdvance(){
        resources.put("FILMADVANC_INDEXCOUNT",new PLC_Resource(569,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_FILMCOUNT",new PLC_Resource(3,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_DRAINOPENED_X",new PLC_Resource(631,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_INLETOPENED_X",new PLC_Resource(632,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_DRIFTCORR_X",new PLC_Resource(634,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_FILMPRESTR_X",new PLC_Resource(607,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_FILMRESEAL_X",new PLC_Resource(630,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_FILMADVANC_X",new PLC_Resource(629,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_SEALRELEASED_X",new PLC_Resource(628,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_DRAINCLOSED_X",new PLC_Resource(627,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_DWELL_X",new PLC_Resource(626,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_N2CLOSED_X",new PLC_Resource(625,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_N2OPEN_X",new PLC_Resource(624,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_INLETCLOSED_X",new PLC_Resource(623,ResourceTypes.REGISTER));
        resources.put("FILMADVANC_ALARMS",new PLC_Resource(321,ResourceTypes.REGISTER));
    }

    public int getValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue();
        }catch (Exception e){
            return -1;
        }
    }

    public double getConvertedValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue()*updated.getConversion_factor()+updated.getOffset();
        }catch (Exception e){
            return -1.0;
        }

    }

    public double getConversionFactor(String key){
        PLC_Resource updated = (PLC_Resource) resources.get(key);
        return updated.getConversion_factor();
    }

    public double getOffset(String key){
        PLC_Resource updated = (PLC_Resource) resources.get(key);
        return updated.getOffset();
    }

    public void updateValue(String key, int value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            updated.setValue(value);
            resources.put(key, updated);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Status: Could not update Value: "+key );
        }
    }

    public Set<Map.Entry<String,PLC_Resource>> getResources() {
        return resources.entrySet();
    }

    public String getJSONString(){
        JSONObject json = new JSONObject();
        resources.forEach((k, v) -> {
            PLC_Resource temp = (PLC_Resource) v;
            int value = temp.getValue();
            json.put(k, String.valueOf(value));
        });
        return json.toJSONString();
    }

    public void parseJSONData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
            for(Object key : json.keySet()) {
                try {
                    updateValue(key.toString(),Integer.parseInt(json.get(key).toString()));
                }catch (Exception e){
                    System.out.println("Could not get value: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not parse json: " + e.getMessage());
        }
    }

}
