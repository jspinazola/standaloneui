package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OnlineSystemSetting {
    private HashMap resources = new HashMap<String, PLC_Resource>();
    private int sqlid;
    private String timestamp = " ";

    public OnlineSystemSetting(){
        resources.put("enable_low_film_alert", new PLC_Resource(769,ResourceTypes.REGISTER));
        resources.put("enable_autorestart", new PLC_Resource(596,ResourceTypes.REGISTER));
        resources.put("enable_driftcorradj", new PLC_Resource(608,ResourceTypes.REGISTER));
        resources.put("enable_sivs", new PLC_Resource(571,ResourceTypes.REGISTER));
        resources.put("run_manual_cal", new PLC_Resource(673,ResourceTypes.REGISTER));
        resources.put("shutdown", new PLC_Resource(235,ResourceTypes.REGISTER));
        resources.put("val_repeats", new PLC_Resource(787,ResourceTypes.REGISTER));
        resources.put("drift_corr_repeats", new PLC_Resource(788,ResourceTypes.REGISTER));
        resources.put("dwm_flush_time", new PLC_Resource(808,ResourceTypes.REGISTER));
        resources.put("analyzer_low_flow", new PLC_Resource(558,ResourceTypes.REGISTER,.01));
        resources.put("fast_loop_low_flow", new PLC_Resource(557,ResourceTypes.REGISTER,.01));
        resources.put("xray_beam_hrs", new PLC_Resource(670,ResourceTypes.REGISTER));
        resources.put("vac_pump_hrs", new PLC_Resource(671,ResourceTypes.REGISTER));
        resources.put("stream_switching", new PLC_Resource(598,ResourceTypes.REGISTER));
        resources.put("default_stream", new PLC_Resource(785,ResourceTypes.REGISTER));
        resources.put("val_interval", new PLC_Resource(774,ResourceTypes.REGISTER));
        resources.put("film_adv_int", new PLC_Resource(578,ResourceTypes.REGISTER));
        resources.put("drift_correction_interval", new PLC_Resource(646,ResourceTypes.REGISTER));
        resources.put("enable_film_adv", new PLC_Resource(610,ResourceTypes.REGISTER));
        resources.put("man_film_adv", new PLC_Resource(267,ResourceTypes.REGISTER)); //Check 267
        resources.put("man_drift_corr", new PLC_Resource(268,ResourceTypes.REGISTER)); //Check 268
        resources.put("man_val", new PLC_Resource(277,ResourceTypes.REGISTER));
        resources.put("start", new PLC_Resource(231,ResourceTypes.REGISTER));
        resources.put("ppm_low_scale", new PLC_Resource(801,ResourceTypes.REGISTER));
        resources.put("ppm_high_scale", new PLC_Resource(809,ResourceTypes.REGISTER));
        resources.put("ppm_high_scale", new PLC_Resource(809,ResourceTypes.REGISTER));
        resources.put("counting_time", new PLC_Resource(513,ResourceTypes.REGISTER));
        sqlid = -1;
    }

    public int getSqlid(){
        return sqlid;
    }

    public void setSqlid(int id){
        sqlid = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue();
        }catch (Exception e){
            return -1;
        }
    }

    public boolean getBoolean(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            if(updated.getValue() == 1){
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    public double getConvertedValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue()*updated.getConversion_factor()+updated.getOffset();
        }catch (Exception e){
            return -1.0;
        }

    }

    public void updateValue(String key, int value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            updated.setValue(value);
            resources.put(key, updated);
        }
        catch (Exception e) {
            //System.out.println("Settings: Could not update Value: "+key );
        }
    }

    public void updateValue(String key, String value){
        if(value != "") {
            try {
                PLC_Resource updated = (PLC_Resource) resources.get(key);
                updated.setValue(Integer.parseInt(value));
                resources.put(key, updated);
            } catch (Exception e) {
                //System.out.println("Settings: Could not update Value: "+key );
            }
        }
    }

    public void updateBooleanValue(String key, boolean value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            if(value){
                updated.setValue(1);
            }else {
                updated.setValue(0);
            }
            resources.put(key, updated);
        }
        catch (Exception e) {
            //System.out.println("Settings: Could not update Value: "+key );
        }
    }

    public Set<Map.Entry<String,PLC_Resource>> getResources() {
        return resources.entrySet();
    }

    public String getJSONString(){
        JSONObject json = new JSONObject();
        resources.forEach((k, v) -> {
            PLC_Resource temp = (PLC_Resource) v;
            int value = temp.getValue();
            json.put(k, String.valueOf(value));
        });
        return json.toJSONString();
    }

    public void parseJSONData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
            for(Object key : json.keySet()) {
                try {
                    updateValue(key.toString(),Integer.parseInt(json.get(key).toString()));
                }catch (Exception e){
                    System.out.println("Could not get value: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not parse json: " + e.getMessage());
        }
    }

}
