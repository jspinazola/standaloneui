package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OnlineSystemStatus {

    private PLC_States state;
    private HashMap resources = new HashMap<String, PLC_Resource>();

    public OnlineSystemStatus(){
        state = PLC_States.UNINIT;
        resources.put("kV_mon", new PLC_Resource(200,ResourceTypes.REGISTER,.001));
        resources.put("mA_mon",new PLC_Resource(201,ResourceTypes.REGISTER,.001));
        resources.put("det_kV_Mon",new PLC_Resource(202,ResourceTypes.REGISTER,.001));
        resources.put("chamber_Vac_mon",new PLC_Resource(206,ResourceTypes.REGISTER,.1));
        resources.put("source_temp",new PLC_Resource(207,ResourceTypes.REGISTER));
        resources.put("box_air_temp",new PLC_Resource(208,ResourceTypes.REGISTER));
        resources.put("fil_current",new PLC_Resource(8507,ResourceTypes.REGISTER));
        resources.put("leak_mon",new PLC_Resource(10,ResourceTypes.ANALOG_INPUT));
        resources.put("hv_enable",new PLC_Resource(1001,ResourceTypes.REGISTER));
        resources.put("shutter_ctl",new PLC_Resource(1002,ResourceTypes.REGISTER));
        resources.put("v_inlet_ctl",new PLC_Resource(1004,ResourceTypes.REGISTER));
        resources.put("v_drain_ctl",new PLC_Resource(1005,ResourceTypes.REGISTER));
        resources.put("air_motor_ctl",new PLC_Resource(1006,ResourceTypes.REGISTER));
        resources.put("v_n2_ctl",new PLC_Resource(1008,ResourceTypes.REGISTER));
        resources.put("cal_ctl",new PLC_Resource(1009,ResourceTypes.REGISTER));
        resources.put("v_drain2_ctl",new PLC_Resource(1010,ResourceTypes.REGISTER));
        resources.put("pancake_cyl_ctl",new PLC_Resource(1011,ResourceTypes.REGISTER));
        resources.put("val_ctl",new PLC_Resource(1013,ResourceTypes.REGISTER));
        resources.put("watchdog",new PLC_Resource(1016,ResourceTypes.REGISTER));
        resources.put("shutter_Open",new PLC_Resource(2001,ResourceTypes.REGISTER));
        resources.put("shutter_Closed",new PLC_Resource(2003,ResourceTypes.REGISTER));
        resources.put("door_Open",new PLC_Resource(2005,ResourceTypes.REGISTER));
        resources.put("door_Closed",new PLC_Resource(2007,ResourceTypes.REGISTER));
        resources.put("film_Advance_Sensor",new PLC_Resource(2011,ResourceTypes.REGISTER));
        resources.put("ms_counter",new PLC_Resource(13002,ResourceTypes.REGISTER));
        resources.put("low_N2",new PLC_Resource(291,ResourceTypes.REGISTER));
        resources.put("film_adv_cnt",new PLC_Resource(569,ResourceTypes.REGISTER));
        resources.put("xray_beam_hrs",new PLC_Resource(670,ResourceTypes.REGISTER));
        resources.put("vac_pump_hrs",new PLC_Resource(671,ResourceTypes.REGISTER));
        resources.put("fuel_press_mon",new PLC_Resource(240,ResourceTypes.REGISTER,.01));
        resources.put("purge_mon",new PLC_Resource(2006,ResourceTypes.REGISTER));
        resources.put("measure_time_remaining",new PLC_Resource(246,ResourceTypes.REGISTER));
        resources.put("counts",new PLC_Resource(60,ResourceTypes.REGISTER));
        resources.put("measurement_In_Progress",new PLC_Resource(17,ResourceTypes.FLAG));
        resources.put("film_advance_In_Progress",new PLC_Resource(18,ResourceTypes.FLAG));
        resources.put("calibration_In_Progress",new PLC_Resource(4,ResourceTypes.FLAG));
        resources.put("validation_In_Progress",new PLC_Resource(18,ResourceTypes.FLAG));
        resources.put("process_alarm",new PLC_Resource(13,ResourceTypes.FLAG));
        resources.put("high_level_alarm",new PLC_Resource(7,ResourceTypes.FLAG));
        resources.put("low_air_press_input", new PLC_Resource(2012,ResourceTypes.REGISTER));
        resources.put("recovery_tank_high", new PLC_Resource(2013,ResourceTypes.REGISTER));
        resources.put("ppm_trend", new PLC_Resource(591,ResourceTypes.REGISTER,.01));
    }

    public void setState(PLC_States state) {
        this.state = state;
    }

    public int getValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue();
        }catch (Exception e){
            return -1;
        }
    }

    public double getConvertedValue(String key){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            return updated.getValue()*updated.getConversion_factor()+updated.getOffset();
        }catch (Exception e){
            return -1.0;
        }

    }

    public double getConversionFactor(String key){
        PLC_Resource updated = (PLC_Resource) resources.get(key);
        return updated.getConversion_factor();
    }

    public double getOffset(String key){
        PLC_Resource updated = (PLC_Resource) resources.get(key);
        return updated.getOffset();
    }

    public void updateValue(String key, int value){
        try {
            PLC_Resource updated = (PLC_Resource) resources.get(key);
            updated.setValue(value);
            resources.put(key, updated);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Status: Could not update Value: "+key );
        }
    }

    public Set<Map.Entry<String,PLC_Resource>> getResources() {
        return resources.entrySet();
    }

    public String getJSONString(){
        JSONObject json = new JSONObject();
        resources.forEach((k, v) -> {
            PLC_Resource temp = (PLC_Resource) v;
            int value = temp.getValue();
            json.put(k, String.valueOf(value));
        });
        return json.toJSONString();
    }

    public void parseJSONData(String jsonData) {
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
            for(Object key : json.keySet()) {
                try {
                    updateValue(key.toString(),Integer.parseInt(json.get(key).toString()));
                }catch (Exception e){
                    System.out.println("Could not get value: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Could not parse json: " + e.getMessage());
        }
    }
}
