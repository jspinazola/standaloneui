package FPS;

public class PLC_Resource {
    private int number;
    private ResourceTypes type;
    private int value;
    private double conversion_factor;

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    private double offset;

    public PLC_Resource(){
        this.number = 0;
        this.type = ResourceTypes.NA;
        this.value = -1;
        this.conversion_factor = 1;
        this.offset = 0;
    }

    public PLC_Resource(int number, ResourceTypes type) {
        this.number = number;
        this.type = type;
        this.value = -1;
        this.conversion_factor = 1;
        this.offset = 0;
    }

    public PLC_Resource(int number, ResourceTypes type,double conversion_factor) {
        this.number = number;
        this.type = type;
        this.value = -1;
        this.conversion_factor = conversion_factor;
        this.offset = 0;
    }

    public PLC_Resource(int number, ResourceTypes type,double conversion_factor,double offset ) {
        this.number = number;
        this.type = type;
        this.value = -1;
        this.conversion_factor = conversion_factor;
        this.offset = offset;
    }

    public double getConversion_factor() {
        return conversion_factor;
    }

    public void setConversion_factor(double conversion_factor) {
        this.conversion_factor = conversion_factor;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ResourceTypes getTypes() {
        return type;
    }

    public void setTypes(ResourceTypes types) {
        this.type = types;
    }
}
