package FPS;

public enum PLC_States {
    UNINIT,
    INITIALIZE,
    RAMPING,
    WAITING,
    MEASURE,
    CALIBRATION,
    FILM_ADVANCE,
    VALIDATION,
    ERROR,
}
