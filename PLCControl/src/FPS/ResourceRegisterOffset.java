package FPS;

import FPS.ResourceTypes;

public class ResourceRegisterOffset {
    public int getOffset(ResourceTypes type){
        switch (type){
            case FLAG: return 13200;
            case REGISTER: return 0;
            case ANALOG_INPUT: return 8500;
            case ANALOG_OUTPUT: return 8000;
            case DIGITAL_INPUT: return 2000;
            case DIGITAL_OUTPUT: return 1000;
            default: return 0;
        }
    }
}
