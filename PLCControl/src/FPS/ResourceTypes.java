package FPS;

public enum ResourceTypes {
    REGISTER,
    ANALOG_INPUT,
    ANALOG_OUTPUT,
    DIGITAL_INPUT,
    DIGITAL_OUTPUT,
    FLAG,
    NA,
}
