package FPS;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ScanInfo {

    private String name;
    private boolean simpleMeasurement;
    private MeasurementData measurementData;
    private boolean doFTIR = false;
    private boolean doFPS = false;
    private int FTIRtime = 2;
    private int repeats = 20;
    private String FWVersion;


    public ScanInfo(){
        name = "null";
        simpleMeasurement = true;
        measurementData = new MeasurementData();
        FWVersion = "V0.0.0.0";
    }

    public ScanInfo(String scan, boolean simple){
        name = scan;
        simpleMeasurement = simple;
        measurementData = new MeasurementData();
        FWVersion = "V0.0.0.0";
    }

    public ScanInfo(String scan, boolean simple, MeasurementData measurementData){
        this.name = scan;
        this.simpleMeasurement=simple;
        this.measurementData = measurementData;
        FWVersion = "V0.0.0.0";
    }

    public ScanInfo(String scan, boolean simple, MeasurementData measurementData, boolean doFPS, boolean doFTIR, int FTIRtime){
        this.name = scan;
        this.simpleMeasurement=simple;
        this.measurementData = measurementData;
        this.doFPS = doFPS;
        this.doFTIR = doFTIR;
        this.FTIRtime = FTIRtime;
        FWVersion = "V0.0.0.0";
    }

    public String getName() {
        return name;
    }
    public Boolean getSimpleMeasurement(){
        return simpleMeasurement;
    }
    public Boolean getDoFPS(){return doFPS;}
    public void setDoFPS(boolean run){doFPS = run;}
    public Boolean getDoFTIR(){return doFTIR;}
    public void setDoFTIR(boolean run){doFTIR=run;}
    public void setFTIRtime(int time){this.FTIRtime = time;}
    public int getFTIRtime(){return this.FTIRtime;}
    public int getRepeats(){return repeats;}
    public void setRepeats(int repeats){this.repeats = repeats;}
    public String getFWVersion(){return FWVersion;}
    public void setFWVersion(String fw){this.FWVersion = fw;}

    public MeasurementData getMeasurementData(){return measurementData;}

    public void setMeasurementData(MeasurementData data){this.measurementData = data;}

    public void setScanInfo(String jsonData){
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(jsonData);
        }
        catch (ParseException e){
            System.out.println("Could not parse Scaninfo JSON");
        }
        for(Object key : json.keySet()){
            try{
                switch (key.toString()){
                    case "name":{name = json.get(key).toString();}break;
                    case "simpleMeasurement":{simpleMeasurement = (Boolean) json.get(key);}break;
                    case "measurementData":{measurementData = new MeasurementData(json.get(key).toString());}break;
                    case "ftir": {doFTIR = (Boolean) json.get(key);} break;
                    case "fps" :{doFPS = (Boolean) json.get(key);} break;
                    case "ftirTime" : { FTIRtime = Integer.parseInt(json.get(key).toString());} break;
                    case "fwVersion":{FWVersion = json.get(key).toString();System.out.println("setScanInfo "+ FWVersion);}break;
                    case "repeats":{repeats = Integer.parseInt(json.get(key).toString());}break;
                    default:{System.out.println("ScanInfo Json Parse - Did not expect " + key.toString() + " : " + json.get(key).toString());}break;
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
                System.out.println("Parse Issue -> " + key + " : " + json.get(key).toString());
            }
        }
    }

    public JSONObject getScanInfoJSON(){
        JSONObject json = new JSONObject();
        json.put("name",name);
        json.put("simpleMeasurement",simpleMeasurement);
        json.put("measurementData",measurementData.getJson());
        json.put("ftir",doFTIR);
        json.put("fps",doFPS);
        json.put("ftirTime",FTIRtime);
        json.put("fwVersion",FWVersion);
        json.put("repeats",repeats);
        return json;
    }
}
