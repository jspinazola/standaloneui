package Util;

public class DisconnectedException extends Exception{
    public DisconnectedException(String errorMessage){
        super(errorMessage);
    }
}
