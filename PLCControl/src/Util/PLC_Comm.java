package Util;

import FPS.PLC_Resource;
import FPS.ResourceRegisterOffset;
import net.wimpi.modbus.msg.*;
import net.wimpi.modbus.io.*;
import net.wimpi.modbus.net.*;
import net.wimpi.modbus.procimg.*;
import net.wimpi.modbus.facade.ModbusTCPMaster;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class PLC_Comm {

    private static TCPMasterConnection con = null; //the connection
    private static ModbusTCPTransaction trans = null; //the transaction
    private static ReadMultipleRegistersRequest readReq;
    private static ReadMultipleRegistersResponse res;
    private static WriteMultipleRegistersRequest writeReq;
    private InetAddress addr = null;
    private int port = 0;
    private boolean isConnected = false;
    private ResourceRegisterOffset offset = new ResourceRegisterOffset();

    public boolean Connect(String ipAddress, int port) {
        try {
            InetAddress addr = InetAddress.getByName(ipAddress);
            this.addr = addr;
            con = new TCPMasterConnection(addr);
            con.setPort(port);
            con.setTimeout(100);
            con.connect();
            isConnected = con.isConnected();
            return isConnected;
        } catch (Exception ex){
            isConnected = false;
            return false;
        }
    }

    public boolean changeConnection(String ipAddress, int port){
        if(isConnected){
            close();
        }
        try {
            InetAddress addr = InetAddress.getByName(ipAddress);
            con = new TCPMasterConnection(addr);
            con.setPort(port);
            con.connect();
            trans = new ModbusTCPTransaction(con);
            trans.setReconnecting(false);
            trans.setRetries(0);
            isConnected = true;
            return true;
        } catch (Exception ex){
            isConnected = false;
            return false;
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void close(){
        isConnected = false;
        con.close();
    }

    public int ReadRegister(int register) throws DisconnectedException {
        register = (register-1) * 2;
        if (isConnected == false){
            return -1;
        }
        try {
            readReq = new ReadMultipleRegistersRequest(register, 4);
            trans = new ModbusTCPTransaction(con);
            trans.setRetries(0);
            trans.setReconnecting(false);
            trans.setRequest(readReq);
            trans.execute();
            res = (ReadMultipleRegistersResponse) trans.getResponse();
            int[] info = new int[res.getByteCount()];
            int counter = 0;
            for (counter = 0; counter < res.getByteCount()/2; counter++) {
                info[counter] = res.getRegisterValue(counter);
            }
            int value = info[1]+(info[0]<<16);
            return value;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
            isConnected = false;
            throw new DisconnectedException("Disconnected : "+ String.valueOf(register));
        }
    }

    public void WriteRegister(int register, int value) {
        register = (register-1) * 2;
        try {
            writeReq = new WriteMultipleRegistersRequest();
            writeReq.setReference(register);  //register number
            byte[] mybytes = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(value).array();
            System.out.println(value);
            System.out.println(mybytes[0] + " : " + mybytes[1] + " : " + mybytes[2] + " : " + mybytes[3]);
            int lowint = (mybytes[2] << 8) + mybytes[3];
            int highint = (mybytes[0] << 8) + mybytes[1];
            System.out.println(highint + " : " + lowint);
            int[] values = new int[]{highint, lowint};
            Register[] registers = new Register[2];
            registers[0] = new SimpleRegister(highint);
            registers[1] = new SimpleRegister(lowint);
            writeReq.setRegisters(registers);
            //4. Prepare the transaction
            trans = new ModbusTCPTransaction(con);
            trans.setRequest(writeReq);
            trans.execute();
        } catch (Exception ex) {
            isConnected = false;
        }
    }

    public int read_PLC_Resource(PLC_Resource resource) throws DisconnectedException{
        return ReadRegister(resource.getNumber()+offset.getOffset(resource.getTypes()));
    }

    public void write_PLC_Resource(PLC_Resource resource){
        System.out.println(resource.getValue());
        WriteRegister(resource.getNumber()+offset.getOffset(resource.getTypes()),resource.getValue());
    }

    //Depreciated
    public int[] Read(String[] request) {
        try {
            int ref = Integer.parseInt(request[1]);
            int count = Integer.parseInt(request[2]);

            readReq = new ReadMultipleRegistersRequest(ref, count);
            //4. Prepare the transaction
            trans = new ModbusTCPTransaction(con);
            trans.setRequest(readReq);
            //5. Execute the transaction repeat times
            trans.execute();
            res = (ReadMultipleRegistersResponse) trans.getResponse();
            //6. Close the connection
            con.close();
            int[] info = new int[res.getByteCount()];
            int counter = 0;
            for (counter = 0; counter < res.getByteCount()/2; counter++) {
                info[counter] = res.getRegisterValue(counter);
                //System.out.println(info[counter]);
            }
            //System.out.println("Read");
            //System.out.println(info[0]);
            return info;
        } catch (Exception ex) {
            ex.printStackTrace();
            int[] fail = new int [1];
            fail[0] = -1;
            return fail;
        }
    }

    //Depreciated
    public void Write(String[] request) {
        try {
            /* The important instances of the classes mentioned before */
            TCPMasterConnection con = null; //the connection
            ModbusTCPTransaction trans = null; //the transaction
            WriteSingleRegisterRequest WriteReq = null;
            WriteSingleRegisterResponse WriteRes = null;
            SimpleRegister MyReg = new SimpleRegister();
            //ReadInputDiscretesRequest req = null; //the request
            //ReadInputDiscretesResponse res = null; //the response

            /* Variables for storing the parameters */
            String astr = request[0];
            int idx = astr.indexOf(':');
            int port = 502;
            port = Integer.parseInt(astr.substring(idx + 1));
            astr = astr.substring(0, idx);
            InetAddress addr = InetAddress.getByName(astr);
            int ref = Integer.decode(request[1]).intValue();
            int value = Integer.decode(request[2]).intValue();

            //1. Setup the Parameters
            //2. Open the connection
            con = new TCPMasterConnection(addr);
            con.setPort(port);
            con.connect();
            //3. Prepare the request
            WriteReq = new WriteSingleRegisterRequest();
            WriteReq.setReference(ref);  //register number
            MyReg.setValue(value);         //value for register
            WriteReq.setRegister(MyReg);
            //4. Prepare the transaction
            trans = new ModbusTCPTransaction(con);
            trans.setRequest(WriteReq);
            trans.execute();
            //6. Close the connection
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
