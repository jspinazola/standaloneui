package Util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import java.nio.file.Paths;

public class SystemConfig {

    private String name = "SystemConfig";
    private String remoteIP = "";
    private String locationWindows = "jdbc:sqlite:C:/Users/jspinazola/OneDrive - Danaher/Temp/DatabaseFiles/sqlite/db/";
    private String locationLinux = "jdbc:sqlite:/XOS/Data/DatabaseFiles/sqlite/db/";
    private String databaseLocation = "";
    private String databaseName = "";
    private String PLCAddress = "192.168.1.53";

    public SystemConfig(){}

    @JsonCreator
    public SystemConfig(
            @JsonProperty("remoteIP") String remoteIP,
            @JsonProperty("databaseLocation") String databaseLocation,
            @JsonProperty("databaseName") String databaseName,
            @JsonProperty("PLCAddress") String PLCAddress
    ){
        this.databaseLocation = databaseLocation;
        this.remoteIP = remoteIP;
        this.databaseName = databaseName;
        this.PLCAddress = PLCAddress;
    }

    public void readFromFile(String filename){
        try{
            ObjectMapper mapper = new ObjectMapper();
            ObjectReader reader = mapper.readerForUpdating(this);
            reader.readValue(Paths.get(filename).toFile());
        }catch (Exception e){
            System.out.println(e.getMessage());
            SaveConfig();
        }
        System.out.println("RemoteIP : " + this.remoteIP);
    }

    public void SaveConfig(){
        try{
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(Paths.get(name + ".json").toFile(),this);
        }catch(Exception e){
            System.out.println("Cannot Save: " + e.getMessage());
        }
    }

    public String getPLCAddress() {
        return PLCAddress;
    }

    public void setPLCAddress(String PLCAddress) {
        this.PLCAddress = PLCAddress;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getDatabaseLocation() {
        if(databaseLocation == ""){
            String osType = System.getProperty("os.name");
            if(osType.startsWith("Windows")){
                return locationWindows;
            }else {
                //System.out.println("Linux");
                return locationLinux;
            }
        }else {
            return databaseLocation;
        }
    }

    public void setDatabaseLocation(String databaseLocation) {
        this.databaseLocation = databaseLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemoteIP() {
        return remoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }
}
