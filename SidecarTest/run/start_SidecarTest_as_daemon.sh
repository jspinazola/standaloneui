#!/bin/sh
# Script to start daemon and obtain PID
/XOS/SidecarTest/run/run &
sleep 4
DAEMON_PID=$(ps ax | grep FPSControl | grep java | sed -e 's/^[[:space:]]*//' | cut -d ' ' -f1 )
if ps -p "${DAEMON_PID}" > /dev/null 2>&1
then
  echo "SidecarTest is running with PID: ${DAEMON_PID}"
  echo ${DAEMON_PID} > /XOS/SidecarTest/SidecarTest.pid
else
  echo "SidecarTest did not start."
fi

