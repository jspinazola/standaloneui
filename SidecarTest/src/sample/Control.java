package sample;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.MainPresenter;

import java.net.InetAddress;

public class Control extends Application {

    private Stage primaryStage;
    private Scene scene;
    private MainPresenter presenter;
    private StringProperty message = new SimpleStringProperty();
    private ControlBoardInterface board;
    private Thread boardThread;
    private SidecarModel model;

    @Override
    public void start(Stage primaryStage) throws Exception{

        model = new SidecarModel();

        this.primaryStage = primaryStage;
        this.primaryStage.setMinHeight(480);
        this.primaryStage.setMinWidth(800);
        this.primaryStage.setMaxHeight(480);
        this.primaryStage.setMaxWidth(800);
        this.primaryStage.setTitle("FPS");

        presenter = new MainPresenter();
        presenter.setModel(model);
        presenter.setPrimaryStage(primaryStage);
        scene = new Scene(presenter.getPanelRoot(),800,480);
        primaryStage.setScene(scene);
        primaryStage.show();
        presenter.setUp();

        board = new ControlBoardInterface();
        board.setMessageHolder(model.getMessage());
        board.setModel(model);
        boardThread = new Thread(board,"boardThread");
        boardThread.start();

        message.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println(newValue);
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
