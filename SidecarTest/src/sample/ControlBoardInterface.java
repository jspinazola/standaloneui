package sample;

import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import org.json.simple.JSONObject;

public class ControlBoardInterface implements Runnable{

    private ZMQ.Context ctx = ZMQ.context(1);
    private ZMQ.Socket sub = ctx.socket(SocketType.SUB);
    private String addressHeader = "tcp://*:";
    //private String addressHeader = "tcp://10.128.54.53:";
    private int portReq = 5557;
    private int portSub = 5556;
    private boolean running = true;
    private SidecarModel model;
    private StringProperty messageHolder;

    public void run() {
        setUp();
         while (running){
             String topic = sub.recvStr();
             switch (topic){
                 case "Status": {
                     String update = sub.recvStr();
                     System.out.println( "Received Update - " + update);
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                             messageHolder.set(update);
                         }
                     });
                 }
                 break;
                 case "Data":{
                     String data = sub.recvStr();
                     String ftir = "";
                     if(sub.hasReceiveMore()){
                         ftir = sub.recvStr();
                     }
                     Platform.runLater(new Runnable() {
                         @Override
                         public void run() {
                            model.setLastMeasurement(data);
                         }
                     });
                 }
                 break;
                 default: {
                     String data = sub.recvStr();
                     System.out.println("Unidentified Topic - " + topic + " : " + data);
                 }
             }
         }

    }

    public void setMessageHolder(StringProperty message){
        messageHolder = message;
    }

    public void setModel(SidecarModel model){
        this.model = model;
    }

    private void setUp(){
        if(model.getConfig().getRemoteIP() != ""){
            System.out.println("ControlBoardInterface IP:" +  model.getConfig().getRemoteIP());
            sub.connect("tcp://" + model.getConfig().getRemoteIP() + ":" +Integer.toString(portSub));
        }else {
            sub.connect(addressHeader+Integer.toString(portSub));
        }

        sub.subscribe("");
    }
}
