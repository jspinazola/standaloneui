package sample;

import FPS.FPSData;
import FPS.FPSSystemState;
import FPS.SQLManager;
import FPS.SensorData;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

public class ExcelWriter implements Runnable{

    private String winLocation = "C:/XOSGit/DatabaseFiles/";
    private String linuxLocation = "/XOS/Data/";
    private String location;
    private FPSData data;
    private boolean exportAll = true;

    public ExcelWriter(){
        String osType = System.getProperty("os.name");
        if(osType.startsWith("Windows")){
            System.out.println("Windows");
            location = winLocation;
        }else {
            System.out.println("Linux");
            location = linuxLocation;
        }
    }

    @Override
    public void run() {
        if(exportAll){
            exportDatabase("DatabaseExport.xls");
        }else {
            writeResult();
        }
    }

    public void setData(FPSData data) {
        this.data = data;
    }

    public void setExportAll(boolean exportAll){
        this.exportAll = exportAll;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public void writeResult(){

        SQLManager sql = new SQLManager("FPSDatabase");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet results = workbook.createSheet("Results");
        HSSFSheet statusSheet = workbook.createSheet("Status");
        HSSFSheet sensorSheet = workbook.createSheet("Data");

        Object[][] resultsData = {{"Setpoint","Kinematic Viscosity","Density","Dielectric Constant","Temperature","Status"},
                {data.getTargetTemp0(),data.getpViscosity0(),data.getpDensity0(),data.getpDielectricStrength0(),data.getpTemperature0(),data.getpStatus0()},
                {data.getTargetTemp1(),data.getpViscosity1(),data.getpDensity1(),data.getpDielectricStrength1(),data.getpTemperature1(),data.getpStatus1()},
                {" "},
                {"KV"+data.getTargetTemp0().toString(),data.getkV0(),"KV"+data.getTargetTemp1().toString(),data.getkV1(),"KV40",data.getKV40Walther()}
        };

        int rowCount = 0;
        for (Object[] aBook : resultsData) {
            Row row = results.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        ArrayList<FPSSystemState> statusData = sql.getStateEntries(data.getSqlID());
        Object[][] statusDataObject = new Object[statusData.size()+1][7];
        Object[] line1 = {"State","Elapsed Time","Measurement Point","HeatX Temp","Sensor Temp","Fluid Temp","Setpoint"};
        statusDataObject[0] = line1;
        int i = 1;
        Enumeration<FPSSystemState> eData = Collections.enumeration(statusData);
        while(eData.hasMoreElements()){
            FPSSystemState state = eData.nextElement();
            Object[] line = {state.getState().toString(),state.getElapsedTime(),state.getMeasurementPoint(),state.convertToC(state.getAdcHeatx()),state.convertToC(state.getAdcSensor()),state.getFluidTemp(),state.convertToC(state.getTempSetpoint())};
            statusDataObject[i] = line;
            i++;
        }
        rowCount = 0;
        for (Object[] aBook : statusDataObject) {
            Row row = statusSheet.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        ArrayList<SensorData> sensorData = sql.getSensorEntries(data.getSqlID());
        Object[][] sensorDataObject = new Object[statusData.size()+1][6];
        Object[] line2 = {"Target Temp","Kinematic Viscosity","Density","Dielectric Constant","Temperature","Status"};
        sensorDataObject[0] = line2;
        i = 1;
        Enumeration<SensorData> eSensor = Collections.enumeration(sensorData);
        while(eSensor.hasMoreElements()){
            SensorData sensorDataPoint = eSensor.nextElement();
            Object[] line = {sensorDataPoint.getTargetTemp(),sensorDataPoint.getpViscosity(),sensorDataPoint.getpDensity(),sensorDataPoint.getpDielectricStrength(),sensorDataPoint.getpTemperature(),sensorDataPoint.getpStatus()};
            sensorDataObject[i] = line;
            i++;
        }
        rowCount = 0;
        for (Object[] aBook : sensorDataObject) {
            Row row = sensorSheet.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        String name = location + String.valueOf(data.getSqlID()) + "_" + data.getScanInfo().getName().replace("[^A-Za-z0-9]","") + ".xls";
        name = name.replace(" ","");

        try (FileOutputStream outputStream = new FileOutputStream(name)) {
            workbook.write(outputStream);
            outputStream.flush();
        }catch (FileNotFoundException e){
            e.printStackTrace();
            System.out.println("Invalid FilePath");
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("IO Exception");
        }
    }

    public void exportDatabase(String fileName){

        SQLManager sql = new SQLManager("FPSDatabase");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet results = workbook.createSheet("Results");
        HSSFSheet statusSheet = workbook.createSheet("Status");
        HSSFSheet sensorSheet = workbook.createSheet("Data");

        ArrayList<FPSData> dataData = sql.getDataEntries();
        Object[] resultLine1 = {"ID","Name","Setpoint 1","Kinematic Viscosity 1","Density 1","Dielectric Constant 1","Temperature 1","Status 1","Setpoint 2","Kinematic Viscosity 2","Density 2","Dielectric Constant 2","Temperature 2","Status 2","System Status","KV1","KV2","KV40"};
        Object[][] resultsDataObject = new Object[dataData.size()+1][resultLine1.length];
        int i = 1;
        for (FPSData point:dataData) {
            Object[] line = {point.getSqlID(),
                                point.getScanInfo().getName(),
                                point.getTargetTemp0(),
                                point.getpViscosity0(),
                                point.getpDensity0(),
                                point.getpDielectricStrength0(),
                                point.getpTemperature0(),
                                point.getpStatus0(),
                                point.getTargetTemp1(),
                                point.getpViscosity1(),
                                point.getpDensity1(),
                                point.getpDielectricStrength1(),
                                point.getpTemperature1(),
                                point.getpStatus1(),
                                point.getpStatusSystem(),
                                point.getkV0(),
                                point.getkV1(),
                                point.getKV40Walther()};
            resultsDataObject[i] = line;
            i++;
        }

        int rowCount = 0;
        for (Object[] aBook : resultsDataObject) {
            Row row = results.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        ArrayList<FPSSystemState> statusData = sql.getStateEntries();
        Object[][] statusDataObject = new Object[statusData.size()+1][8];
        Object[] line1 = {"SQLID","State","Elapsed Time","Measurement Point","HeatX Temp","Sensor Temp","Fluid Temp","Setpoint"};
        statusDataObject[0] = line1;
        i =1;
        Enumeration<FPSSystemState> eData = Collections.enumeration(statusData);
        while(eData.hasMoreElements()){
            FPSSystemState state = eData.nextElement();
            Object[] line = {state.getId(),state.getState().toString(),state.getElapsedTime(),state.getMeasurementPoint(),state.convertToC(state.getAdcHeatx()),state.convertToC(state.getAdcSensor()),state.getFluidTemp(),state.convertToC(state.getTempSetpoint())};
            statusDataObject[i] = line;
            i++;
        }
        rowCount = 0;
        for (Object[] aBook : statusDataObject) {
            Row row = statusSheet.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        ArrayList<SensorData> sensorData = sql.getSensorEntries();
        Object[][] sensorDataObject = new Object[statusData.size()+1][6];
        Object[] line2 = {"Target Temp","Kinematic Viscosity","Density","Dielectric Constant","Temperature","Status"};
        sensorDataObject[0] = line2;
        i = 1;
        Enumeration<SensorData> eSensor = Collections.enumeration(sensorData);
        while(eSensor.hasMoreElements()){
            SensorData sensorDataPoint = eSensor.nextElement();
            Object[] line = {sensorDataPoint.getTargetTemp(),sensorDataPoint.getpViscosity(),sensorDataPoint.getpDensity(),sensorDataPoint.getpDielectricStrength(),sensorDataPoint.getpTemperature(),sensorDataPoint.getpStatus()};
            sensorDataObject[i] = line;
            i++;
        }
        rowCount = 0;
        for (Object[] aBook : sensorDataObject) {
            Row row = sensorSheet.createRow(++rowCount);
            int columnCount = 0;
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }

        String name = location + "DatabaseExport.xls";
        name = name.replace(" ","");

        try (FileOutputStream outputStream = new FileOutputStream(name)) {
            workbook.write(outputStream);
            outputStream.flush();
        }catch (FileNotFoundException e){
            e.printStackTrace();
            System.out.println("Invalid FilePath");
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("IO Exception");
        }

    }

}
