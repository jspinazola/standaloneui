package sample;

import FPS.FPSData;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.AbstractFxmlPanelController;
import FPS.SQLManager;
import xos.common.fullResult.WeakRef;

import java.util.ArrayList;

public class HistoryPresenter extends AbstractFxmlPanelController {

    private SQLManager sql = new SQLManager("FPSDatabase");
    private Controller control;
    private SidecarModel model;

    @FXML
    private TableView<HistoryRow> tableView;
    private ObservableList<HistoryRow> tableData;
    private int lastMeasurementID;
    private int numDisplayed = 6;

    @FXML
    private Label scanCounter;
    @FXML
    private Button previous;
    @FXML
    private Button view;
    @FXML
    private Button next;
    @FXML
    private Button export;




    public HistoryPresenter(){
        super(HistoryPresenter.class.getResource("History.fxml"), I18N.getBundle());
        makePanel();
        tableData = FXCollections.observableArrayList();
        control = Controller.getInstance();
        model = control.getModel();
        model.getLastMeasurementProperty().addListener(((observable, oldValue, newValue) -> {getHistory();}));
        getHistory();
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().addAll(
                makeTableColumn("sqlID","#",40),
                makeTableColumn("scanName","Scan Name",40),
                makeTableColumn("kv100","KV 100",40),
                makeTableColumn("kv40", "KV 40", 40),
                makeTableColumn("simpleMeasurement", "Run",40)
        );
        tableView.setItems(tableData);
    }

    private void getHistory(){
        tableData.clear();
        ArrayList<FPSData> history = sql.getDataEntries(numDisplayed);
        if(history.size() > 0) {
            lastMeasurementID = history.get(0).getSqlID();
            for (FPSData data : history) {
                tableData.add(new HistoryRow(data));
            }
            tableView.setItems(tableData);
            setCounterLabel();
        }
        next.setDisable(true);
        previous.setDisable(false);
    }

    private void setCounterLabel(){
        String text = Integer.toString(tableData.get(0).getSqlID()) + " of " + Integer.toString(lastMeasurementID);
        scanCounter.setText(text);
    }

    private void update(ArrayList<FPSData> history){
        export.setDisable(false);
        tableData.clear();
        for (FPSData data : history) {
            tableData.add(new HistoryRow(data));
        }
        tableView.setItems(tableData);
        setCounterLabel();
    }

    private TableColumn makeTableColumn(String factoryStr, String label, double prefWidth) {
        TableColumn column = new TableColumn(label);
        column.prefWidthProperty().set(prefWidth);
        column.minWidthProperty().set(prefWidth);
        column.setSortable(false);
        column.setCellValueFactory(new PropertyValueFactory<WeakRef<HistoryRow>, String>(factoryStr));
        return column;
    }

    @FXML
    private void handleDetail(){
        final ReadOnlyObjectProperty<HistoryRow> row = tableView.getSelectionModel().selectedItemProperty();
        if(row.get() != null){
            QuickViewPresenter qv = new QuickViewPresenter();
            qv.setData(row.get().getData());
            qv.show();
        }else {
            System.out.println("Nothing Selected");
        }
    }

    @FXML
    private void handlePrevious(){
        //Find next top row ID
        int nextStart = tableData.get(0).getSqlID() - numDisplayed;
        if (nextStart <= numDisplayed){
            nextStart = numDisplayed;
            previous.setDisable(true);
        }
        update(sql.getDataEntriesFrom(numDisplayed,nextStart));
        next.setDisable(false);
    }

    @FXML
    private void handleNext(){
        //Find next top row ID
        int nextStart = tableData.get(0).getSqlID() + numDisplayed;
        if (nextStart >= lastMeasurementID){
            nextStart = lastMeasurementID;
            next.setDisable(true);
        }
        update(sql.getDataEntriesFrom(numDisplayed,nextStart));
        previous.setDisable(false);
    }

    @FXML
    private void handleExport(){
        System.out.println("Export");
        export.setDisable(true);
        ExcelWriter ex = new ExcelWriter();
        ex.setExportAll(true);
        Platform.runLater(ex);
    }

    public void refreshContent(){

    }

}
