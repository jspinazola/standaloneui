package sample;

import FPS.FPSData;

public class HistoryRow {

    private FPSData data;

    public String getScanName() {
        return data.getScanInfo().getName();
    }

    public int getSqlID() {
        return data.getSqlID();
    }

    public double getKv40() {
        return data.getKV40Walther();
    }

    public double getKv100() {
        double kv100;
        try{
           kv100 = data.getpViscosity0()/data.getpDensity0();
        }catch (ArithmeticException e){
            kv100 = 0;
        }
        return kv100;
    }

    public boolean isSimpleMeasurement() {
        return data.getScanInfo().getSimpleMeasurement();
    }

    public HistoryRow(FPSData data){
        this.data = data;
    }

    public FPSData getData() {
        return data;
    }
}
