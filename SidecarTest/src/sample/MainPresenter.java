package sample;

import FPS.FPSData;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import com.sun.javafx.scene.control.skin.FXVK;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXML;
import javafx.scene.DepthTest;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import xos.common.control.HDControl;
import xos.common.jfxasync.PlatformHelper;
import xos.common.view.HDView;
import sample.I18N;
import sample.Controller;
import sample.AbstractFxmlPanelController;

import java.util.HashMap;

public class MainPresenter extends AbstractFxmlPanelController{

    @FXML
    private StackPane rootStack;

    private Controller control;
    private SidecarModel model;
    private QuickViewPresenter quickViewPresenter;
    private SidecarPageName  currentPage;

    @FXML public ModalDimmer modalDimmer;

    @FXML
    private BorderPane body;
    @FXML
    ToggleButton btnScanInfo;
    @FXML
    ToggleButton btnResults;
    @FXML
    ToggleButton btnSettings;
    @FXML
    ToggleButton btnHistory;
    @FXML
    ToggleButton btnStatus;
    @FXML
    ToggleGroup toggleGroup;

    private HashMap<SidecarPageName, AbstractFxmlPanelController> pages;

    private Stage primaryStage;

    public MainPresenter(){
        super(MainPresenter.class.getResource("sample.fxml"), I18N.getBundle());
        makePanel();
        quickViewPresenter = new QuickViewPresenter();
    }

    public void setUp(){

        control = Controller.getInstance();
        control.setModel(model);
        control.connect();
        control.setView(this);
        pages = new HashMap<>();

        //control.setButtonIcons(lblBack, FontAwesomeIcon.CHEVRON_LEFT, ContentDisplay.LEFT, Color.web(XosColors.XOS_ORANGE));
        control.setButtonIcons(btnSettings, FontAwesomeIcon.COGS, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnResults, FontAwesomeIcon.BAR_CHART, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnScanInfo, FontAwesomeIcon.QRCODE, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnStatus, FontAwesomeIcon.TACHOMETER, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        control.setButtonIcons(btnHistory, FontAwesomeIcon.HISTORY, ContentDisplay.TOP, Color.web(XosColors.WHITE));
        //control.setButtonIcons(btnQuickView, FontAwesomeIcon.EYE, ContentDisplay.TOP, Color.web(XosColors.XOS_BLUE));
        setContent(SidecarPageName.HISTORY,false);
        setContent(SidecarPageName.STATUS,false);
        setContent(SidecarPageName.SETTINGS,false);
        setContent(SidecarPageName.RESULTS,false);
        setContent(SidecarPageName.SCANINFO,false);

        model.getLastMeasurementProperty().addListener(new ChangeListener<FPSData>() {
            @Override
            public void changed(ObservableValue<? extends FPSData> observable, FPSData oldValue, FPSData newValue) {
                setContent(SidecarPageName.RESULTS,false);
            }
        });
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, ke -> handleKeyEvent(ke));
    }

    public void setContent(Node content){
        body.setCenter(content);
        body.setDepthTest(DepthTest.DISABLE);
    }

    public void setContent(SidecarPageName page, boolean addToHistory){
        if (getPage(page) == null) {
            AbstractFxmlPanelController afpc = page.getPageFactory().create();
            afpc.setUp();
            addPage(page, afpc);
            //updatePageCss(page);
        }
        currentPage = page;
        refreshContent();
        setContent(getPage(page).getPanelRoot());
        selectToggle(page);
    }

    public void generateContent(SidecarPageName page){
        if (getPage(page) == null) {
            AbstractFxmlPanelController afpc = page.getPageFactory().create();
            afpc.setUp();
            addPage(page, afpc);
            getPage(page).getPanelRoot();
            //updatePageCss(page);
        }
        refreshContent();
    }

    private void refreshContent(){
        if(null != currentPage){
            switch (currentPage) {
                case STATUS:
                    ((StatusPresenter) pages.get(SidecarPageName.STATUS)).refreshContent();
                    break;
                case HISTORY:
                    ((HistoryPresenter) pages.get(SidecarPageName.HISTORY)).refreshContent();
                    break;
                case SCANINFO:
                    ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).refreshContent();
                    break;
                case SETTINGS:
                    ((SettingsPresenter) pages.get(SidecarPageName.SETTINGS)).refreshContent();
                    break;
                case RESULTS:
                    ((ResultsPresenter) pages.get(SidecarPageName.RESULTS)).refreshContent();
                    break;
                default:
                    break;
            }
        }
    }

    private void addPage(SidecarPageName name, AbstractFxmlPanelController page) {
        //TODO This implementation does not check if the key exists, but simply overwrites if it does. This may or may not be desirable.
        pages.put(name, page);
    }

    public AbstractFxmlPanelController getPage(SidecarPageName name) {
        return pages.get(name);
    }

    private void selectToggle(SidecarPageName page) {
        if (null != page) {
            switch (page) {
                case STATUS:
                    btnStatus.setSelected(true);
                    break;
                case HISTORY:
                    btnHistory.setSelected(true);
                    break;
                case SCANINFO:
                    btnScanInfo.setSelected(true);
                    break;
                case SETTINGS:
                    btnSettings.setSelected(true);
                    break;
                case RESULTS:
                    btnResults.setSelected(true);
                    break;
                default:
                    break;
            }
        }
    }

    @FXML
    public void handleStatus(){
        setContent(SidecarPageName.STATUS,false);
    }

    @FXML
    void handleHistory(){
        setContent(SidecarPageName.HISTORY,false);
    }
    @FXML
    void handleScanInfo(){
        setContent(SidecarPageName.SCANINFO,false);
    }

    @FXML
    void handleSettings(){
        setContent(SidecarPageName.SETTINGS,false);
    }

    @FXML
    void handleResults(){
        setContent(SidecarPageName.RESULTS,false);
    }

    public void setModel(SidecarModel model){
        this.model = model;
    }

    public SidecarModel getModel(){
        return model;
    }

    public void showModalMessage(Node message, boolean blockOutsideMouseEvents) {
        modalDimmer = new ModalDimmer();
        modalDimmer.setId("ModalDimmer");
        modalDimmer.setControl(control);
        rootStack.getChildren().add(modalDimmer);
        modalDimmer.showModalMessage(message, blockOutsideMouseEvents);
    }

    /**
     * Hide any modal message that is shown
     */
    public void hideModalMessage() {
        PlatformHelper.run(() -> {
            if (modalDimmer != null) {
                modalDimmer.hideModalMessage();
            }
            rootStack.getChildren().remove(modalDimmer);
            modalDimmer = null;
        });
    }

    public void handleKeyEvent(KeyEvent ke) {
        if (currentPage == SidecarPageName.SCANINFO) {
            ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).addText();
        }
        else{
            setContent(SidecarPageName.SCANINFO,false);
            ((ScanInfoPresenter) pages.get(SidecarPageName.SCANINFO)).addText();
        }
        if (ke.getCode() == KeyCode.O && ke.isShortcutDown()) {
            //openFile();
        }
        else if (ke.getCode() == KeyCode.ESCAPE) {
            modalDimmer.hideModalMessage();
        }
    }



}
