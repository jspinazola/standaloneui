package sample;

import sample.AbstractFxmlPanelController;

public interface PageFactory<T extends AbstractFxmlPanelController> {
    public T create();
}
