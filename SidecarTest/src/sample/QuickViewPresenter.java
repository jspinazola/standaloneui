package sample;

import FPS.FPSData;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import xos.limitsEditor.demo.Control;

public class QuickViewPresenter extends AbstractFxmlPanelController{

    @FXML
    BorderPane bpRoot;
    @FXML
    Button btnClose;

    private FPSData data;
    private Controller control;

    public QuickViewPresenter() {
        super(QuickViewPresenter.class.getResource("quickView.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        control.setButtonIcons(btnClose, FontAwesomeIcon.WINDOW_CLOSE, ContentDisplay.TOP, Color.web(XosColors.XOS_BLUE));
    }

    public void setData(FPSData data) {
        this.data = data;
    }

    public void show(){
        ResultsSummary summary = new ResultsSummary();
        summary.setData(data);
        summary.refreshContent();
        bpRoot.setCenter(summary.getPanelRoot());
        control.showModalMessage(getPanelRoot(),true);
    }

    @FXML
    void handleClose() {
        control.hideModalMessage();
    }
}
