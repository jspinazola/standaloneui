package sample;

import FPS.FPSData;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;


public class ResultsPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;

    @FXML
    FlowPane flowPane;
    @FXML
    Label nameLabel;
    @FXML
    Label sp1Value;
    @FXML
    Label sp2Value;
    @FXML
    Label v0Value;
    @FXML
    Label p0Value;
    @FXML
    Label d0Value;
    @FXML
    Label t0Value;
    @FXML
    Label s0Value;
    @FXML
    Label v1Value;
    @FXML
    Label p1Value;
    @FXML
    Label d1Value;
    @FXML
    Label t1Value;
    @FXML
    Label s1Value;
    @FXML
    Label kv1Label;
    @FXML
    Label kv1Value;
    @FXML
    Label kv0Label;
    @FXML
    Label kv0Value;
    @FXML
    Label kv40Label;
    @FXML
    Label kv40Value;
    @FXML
    Label systemStatusValue;

    private FPSData data;

    public ResultsPresenter(){
        super(ResultsPresenter.class.getResource("Results.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        model.getLastMeasurementProperty().addListener(((observable, oldValue, newValue) -> {update();}));
        update();
    }

    private void update(){
        data = model.getLastMeasurement();
        nameLabel.setText(data.getScanInfo().getName());
        systemStatusValue.setText(data.getpStatusSystem().toString());
        sp1Value.setText(data.getTargetTemp0().toString());
        sp2Value.setText(data.getTargetTemp1().toString());

        v0Value.setText(data.getpViscosity0().toString());
        p0Value.setText(data.getpDensity0().toString());
        d0Value.setText(data.getpDielectricStrength0().toString());
        t0Value.setText(data.getpTemperature0().toString());
        s0Value.setText(data.getpStatus0().toString());

        v1Value.setText(data.getpViscosity1().toString());
        p1Value.setText(data.getpDensity1().toString());
        d1Value.setText(data.getpDielectricStrength1().toString());
        t1Value.setText(data.getpTemperature1().toString());
        s1Value.setText(data.getpStatus1().toString());

        kv0Label.setText("kV"+data.getTargetTemp0());
        kv0Value.setText(String.format("%.3f",data.getkV0()));
        kv1Label.setText("kV"+data.getTargetTemp1());
        kv1Value.setText(String.format("%.3f",data.getkV1()));
        kv40Value.setText(String.format("%.3f",data.getKV40Walther()));

        if(data.getSqlID() != -1){
            ExcelWriter ex = new ExcelWriter();
            ex.setData(data);
            ex.setExportAll(false);
            Platform.runLater(ex);
        }
    }

    public void refreshContent(){

    }

}

