package sample;

import FPS.FPSData;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

public class ResultsSummary extends AbstractFxmlPanelController{

    @FXML
    FlowPane flowPane;
    @FXML
    Label TestLabel;
    @FXML
    Label sp1Value;
    @FXML
    Label sp2Value;
    @FXML
    Label v0Value;
    @FXML
    Label p0Value;
    @FXML
    Label d0Value;
    @FXML
    Label t0Value;
    @FXML
    Label s0Value;
    @FXML
    Label v1Value;
    @FXML
    Label p1Value;
    @FXML
    Label d1Value;
    @FXML
    Label t1Value;
    @FXML
    Label s1Value;
    @FXML
    Label kv1Label;
    @FXML
    Label kv1Value;
    @FXML
    Label kv0Label;
    @FXML
    Label kv0Value;
    @FXML
    Label kv40Label;
    @FXML
    Label kv40Value;

    private FPSData data;
    private Controller control;

    public ResultsSummary() {
        super(ResultsSummary.class.getResource("resultsSummary.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        flowPane.setAlignment(Pos.CENTER);
    }

    public void refreshContent(){
        TestLabel.setText(data.getScanInfo().getName());
        sp1Value.setText(data.getTargetTemp0().toString());
        sp2Value.setText(data.getTargetTemp1().toString());

        v0Value.setText(data.getpViscosity0().toString());
        p0Value.setText(data.getpDensity0().toString());
        d0Value.setText(data.getpDielectricStrength0().toString());
        t0Value.setText(data.getpTemperature0().toString());
        s0Value.setText(data.getpStatus0().toString());

        v1Value.setText(data.getpViscosity1().toString());
        p1Value.setText(data.getpDensity1().toString());
        d1Value.setText(data.getpDielectricStrength1().toString());
        t1Value.setText(data.getpTemperature1().toString());
        s1Value.setText(data.getpStatus1().toString());

        kv0Label.setText("kV"+data.getTargetTemp0());
        kv0Value.setText(String.format("%.3f",data.getkV0()));
        kv1Label.setText("kV"+data.getTargetTemp1());
        kv1Value.setText(String.format("%.3f",data.getkV1()));
        kv40Value.setText(String.format("%.3f",data.getKV40Walther()));
    }

    public void setData(FPSData data){
        this.data = data;
    }
}
