package sample;

import FPS.MeasurementData;
import FPS.ScanInfo;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import org.controlsfx.control.ToggleSwitch;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import sample.AbstractFxmlPanelController;
import sample.Controller;
import sample.SidecarModel;

import java.awt.*;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class SettingsPresenter extends AbstractFxmlPanelController {
    private Controller control;
    private SidecarModel model;
    private InetAddress ipAddress;

    @FXML
    private ToggleSwitch measurementMode;

    @FXML
    private ToggleSwitch setpointsMode;
    @FXML
    private ToggleSwitch measureFTIR;
    @FXML
    private ToggleSwitch measureFPS;

    @FXML
    private ToggleSwitch pwmMode;

    @FXML
    private ToggleSwitch testMode;

    @FXML
    private Label IP;

    @FXML
    private Label FW;


    public SettingsPresenter(){
        super(SettingsPresenter.class.getResource("settings.fxml"), I18N.getBundle());
        makePanel();
        control = Controller.getInstance();
        model = control.getModel();
        measurementMode.setSelected(model.getConfig().isSimpleMeasurementMode());
        pwmMode.setSelected(model.getConfig().isPwmViewMode());
        model.getControlStateProperty().addListener(((observable, oldValue, newValue) -> {
            if(model.getControlState() != SidecarModel.CONTROLSTATE.MEASURING){
                measurementMode.setDisable(false);
                testMode.setDisable(false);
            }else {
                measurementMode.setDisable(true);
                testMode.setDisable(true);
            }
        }));
        measurementMode.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.getConfig().setSimpleMeasurementMode(measurementMode.isSelected());
            System.out.println(measurementMode.isSelected());
        }));
        setpointsMode.setDisable(true);
        setpointsMode.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.getConfig().setSetpointsMeasurementMode(measurementMode.isSelected());
            System.out.println(setpointsMode.isSelected());
        }));
        pwmMode.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.getConfig().setPwmViewMode(pwmMode.isSelected());
            System.out.println(pwmMode.isSelected());
        }));
        //testMode.setDisable(true);
        testMode.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            control.TESTMODE();
        }));
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ipAddress = socket.getLocalAddress();
        }catch (SocketException e){
            System.out.println("Can't IP 1");
        }catch (UnknownHostException ie){ System.out.println("Can't IP 2");}
        IP.setText(ipAddress.getHostAddress().trim());
        FW.setText(model.getFPSStatus().getFWVersion());
        model.getFPSStatusProperty().addListener(((observable, oldValue, newValue) -> {
            FW.setText(model.getFPSStatus().getFWVersion());
        }));
        measureFPS.setSelected(model.getConfig().isRunFPS());
        measureFPS.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.getConfig().setRunFPS(measureFPS.isSelected());
        }));
        measureFTIR.setSelected(model.getConfig().isRunFTIR());
        measureFTIR.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            model.getConfig().setRunFTIR(measureFTIR.isSelected());
        }));
    }

    public void refreshContent(){
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ipAddress = socket.getLocalAddress();
        }catch (SocketException e){
            System.out.println("Can't IP 1");
        }catch (UnknownHostException ie){ System.out.println("Can't IP 2");}
        IP.setText(ipAddress.getHostAddress().trim());
    }

}
