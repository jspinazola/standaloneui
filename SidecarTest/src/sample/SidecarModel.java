package sample;

import FPS.FPSSystemState;
import FPS.FTIR.FTIRData;
import FPS.FTIR.FTIRStatus;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import FPS.FPSData;
import FPS.FPSStates;
import FPS.ScanInfo;
import FPS.FPSSystemState;
import FPS.FTIR.FTIRStatus;
import sample.Util.SidecarConfig;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SidecarModel{

    private StringProperty message = new SimpleStringProperty();;
    private ObjectProperty<SidecarConfig> config = new SimpleObjectProperty();

    private ObjectProperty<ScanInfo> scanInfo = new SimpleObjectProperty<>();
    private ObjectProperty<FPSData> lastMeasurement = new SimpleObjectProperty<>();
    private ObjectProperty<FTIRData> lastFTIR = new SimpleObjectProperty<>();
    private ObjectProperty<CONTROLSTATE> controlState = new SimpleObjectProperty<>();
    private ObjectProperty<FPSSystemState> fpsStatus = new SimpleObjectProperty<>();
    private ObjectProperty<FTIRStatus> ftirStatus = new SimpleObjectProperty<>();
    private StringProperty rep = new SimpleStringProperty();
    private BooleanProperty readingQRProperty = new SimpleBooleanProperty();


    public enum CONTROLSTATE {
        UNINIT,
        INIT,
        WAITING,
        WAITINGTEST,
        MEASURING,
        FINISHED,
        CUSTOM_MEASUREMENT,
        FTIR,
        FTIRBG,
        FTIRPURGE,
        FTIRDRAIN,
        ERROR
    }


    public SidecarModel (){
        config.setValue(new SidecarConfig());
        lastMeasurement.setValue(new FPSData());
        scanInfo.setValue(new ScanInfo());
        controlState.setValue(CONTROLSTATE.UNINIT);
        fpsStatus.setValue(new FPSSystemState());
        ftirStatus.setValue(new FTIRStatus());
        config.getValue().readFromFile("/XOS/SidecarTest/SidecarConfig.json");
        System.out.println("Did it take: " + config.getValue().getRemoteIP());
        message.addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                convertToJson();
            }
        });
    }

    public StringProperty getMessage() {
        return message;
    }

    public void setMessage(StringProperty message){
        this.message = message;
    }

    private void convertToJson(){
        //System.out.println( "Converting Message - " + message.get());
        JSONParser parser = new JSONParser();
        JSONObject json = new JSONObject();
        try {
            json = (JSONObject) parser.parse(message.get());
        }catch (ParseException e){
            System.out.println("Could Not Parse JSON");
        }
        for(Object key : json.keySet()){
            try {
                switch (key.toString()) {
                    case "controlState":{
                        controlState.setValue(CONTROLSTATE.valueOf(json.get(key).toString()));
                    }
                    break;
                    case "ftir":{
                        ObjectMapper ob = new ObjectMapper();
                        try{
                            ftirStatus.setValue(ob.readValue(json.get(key).toString(),FTIRStatus.class));
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                    case "fps":{
                        ObjectMapper ob = new ObjectMapper();
                        try{
                            fpsStatus.setValue(ob.readValue(json.get(key).toString(),FPSSystemState.class));
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                    default: {
                        System.out.println(key.toString());
                    }
                    break;
                }
            }catch (Exception e){
                System.out.println(key);
                System.out.println(json.get(key).toString());
           }
        }
    }

    public CONTROLSTATE getControlState(){return controlState.getValue();}
    public ObjectProperty getControlStateProperty(){return controlState;}
    public StringProperty getRep(){return rep;}
    public void setRep(String value){rep.setValue(value);}


    public void setLastMeasurement(String data){
        FPSData newData = new FPSData();
        newData.parseJSONData(data);
        lastMeasurement.setValue(newData);
    }
    public FPSData getLastMeasurement(){ return lastMeasurement.getValue();}
    public ObjectProperty getLastMeasurementProperty(){return lastMeasurement;}

    public void setLastFTIR(String data){
       FTIRData newData = new FTIRData();
       newData.parseJSON(data);
       lastFTIR.setValue(newData);
    }
    public FTIRData getLastFTIR(){return lastFTIR.getValue();}
    public ObjectProperty getLastFTIRProperty(){return lastFTIR;}

    public void setScanInfo(ScanInfo scanInfo) {
        this.scanInfo.setValue(scanInfo);
    }
    public ScanInfo getScanInfo(){return scanInfo.get();}

    public boolean isReadingQR() {
        return readingQRProperty.get();
    }

    public BooleanProperty readingQRProperty() {
        return readingQRProperty;
    }

    public void setReadingQR(boolean readingQR) {
        this.readingQRProperty.set(readingQR);
    }

    public ObjectProperty getFPSStatusProperty(){return fpsStatus;}
    public FPSSystemState getFPSStatus(){return fpsStatus.getValue();}

    public ObjectProperty getFTIRStatusProperty(){return ftirStatus;}
    public FTIRStatus getFTIRStatus(){return ftirStatus.getValue();}

    public SidecarConfig getConfig(){return config.getValue();}
    public void setConfig(SidecarConfig config) {this.config.set(config);}
    public ObjectProperty getConfigObjectProperty(){return config;}
}
