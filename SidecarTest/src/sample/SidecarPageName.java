package sample;

public enum SidecarPageName {

    STATUS(StatusPresenter::new),
    SCANINFO(ScanInfoPresenter::new),
    RESULTS(ResultsPresenter::new),
    SETTINGS(SettingsPresenter::new),
    HISTORY(HistoryPresenter::new);

    private final PageFactory factory;

    SidecarPageName (PageFactory pf) {factory = pf;}

    public PageFactory getPageFactory(){return factory;}

}
