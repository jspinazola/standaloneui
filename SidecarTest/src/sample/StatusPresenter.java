package sample;

import FPS.FPSSystemState;
import FPS.FTIR.FTIRComm;
import FPS.FTIR.FTIRState;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import eu.hansolo.medusa.*;

public class StatusPresenter extends AbstractFxmlPanelController {

    private Controller control;
    private SidecarModel model;
    private FPSSystemState converter;

    @FXML
    private Label statusLabel;
    @FXML
    private Label elapsedTime;
    @FXML
    private Label adcSensor;
    @FXML
    private Label adcHeatx;
    @FXML
    private Label fluidTemp;
    @FXML
    private Label status;
    @FXML
    private Gauge sensorGauge;
    @FXML
    private Gauge heatGauge;
    @FXML
    private Gauge fluidGauge;
    @FXML
    private Label sensorGaugeLabel;
    @FXML
    private Label heatGaugeLabel;
    @FXML
    private Label fluidGaugeLabel;
    @FXML
    private Label heatPWM;
    @FXML
    private Label sensorPWM;
    @FXML
    private Gauge FTIRProgress;

    public StatusPresenter() {
        super(StatusPresenter.class.getResource("status.fxml"), I18N.getBundle());
        makePanel();
        converter = new FPSSystemState();
        control = Controller.getInstance();
        model = control.getModel();
        model.getFPSStatusProperty().addListener((observable, oldValue, newValue) -> updateStatus());
        heatPWM.setVisible(false);
        sensorPWM.setVisible(false);
        model.getConfigObjectProperty().addListener(((observable, oldValue, newValue) -> {
            if(model.getConfig().isPwmViewMode()){
                heatPWM.setVisible(true);
                sensorPWM.setVisible(true);
            }else {
                heatPWM.setVisible(false);
                sensorPWM.setVisible(false);
            }
        }));
        setUpGauge();
        updateStatus();
    }

    private void setUpGauge(){
        sensorGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        sensorGauge.setMaxValue(125.0);
        sensorGauge.setTitle("Sensor Temperature");
        heatGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        heatGauge.setMaxValue(125.0);
        heatGauge.setTitle("Heat Exchanger Temperature");
        fluidGauge.setSkinType(Gauge.SkinType.TILE_SPARK_LINE);
        fluidGauge.setMaxValue(150.0);
        fluidGauge.setTitle("Oil Temperature");
        FTIRProgress.setSkinType(Gauge.SkinType.TILE_TEXT_KPI);
        FTIRProgress.setTitle("FTIR Warmup Progress");
        sensorGauge.managedProperty().bind(sensorGauge.visibleProperty());
        heatGauge.managedProperty().bind(heatGauge.visibleProperty());
        fluidGauge.managedProperty().bind(fluidGauge.visibleProperty());
        FTIRProgress.managedProperty().bind(FTIRProgress.visibleProperty());
        FTIRProgress.setVisible(false);
    }

    private void updateStatus() {
        if(model.getFTIRStatus().getState()==FTIRState.MEASURING || model.getFTIRStatus().getState()==FTIRState.MEASURING_BACKGROUND){
            FTIRProgress.setVisible(true);
            FTIRProgress.setValue(model.getFTIRStatus().getPercentFinished()*100);
            sensorGauge.setVisible(false);
            heatGauge.setVisible(false);
            fluidGauge.setVisible(false);
        }else {
            FTIRProgress.setVisible(false);
            sensorGauge.setVisible(true);
            heatGauge.setVisible(true);
            fluidGauge.setVisible(true);
        }
        if(model.getControlState() == SidecarModel.CONTROLSTATE.MEASURING){
            status.setText(model.getControlState().toString() + " - " + model.getFPSStatus().getState());
        }else if(model.getControlState() == SidecarModel.CONTROLSTATE.WAITING || model.getControlState() == SidecarModel.CONTROLSTATE.ERROR){
            status.setText(model.getControlState().toString() + " - " + model.getFPSStatus().getState() + " - " + model.getFTIRStatus().getState());
        }
        else {
            if(model.getControlState() != null){
                status.setText(model.getControlState().toString());
            }
        }
        elapsedTime.setText("Elapsed Time: " + Integer.toString(model.getFPSStatus().getElapsedTimeSeconds()));
        sensorGauge.setValue(converter.convertToC(model.getFPSStatus().getAdcSensor()));
        heatGauge.setValue(converter.convertToC(model.getFPSStatus().getAdcHeatx()));
        fluidGauge.setValue(model.getFPSStatus().getFluidTemp());
        if(model.getControlState() == SidecarModel.CONTROLSTATE.MEASURING) {
            sensorGaugeLabel.setText(" ");
        }else {

        }
        heatPWM.setText(Integer.toString(model.getFPSStatus().getPWM2()));
        sensorPWM.setText(Integer.toString(model.getFPSStatus().getPWM1()));
    }


    private void setModel(SidecarModel model){
        this.model = model;
    }

    public void refreshContent(){

    }
}